1490.1.1 = {
	discover_innovation = innovation_motte
	discover_innovation = innovation_catapult
	discover_innovation = innovation_barracks
	discover_innovation = innovation_mustering_grounds
	discover_innovation = innovation_bannus
	discover_innovation = innovation_quilted_armor
	#
	discover_innovation = innovation_development_01
	discover_innovation = innovation_currency_01
	discover_innovation = innovation_gavelkind
	discover_innovation = innovation_crop_rotation
	discover_innovation = innovation_city_planning
	discover_innovation = innovation_casus_belli
	discover_innovation = innovation_plenary_assemblies
	discover_innovation = innovation_ledger
	#
	join_era = culture_era_early_medieval
}

1500.1.1 = {
	add_innovation_progress = {
		culture_innovation = innovation_battlements
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_mangonel
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_burhs
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_house_soldiers
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_horseshoes
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_arched_saddle
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_hereditary_rule
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_manorialism
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_development_02
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_currency_02
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_royal_prerogative
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_chronicle_writing
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_armilary_sphere
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_baliffs
		progress = 30
	}
}

1510.1.1 = {
	add_innovation_progress = {
		culture_innovation = innovation_battlements
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_mangonel
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_burhs
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_house_soldiers
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_horseshoes
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_arched_saddle
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_hereditary_rule
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_manorialism
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_development_02
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_currency_02
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_royal_prerogative
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_chronicle_writing
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_armilary_sphere
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_baliffs
		progress = 30
	}
}

1520.1.1 = {
	discover_innovation = innovation_battlements
	discover_innovation = innovation_mangonel
	discover_innovation = innovation_burhs
	discover_innovation = innovation_house_soldiers
	discover_innovation = innovation_horseshoes
	discover_innovation = innovation_arched_saddle
	#
	discover_innovation = innovation_hereditary_rule
	discover_innovation = innovation_manorialism
	discover_innovation = innovation_development_02
	discover_innovation = innovation_currency_02
	discover_innovation = innovation_royal_prerogative
	discover_innovation = innovation_chronicle_writing
	discover_innovation = innovation_armilary_sphere
	discover_innovation = innovation_baliffs
	#
	join_era = culture_era_high_medieval
}

1530.1.1 = {
	add_innovation_progress = {
		culture_innovation = innovation_hoardings
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_trebuchet
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_castle_baileys
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_men_at_arms
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_knighthood
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_advanced_bowmaking
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_heraldry
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_windmills
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_divine_right
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_land_grants
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_scutage
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_guilds
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_development_03
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_currency_03
		progress = 30
	}
}

1540.1.1 = {
	add_innovation_progress = {
		culture_innovation = innovation_hoardings
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_trebuchet
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_castle_baileys
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_men_at_arms
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_knighthood
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_advanced_bowmaking
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_heraldry
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_windmills
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_divine_right
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_land_grants
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_scutage
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_guilds
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_development_03
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_currency_03
		progress = 30
	}
}

1550.1.1 = {
	discover_innovation = innovation_hoardings
	discover_innovation = innovation_trebuchet
	discover_innovation = innovation_castle_baileys
	discover_innovation = innovation_men_at_arms
	discover_innovation = innovation_knighthood
	discover_innovation = innovation_advanced_bowmaking
	#
	discover_innovation = innovation_heraldry
	discover_innovation = innovation_windmills
	discover_innovation = innovation_divine_right
	discover_innovation = innovation_land_grants
	discover_innovation = innovation_scutage
	discover_innovation = innovation_guilds
	discover_innovation = innovation_development_03
	discover_innovation = innovation_currency_03
	#
	join_era = culture_era_late_medieval
}

1560.1.1 = {
	add_innovation_progress = {
		culture_innovation = innovation_machicolations
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_bombard
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_royal_armory
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_standing_armies
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_sappers
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_plate_armor
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_primogeniture
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_cranes
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_noblesse_oblige
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_rightful_ownership
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_ermine_cloaks
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_court_officials
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_development_04
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_currency_04
		progress = 30
	}
}

1570.1.1 = {
	add_innovation_progress = {
		culture_innovation = innovation_machicolations
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_bombard
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_royal_armory
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_standing_armies
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_sappers
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_plate_armor
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_primogeniture
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_cranes
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_noblesse_oblige
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_rightful_ownership
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_ermine_cloaks
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_court_officials
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_development_04
		progress = 30
	}
	add_innovation_progress = {
		culture_innovation = innovation_currency_04
		progress = 30
	}
}

1580.1.1 = {
	discover_innovation = innovation_machicolations
	discover_innovation = innovation_bombard
	discover_innovation = innovation_royal_armory
	discover_innovation = innovation_standing_armies
	discover_innovation = innovation_sappers
	discover_innovation = innovation_plate_armor
	#
	discover_innovation = innovation_primogeniture
	discover_innovation = innovation_cranes
	discover_innovation = innovation_noblesse_oblige
	discover_innovation = innovation_rightful_ownership
	discover_innovation = innovation_ermine_cloaks
	discover_innovation = innovation_court_officials
	discover_innovation = innovation_development_04
	discover_innovation = innovation_currency_04
}
