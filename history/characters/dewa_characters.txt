﻿10000101 = {
	name=character_name_10000101 # Hirosue
	dynasty=10000001 # Ando
	martial=7
	diplomacy=6
	intrigue=8
	stewardship=5
	learning=6
	religion="shinto"
	culture="dewa"
	father=10000105
	trait="just"
	trait="education_intrigue_1"
	prowess=10
	1480.1.2={
		birth=yes
	}
	1496.1.1={
		give_nickname="nick_taro"
	}
	1547.2.27={
		death=yes
	}
}
10000102 = {
	name=character_name_10000102 # Kiyosue
	dynasty=10000001 # Ando
	martial=6
	diplomacy=6
	intrigue=8
	stewardship=6
	learning=6
	religion="shinto"
	culture="dewa"
	father=10000101
	trait="education_intrigue_1"
	prowess=10
	1514.1.1={
		birth=yes
	}
	1530.1.1={
		add_spouse=10000104 # Ando Takako, daughter of Ando Takasue
		give_nickname="nick_taro"
	}
	1553.9.18={
		death=yes
	}
}
10000103 = {
	name=character_name_10000103 # Takasue
	dynasty=10000001 # Ando
	father=10000107
	martial=7
	diplomacy=5
	intrigue=4
	stewardship=5
	learning=5
	religion="shinto"
	culture="dewa"
	trait="education_diplomacy_3"
	trait="zealous"
	prowess=11
	1492.1.1={
		birth=yes
	}
	1551.10.12={
		death=yes
	}
}
10000104 = {
	name=character_name_10000104 # Takako
	female = yes
	dynasty=10000001 # Ando
	martial=8
	diplomacy=7
	intrigue=5
	stewardship=7
	religion="shinto"
	culture="dewa"
	trait="education_learning_3"
	father=10000103 # Ando Takasue
	prowess=10
	1516.1.2={
		birth=yes
	}
	1553.9.2={
		death=yes
	}
}
10000105 = {
	name=character_name_10000105 # Tadasue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000106
	prowess=11
	1450.1.2={
		birth=yes
	}
	1466.1.1={
		give_nickname="nick_taro"
	}
	1511.8.19={
		death=yes
	}
}
10000106 = {
	name=character_name_10000106 # Masasue
	dynasty=10000001 # Ando
	father=10000118
	religion="shinto"
	culture="dewa"
	prowess=11
	1420.1.2={
		birth=yes
	}
	1436.1.1={
		give_nickname="nick_taro"
	}
	1488.9.15={
		death=yes
	}
}
10000107 = {
	name=character_name_10000107 # Nobusue
	dynasty=10000001 # Ando
	father=10000108
	religion="shinto"
	culture="dewa"
	prowess=11
	1470.1.1={
		birth=yes
	}
	1533.9.25={
		death=yes
	}
}
10000108 = {
	name=character_name_10000108 # Munesue
	dynasty=10000001 # Ando
	father=10000109
	religion="shinto"
	culture="dewa"
	prowess=11
	1450.1.1={
		birth=yes
	}
	1514.8.11={
		death=yes
	}
}
10000109 = {
	name=character_name_10000109 # Akisue
	dynasty=10000001 # Ando
	father=10000110
	religion="shinto"
	culture="dewa"
	prowess=12
	1430.1.1={
		birth=yes
	}
	1491.7.6={
		death=yes
	}
}
10000110 = {
	name=character_name_10000110 # Koresue
	dynasty=10000001 # Ando
	father=10000111
	religion="shinto"
	culture="dewa"
	prowess=9
	1400.1.1={
		birth=yes
	}
	1462.8.30={
		death=yes
	}
}
10000111 = {
	name=character_name_10000111 # Narisue
	dynasty=10000001 # Ando
	father=10000112
	religion="shinto"
	culture="dewa"
	prowess=10
	1380.1.1={
		birth=yes
	}
	1445.5.17={
		death=yes
	}
}
10000112= {
	name=character_name_10000112 # Kanosue
	dynasty=10000001 # Ando
	father=10000113
	religion="shinto"
	culture="dewa"
	prowess=10
	1365.1.1={
		birth=yes
	}
	1423.7.23={
		death=yes
	}
}
10000113= {
	name=character_name_10000113 # Norisue
	dynasty=10000001 # Ando
	father=12030300 # Ando Morosue
	religion="shinto"
	culture="dewa"
	prowess=11
	1340.1.1={
		birth=yes
	}
	1400.1.1={
		death=yes
	}
}
10000114= {
	name=character_name_10000114 # Yoshisue
	dynasty=10000001 # Ando
	father=10000115
	religion="shinto"
	culture="dewa"
	prowess=9
	1420.1.1={
		birth=yes
	}
	1453.1.1={
		death=yes
	}
}
10000115= {
	name=character_name_10000115 # Yasusue
	dynasty=10000001 # Ando
	father=10000116
	religion="shinto"
	culture="dewa"
	prowess=11
	1390.1.1={
		birth=yes
	}
	1441.7.12={
		death=yes
	}
}
10000116= {
	name=character_name_10000116 # Morisue
	dynasty=10000001 # Ando
	father=10000113
	religion="shinto"
	culture="dewa"
	prowess=11
	1360.1.1={
		birth=yes
	}
	1414.2.20={
		death=yes
	}
}
10000117= {
	name=character_name_10000117 # Michisada
	dynasty=10000001 # Ando
	father=10000113
	religion="shinto"
	culture="dewa"
	prowess=11
	1380.1.1={
		birth=yes
	}
	1445.11.8={
		death=yes
	}
}
10000118= {
	name=character_name_10000118 # Shigesue
	dynasty=10000001 # Ando
	father=10000117
	religion="shinto"
	culture="dewa"
	prowess=10
	1405.1.1={
		birth=yes
	}
	1451.2.28={
		death=yes
	}
}
10000119= {
	name=character_name_10000119 # Iemasa #At the time of Kosamayn war, ruler of Mobetsu
	dynasty=10001670 # Ando
	father=10000118
	religion="shinto"
	culture="dewa"
	prowess=11
	1430.1.1={
		birth=yes
	}
	1495.7.28={
		death=yes
	}
}
10000120= {
	name=character_name_10000120 # Iesue
	dynasty=10001670 # Ando
	father=10000119
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=10
	1460.1.1={
		birth=yes
	}
	1495.1.1={
		death=yes
	}
}
10000121= {
	name=character_name_10000121 # Morosue #In 1525,His clan has escaped to Otate from Mobetsu by Ainu's attack
	dynasty=10001670 # Ando
	father=10000120
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=12
	1490.1.1={
		birth=yes
	}
	1555.1.1={
		add_spouse=10159013
	}
	1563.10.12={
		death=yes
	}
}
10000122= {
	name=character_name_10000122 # Shigesue
	dynasty=10001670 # Ando
	father=10000121
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=11
	1520.1.1={
		birth=yes
	}
	1555.1.1={
		add_spouse=10159014
	}
	1596.3.28={
		death=yes
	}
}
10000123= {
	name=character_name_10000123 # Norisue# In 1428,He esaped to Ezochi from Nambu Moriyuki
	dynasty=10000001 # Ando
	father=10000116
	religion="shinto"
	culture="dewa"
	prowess=11
	1390.1.1={
		birth=yes
	}
	1430.1.1={
		death=yes
	}
}
10000124= {
	name=character_name_10000124 # Sadasue#At the time of Kosamayn war, ruler of Mobetsu
	dynasty=10001671 # Shimokuni
	father=10000115
	religion="shinto"
	culture="dewa"
	prowess=11
	1430.1.1={
		birth=yes
	}
	1485.1.1={
		death=yes
	}
}
10000125= {
	name=character_name_10000125 # Tsunesue
	dynasty=10001671 # Shimokuni
	father=10000124
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=11
	1460.1.1={
		birth=yes
	}
	1486.1.1={
		add_spouse=10159006
	}
	1496.12.30={
		death=yes
	}
}
10000126 = {
	name=character_name_10000126 # Munesue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000101
	prowess=12
	1510.1.1={
		birth=yes
	}
	1531.1.11={
		death=yes
	}
}
10000127= {
	name=character_name_10000127 # Masako #Wife of Takeda Nobuhiro, Mother of Kakizaki Mitsuhiro
	dynasty=10000001 # Ando
	female=yes
	father=10000106
	religion="shinto"
	culture="dewa"
	prowess=12
	1438.1.1={
		birth=yes
	}
	1510.1.1={
		death=yes
	}
}
10000128= {
	name=character_name_10000128 # Naosue
	dynasty=10001670 # Ando
	father=10000121
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=11
	1530.1.1={
		birth=yes
	}
	1560.1.1={
		add_spouse=10159015
	}
	1590.1.1={
		death=yes
	}
}
10000129= {
	name=character_name_10000129 # Yoshisue
	dynasty=10001670 # Ando
	father=10000128
	mother=10159015
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=9
	1561.1.1={
		birth=yes
	}
	1594.1.1={
		death=yes
	}
}
10000130= {
	name=character_name_10000130 # Morisue
	dynasty=10001670 # Ando
	father=10000128
	mother=10159015
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=10
	1563.1.1={
		birth=yes
	}
	1620.1.1={
		death=yes
	}
}
10000131= {
	name=character_name_10000131 # Hirosue
	dynasty=10001670 # Ando
	father=10000128
	mother=10159015
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=12
	1565.1.1={
		birth=yes
	}
	1620.1.1={
		death=yes
	}
}
10000132= {
	name=character_name_10000132 # Sueko #Sato Suehira's wife
	dynasty=10001670 # Ando
	female=yes
	father=10000128
	mother=10159015
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=10
	1562.1.1={
		birth=yes
	}
	1620.1.1={
		death=yes
	}
}
10000133= {
	name=character_name_10000133 # Sakura #Maeda Kan'uemon 's wife
	dynasty=10001670 # Ando
	female=yes
	father=10000128
	mother=10159015
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=11
	1564.1.1={
		birth=yes
	}
	1630.1.1={
		death=yes
	}
}
10000134= {
	name=character_name_10000134 # Yukari #Matsumae Morihiro's wife
	dynasty=10001670 # Ando
	female=yes
	father=10000128
	mother=10159015
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=11
	1570.1.1={
		birth=yes
	}
	1636.1.1={
		death=yes
	}
}
10000135= {
	name=character_name_10000135 # Yoshisue
	dynasty=10001670 # Ando
	father=10000129
	religion="shinto"
	culture="hokkaido_wajin"
	prowess=11
	1589.1.1={
		birth=yes
	}
	1660.9.3={
		death=yes
	}
}
10000136 = {
	name=character_name_10000136 # Chikasue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000102 # Ando Kiyosue
	mother=10000104 # Ando Takako, daughter of Ando Takasue
	diplomacy=7
	martial=6
	stewardship=4
	intrigue=4
	learning=6
	trait="education_diplomacy_4"
	trait="intellect_good_2"
	trait="just"
	prowess=11
	1539.1.1={
		birth=yes
	}
	1555.1.1={
		give_nickname="nick_taro"
	}
	1587.10.2={
		death=yes
	}
}
10000137 = {
	name=character_name_10000137 # Shigesue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000102 # Ando Kiyosue
	mother=10000104 # Ando Takako, daughter of Ando Takasue
	diplomacy=4
	martial=4
	stewardship=5
	intrigue=7
	learning=6
	trait="education_diplomacy_1"
	prowess=11
	1540.1.1={
		birth=yes
	}
	1579.7.16={
		death=yes
	}
}
10000138 = {
	name=character_name_10000138 # Tomosue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000102 # Ando Kiyosue
	mother=10000104 # Ando Takako, daughter of Ando Takasue
	prowess=11
	1545.1.1={
		birth=yes
	}
	1585.1.1={
		death=yes
	}
}
10000139 = {
	name=character_name_10000139 # Suetaka
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000102 # Ando Kiyosue
	mother=10000104 # Ando Takako, daughter of Ando Takasue
	prowess=11
	1547.1.1={
		birth=yes
	}
	1590.1.1={
		death=yes
	}
}
10000140 = {
	name=character_name_10000140 # Suekata
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000102 # Ando Kiyosue
	mother=10000104 # Ando Takako, daughter of Ando Takasue
	prowess=12
	1549.1.1={
		birth=yes
	}
	1595.1.1={
		death=yes
	}
}
10000141 = {
	name=character_name_10000141 # Narisue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000136
	prowess=12
	1574.1.1={
		birth=yes
	}
	1580.1.1={
		death=yes
	}
}
10000142 = {
	name=character_name_10000142 # Sanesue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000136
	prowess=11
	1576.1.1={
		birth=yes
	}
	1660.1.1={
		death=yes
	}
}
10000143 = {
	name=character_name_10000143 # Hidesue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000136
	prowess=10
	1581.1.1={
		birth=yes
	}
	1640.1.1={
		death=yes
	}
}
10000144 = {
	name=character_name_10000144 # Katsusue
	dynasty=10000001 # Ando
	religion="shinto"
	culture="dewa"
	father=10000136
	prowess=10
	1582.1.1={
		birth=yes
	}
	1632.1.1={
		death=yes
	}
}
10000145 = {
	name=character_name_10000145 # Michisue
	dynasty=10000001 # Ando
	father=10000137
	religion="shinto"
	culture="dewa"
	diplomacy=5
	martial=6
	stewardship=6
	intrigue=8
	learning=4
	trait="education_intrigue_2"
	trait="callous"
	trait="ambitious"
	prowess=10
	1564.1.1={
		birth=yes
	}
	1614.1.1={
		death=yes
	}
}
10000201 = {
	name=character_name_10000201 # Michimori
	dynasty=10000002 # Tozawa
	martial=5
	diplomacy=5
	intrigue=6
	stewardship=5
	religion="shinto"
	culture="dewa"
	father=10000202
	trait="education_diplomacy_1"
	trait="weak"
	prowess=11
	1524.1.1={
		birth=yes
	}
	1604.7.1={
		death=yes
	}
}
10000202 = {
	name=character_name_10000202 # Hidemori
	dynasty=10000002 # Tozawa
	religion="shinto"
	culture="dewa"
	father=10000204
	prowess=9
	1466.1.1={
		birth=yes
	}
	1529.1.1={
		death=yes
	}
}
10000203 = {
	name=character_name_10000203 # Tadamori
	dynasty=10000002 # Tozawa
	religion="shinto"
	culture="dewa"
	father=10000204
	prowess=11
	1470.1.1={
		birth=yes
	}
	1545.5.24={
		death=yes
	}
}
10000204 = {
	name=character_name_10000204 # Masamori
	dynasty=10000002 # Tozawa
	religion="shinto"
	culture="dewa"
	father=10000205
	prowess=10
	1445.1.1={
		birth=yes
	}
	1500.1.1={
		death=yes
	}
}
10000205 = {
	name=character_name_10000205 # Sumimori
	father=12031120 # Tozawa Hisamori
	dynasty=10000002 # Tozawa
	religion="shinto"
	culture="dewa"
	prowess=10
	1430.1.1={
		birth=yes
	}
	1470.1.1={
		death=yes
	}
}
10000206 = {
	name=character_name_10000206 # Morishige
	dynasty=10000002 # Tozawa
	religion="shinto"
	culture="dewa"
	father=10000201
	diplomacy=5
	martial=6
	stewardship=6
	intrigue=6
	learning=6
	trait="education_intrigue_2"
	trait="weak"
	trait="callous"
	prowess=12
	1551.1.1={
		birth=yes
	}
	1592.1.1={
		death=yes
	}
}
10000207 = {
	name=character_name_10000207 # Moriyasu
	dynasty=10000002 # Tozawa
	religion="shinto"
	culture="dewa"
	father=10000201
	diplomacy=6
	martial=6
	stewardship=7
	intrigue=7
	learning=6
	trait="education_martial_3"
	trait="intellect_good_2"
	trait="brave"
	trait="compassionate"
	trait="organizer"
	prowess=15
	1566.2.1={
		birth=yes
	}
	1590.7.7={
		death=yes
	}
}
10000208 = {
	name=character_name_10000208 # Mitsumori
	dynasty=10000002 # Tozawa
	trait="weak"
	religion="shinto"
	culture="dewa"
	father=10000201
	prowess=9
	1577.1.1={
		birth=yes
	}
	1592.4.26={
		death=yes
	}
}
10000209 = {
	name=character_name_10000209 # Masamori
	dynasty=10000002 # Tozawa
	religion="shinto"
	culture="dewa"
	father=10000207
	prowess=11
	1585.1.1={
		birth=yes
	}
	1648.3.16={
		death=yes
	}
}
10000301 = {
	name=character_name_10000301 # Tanemichi
	dynasty=10000003 # Onodera
	father=10000303
	martial=5
	diplomacy=5
	intrigue=7
	stewardship=6
	trait="just"
	trait="cynical"
	trait="education_intrigue_2"
	religion="shinto"
	culture="dewa"
	prowess=12
	1487.1.1={
		birth=yes
	}
	1546.6.25={
		death=yes
	}
}
10000302 = {
	name=character_name_10000302 # Kagemichi
	dynasty=10000003 # Onodera
	martial=5
	diplomacy=6
	intrigue=7
	stewardship=4
	religion="shinto"
	culture="dewa"
	father=10000301
	prowess=9
	1534.1.1={
		birth=yes
	}
	1597.1.1={
		death=yes
	}
}
10000303 = {
	name=character_name_10000303 # Yasumichi
	dynasty=10000003 # Onodera
	father=12001031
	religion="shinto"
	culture="dewa"
	prowess=9
	1403.1.1={
		birth=yes
	}
	1487.1.1={
		death=yes
	}
}
10000304 = {
	name=character_name_10000304 # Mitsumichi
	dynasty=10000003 # Onodera
	religion="shinto"
	culture="dewa"
	father=10000302
	prowess=11
	1560.1.1={
		birth=yes
	}
	1580.1.1={
		death=yes
	}
}
10000305 = {
	name=character_name_10000305 # Yoshimichi
	dynasty=10000003 # Onodera
	religion="shinto"
	culture="dewa"
	father=10000302
	prowess=10
	1566.8.19={
		birth=yes
	}
	1646.1.8={
		death=yes
	}
}
10000306 = {
	name=character_name_10000306 # Yasumichi
	dynasty=10000003 # Onodera
	religion="shinto"
	culture="dewa"
	father=10000302
	prowess=11
	1570.1.1={
		birth=yes
	}
	1620.1.1={
		death=yes
	}
}
10000401 = {
	name=character_name_10000401 # Noriyori
	dynasty=10000004 # Asari
	father=10000404
	martial=8
	diplomacy=6
	intrigue=6
	stewardship=5
	religion="shinto"
	culture="dewa"
	prowess=11
	1499.1.1={
		birth=yes
	}
	1550.7.31={
		death=yes
	}
}
10000402 = {
	name=character_name_10000402 # Norisuke
	dynasty=10000004 # Asari
	martial=7
	diplomacy=6
	intrigue=5
	stewardship=8
	religion="shinto"
	culture="dewa"
	father=10000401
	prowess=12
	1523.1.1={
		birth=yes
	}
	1562.1.1={
		death=yes
	}
}
10000403 = {
	name=character_name_10000403 # Katsuyori
	dynasty=10000004 # Asari
	martial=5
	diplomacy=6
	intrigue=6
	stewardship=6
	trait="callous"
	religion="shinto"
	culture="dewa"
	father=10000401
	prowess=11
	1529.1.1={
		birth=yes
	}
	1582.6.7={
		death=yes
	}
}
10000404 = {
	name=character_name_10000404 # Noriaki
	dynasty=10000004 # Asari
	religion="shinto"
	culture="dewa"
	father=12010008 # Asari Yoichi
	prowess=11
	1460.1.1={
		birth=yes
	}
	1525.1.1={
		death=yes
	}
}
10000405 = {
	name=character_name_10000405 # Yorihira
	dynasty=10000004 # Asari
	religion="shinto"
	culture="dewa"
	father=10000403
	prowess=10
	1560.1.1={
		birth=yes
	}
	1598.2.13={
		death=yes
	}
}
10000501 = {
	name=character_name_10000501 # Ujifusa
	dynasty=10000006 # Daihoji
	father=10000602
	martial=7
	diplomacy=5
	intrigue=5
	stewardship=5
	religion="shinto"
	culture="dewa"
	prowess=11
	1490.1.1={
		birth=yes
	}
	1518.1.1={
		dynasty=10000005 # Sagoshi
	}
	1588.1.1={
		death=yes
	}
}
10000502 = {
	name=character_name_10000502 # Ujitaka
	dynasty=10000005 # Sagoshi
	father=10000504 # Sagoshi Ujimasu
	religion="shinto"
	culture="dewa"
	prowess=9
	1450.1.1={
		birth=yes
	}
	1513.10.29={
		death=yes
	}
}
10000503={
	name=character_name_10000503 # Ujinobu
	dynasty=10000005 # Sagoshi
	religion="shinto"
	culture="dewa"
	prowess=9
	1390.1.1={
		birth=yes
	}
	1443.1.1={
		death=yes
	}
}
10000504={
	name=character_name_10000504 # Ujimasu
	dynasty=10000005 # Sagoshi
	father=10000503 # Sagoshi Ujinobu
	religion="shinto"
	culture="dewa"
	prowess=12
	1420.1.1={
		birth=yes
	}
	1478.1.1={
		death=yes
	}
}
10000550 = {
	name=character_name_10000550 # Shigekiyo
	dynasty=10000012 # Nikaho
	father=10000553
	religion="shinto"
	culture="dewa"
	prowess=12
	1562.1.1={
		birth=yes
	}
	1583.1.1={
		death=yes
	}
}
10000551 = {
	name=character_name_10000551 # Kiyoharu
	religion="shinto"
	culture="dewa"
	prowess=11
	1568.1.1={
		birth=yes
	}
	1583.1.1={
		dynasty=10000012 # Nikaho
	}
	1586.1.1={
		death=yes
	}
}
10000552 = {
	name=character_name_10000552 # Mitsushige
	religion="shinto"
	culture="dewa"
	prowess=11
	1560.1.1={
		birth=yes
	}
	1586.1.1={
		dynasty=10000012 # Nikaho
	}
	1624.4.1={
		death=yes
	}
}
10000553 = {
	name=character_name_10000553 # Kiyonaga
	dynasty=10000012 # Nikaho
	father=10000556 # Nikaho Kiyoihisa
	religion="shinto"
	culture="dewa"
	prowess=11
	1540.1.1={
		birth=yes
	}
	1577.1.1={
		death=yes
	}
}
10000555={
	name=character_name_10000555 # Kiyomasa
	dynasty=10000012 # Nikaho
	father=12361070 # Oi Tomokiyo
	religion="shinto"
	culture="dewa"
	prowess=12
	1480.1.1={
		birth=yes
	}
	1540.1.1={
		death=yes
	}
}
10000556={
	name=character_name_10000556 # Kiyohisa
	dynasty=10000012 # Nikaho
	father=10000555 # Nikaho Kiyomasa
	religion="shinto"
	culture="dewa"
	prowess=11
	1523.1.1={
		birth=yes
	}
	1576.1.1={
		death=yes
	}
}
10000601={
	name=character_name_10000601 # Takeuji
	dynasty=10000006 # Daihoji
	father=12010140 # Daihoji Kiyouji
	religion="shinto"
	culture="dewa"
	prowess=11
	1425.1.1={
		birth=yes
	}
	1475.1.1={
		death=yes
	}
}
10000602 = {
	name=character_name_10000602 # Masauji
	dynasty=10000006 # Daihoji
	father=10000601 # Daihoji Takeuji
	religion="shinto"
	culture="dewa"
	prowess=11
	1445.1.1={
		birth=yes
	}
	1495.1.1={
		death=yes
	}
}
10000603 = {
	name=character_name_10000603 # Sumiuji
	dynasty=10000006 # Daihoji
	father=10000602 # Daihoji Masauji
	religion="shinto"
	culture="dewa"
	prowess=10
	1475.1.1={
		birth=yes
	}
	1510.1.1={
		death=yes
	}
}
10000604 = {
	name=character_name_10000604 # Ujitoki
	dynasty=10000006 # Daihoji
	father=10000602 # Daihoji Masauji
	religion="shinto"
	culture="dewa"
	prowess=9
	1480.1.1={
		birth=yes
	}
	1518.1.1={
		death=yes
	}
}
10000605 = {
	name=character_name_10000605 # Kurou
	dynasty=10000006 # Daihoji
	father=10000602 # Daihoji Masauji
	religion="shinto"
	culture="dewa"
	prowess=9
	1480.1.1={
		birth=yes
	}
	1530.1.1={
		death=yes
	}
}
10000606 = {
	name=character_name_10000606 # Harutoki
	dynasty=10000006 # Daihoji
	father=10000604 # Daihoji Ujitoki
	martial=4
	diplomacy=7
	intrigue=6
	stewardship=6
	religion="shinto"
	culture="dewa"
	prowess=11
	1512.1.1={
		birth=yes
	}
	1541.1.1={
		death=yes
	}
}
10000607 = {
	name=character_name_10000607 # Yoshimasu
	dynasty=10000006 # Daihoji
	father=10000605 # Daihoji Kurou
	martial=7
	diplomacy=7
	intrigue=5
	stewardship=6
	religion="shinto"
	culture="dewa"
	prowess=9
	1522.1.1={
		birth=yes
	}
	1569.1.1={
		death=yes
	}
}
10000608 = {
	name=character_name_10000608 # Yoshiuji
	dynasty=10000006 # Daihoji
	father=10000607 # Daihoji Yoshimasu
	religion="shinto"
	culture="dewa"
	diplomacy=7
	martial=6
	stewardship=7
	intrigue=5
	learning=8
	trait="education_martial_3"
	trait="greedy"
	trait="ambitious"
	trait="arrogant"
	prowess=12
	1551.1.1={
		birth=yes
	}
	1583.4.27={
		death=yes
	}
}
10000609 = {
	name=character_name_10000609 # Yoshioki
	dynasty=10000006 # Daihoji
	father=10000607 # Daihoji Yoshimasu
	religion="shinto"
	culture="dewa"
	diplomacy=7
	martial=7
	stewardship=6
	intrigue=6
	learning=4
	trait="education_learning_2"
	prowess=12
	1554.1.1={
		birth=yes
	}
	1587.12.13={
		death=yes
	}
}
10000701 = {
	name=character_name_10000701 # Yoshimori
	dynasty=10000007 # Mogami
	father=12015023 # Nakano Yoshikiyo
	religion="shinto"
	culture="dewa"
	martial=6
	diplomacy=5
	intrigue=5
	stewardship=6
	learning=4
	trait="ashikaga_family"
	trait="education_diplomacy_3"
	prowess=9
	1521.1.2={
		birth=yes
	}
	1522.1.2={
		dynasty=10000007 # Mogami
		trait="child_of_adoption"
		effect={
			set_realm_capital=title:c_nmih_mogami
		}
	}
	1590.5.18={
		death=yes
	}
}
10000703 = {
	name=character_name_10000703 # Hisako
	female=yes
	dynasty=10000010 # Date
	religion="shinto"
	culture="southern_oshu"
	father=12071045 # Date Hisamune
	mother=10001015 # Sekisuiin
	prowess=10
	1495.1.1={
		birth=yes
	}
	1545.1.1={
		death=yes
	}
}
10000716 = {
	name=character_name_10000716 # Yoshiaki # the 11th header of the Mogami family.
	dynasty=10000007 # Mogami
	religion="shinto"
	culture="dewa"
	father=10000701 # Mogami Yoshimori, the 10th header of the Mogami family.
	diplomacy=5
	martial=7
	stewardship=6
	intrigue=7
	learning=5
	trait="education_martial_3"
	trait="intellect_good_2"
	trait="ambitious"
	trait="just"
	trait="patient"
	trait="strong"
	trait="poet"
	trait="zealous"
	trait="organizer"
	prowess=13
	1546.2.1={
		birth=yes
	}
	1567.1.1={
		add_spouse=10006221
	}
	1574.9.10={
		effect={
			set_realm_capital=title:c_nmih_mogami
		}
	}
	1611.3.1={
		trait="jushii_ge"
	}
	1614.2.26={
		death=yes
	}
}
10000717 = {
	name=character_name_10000717 # Mitsunao # Tateoka Mitsunao
	dynasty=10000007 # Mogami
	religion="shinto"
	culture="dewa"
	father=10000701 # Mogami Yoshimori, the 10th header of the Mogami family.
	prowess=12
	1559.1.1={
		birth=yes
	}
	1629.7.11={
		death=yes
	}
}
10000718 = {
	name=character_name_10000718 # Yoshi
	female=yes
	dynasty=10000007 # Mogami
	religion="shinto"
	culture="dewa"
	father=10000701 # Mogami Yoshimori, the 10th header of the Mogami family.
	diplomacy=6
	martial=7
	stewardship=5
	intrigue=7
	learning=7
	trait="education_diplomacy_4"
	trait="poet"
	prowess=11
	1548.1.1={
		birth=yes
	}
	1623.8.13={
		death=yes
	}
}
10000719={
	name=character_name_10000719 # Take
	female=yes
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	mother=10006221 # Osaki Myoei
	religion="shinto"
	culture="dewa"
	prowess=9
	1570.1.1={
		birth=yes
	}
	1620.1.1={
		death=yes
	}
}
10000720={
	name=character_name_10000720 # Yoshiyasu
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	mother=10006221 # Osaki Myoei
	religion="shinto"
	culture="dewa"
	diplomacy=4
	martial=6
	stewardship=5
	intrigue=5
	learning=4
	prowess=10
	1575.1.1={
		birth=yes
	}
	1603.9.21={
		death={
			death_reason=death_murder
		}
	}
}
10000721={
	name=character_name_10000721 # Matsuo
	female=yes
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	mother=10006221 # Osaki Myoei
	religion="shinto"
	culture="dewa"
	prowess=12
	1578.1.1={
		birth=yes
	}
	1606.1.1={
		death=yes
	}
}
10000722={
	name=character_name_10000722 # Koma
	female=yes
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	mother=10006221 # Osaki Myoei
	religion="shinto"
	culture="dewa"
	trait="beauty_good_2"
	prowess=11
	1581.1.1={
		birth=yes
	}
	1595.9.5={
		death={
			death_reason=death_execution
		}
	}
}
10000723={
	name=character_name_10000723 # Iechika
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	mother=10006221 # Osaki Myoei
	religion="shinto"
	culture="dewa"
	prowess=10
	1582.1.1={
		birth=yes
	}
	1617.4.11={
		death=yes
	}
}
10000724={
	name=character_name_10000724 # Yoshichika
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	religion="shinto"
	culture="dewa"
	prowess=11
	1582.1.1={
		birth=yes
	}
	1614.11.14={
		death={
			death_reason=death_suicide
		}
	}
}
10000725={
	name=character_name_10000725 # Yoshitada
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	religion="shinto"
	culture="dewa"
	prowess=9
	1588.1.1={
		birth=yes
	}
	1665.1.29={
		death=yes
	}
}
10000726={
	name=character_name_10000726 # Yoshinao
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	religion="shinto"
	culture="dewa"
	prowess=9
	1594.1.1={
		birth=yes
	}
	1622.1.1={
		death={
			death_reason=death_suicide
		}
	}
}
10000727={
	name=character_name_10000727 # Akitaka
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	religion="shinto"
	culture="dewa"
	prowess=10
	1602.1.1={
		birth=yes
	}
	1623.1.1={
		death={
			death_reason=death_suicide
		}
	}
}
10000728={
	name=character_name_10000728 # Kiku
	female=yes
	dynasty=10000007 # Mogami
	father=10000716 # Mogami Yoshiaki
	religion="shinto"
	culture="dewa"
	prowess=10
	1610.1.1={
		birth=yes
	}
	1664.1.1={
		death=yes
	}
}
10000801 = {
	name=character_name_10000801 # Yoshitaka
	dynasty=10000008 # Shimizu
	father=12015153 # Shimizu Yoshinori
	martial=7
	diplomacy=6
	intrigue=6
	stewardship=6
	religion="shinto"
	culture="dewa"
	trait="generous"
	trait="education_intrigue_3"
	prowess=14
	1520.1.1={
		birth=yes
	}
	1565.06.29={
		death={
			death_reason=death_battle
			killer=10000607
		}
	}
}
10000802={
	name=character_name_10000802 # Yoshiuji
	dynasty=10000008 # Shimizu
	father=10000801 # Shimizu Yoshitaka
	religion="shinto"
	culture="dewa"
	prowess=11
	1547.1.1={
		birth=yes
	}
	1563.1.1={
		give_nickname="nick_Magosaburo"
	}
	1586.11.28={
		death=yes
	}
}
10000803={
	female=yes
	name=character_name_10000803 # Tatsu
	dynasty=10000008 # Shimizu
	father=10000802 # Shimizu Yoshiuji
	religion="shinto"
	culture="dewa"
	prowess=10
	1577.1.1={
		birth=yes
	}
	1638.8.20={
		death=yes
	}
}
10000850={
	name=character_name_10000850 # Mitsushige
	dynasty=10000013 # Tateoka
	father=12015132 # Tateoka Yoshisato
	religion="shinto"
	culture="dewa"
	martial=4
	diplomacy=5
	intrigue=5
	prowess=10
	1547.1.1={
		birth=yes
	}
	1563.1.1={
		give_nickname="nick_buzen_no_kami"
	}
	1639.1.1={
		death=yes
	}
}
10000851={
	name=character_name_10000851 # Mitsuhiro
	dynasty=10000013 # Tateoka
	father=12015132 # Tateoka Yoshisato
	religion="shinto"
	culture="dewa"
	prowess=10
	1560.1.1={
		birth=yes
	}
	1576.1.1={
		give_nickname="nick_nagato_no_kami"
	}
	1663.1.1={
		death=yes
	}
}
10000852={
	name=character_name_10000852 # Takashige
	dynasty=10000013 # Tateoka
	father=12015132 # Tateoka Yoshisato
	religion="shinto"
	culture="dewa"
	prowess=11
	1565.1.1={
		birth=yes
	}
	1581.1.1={
		give_nickname="nick_shinano_no_kami"
	}
	1605.1.1={
		death=yes
	}
}
10000860={
	name=character_name_10000860 # Michitada
	dynasty=10000014 # Narisawa
	father=12015141 # Narisawa Yoshikiyo
	religion="shinto"
	culture="dewa"
	martial=7
	diplomacy=6
	intrigue=5
	prowess=11
	1540.1.1={
		birth=yes
	}
	1617.1.1={
		death=yes
	}
}
10000870={
	name=character_name_10000870 # Mitsushige
	dynasty=10000017 # Nobesawa
	religion="shinto"
	culture="dewa"
	prowess=11
	1500.1.1={
		birth=yes
	}
	1510.1.1={
		employer = 12015209 # Tendo Yorimichi
	}
	1530.1.1={
		employer = 10000901 # Tendo Yorinaga
	}
	1560.1.1={
		death=yes
	}
}
10000871={
	name=character_name_10000871 # Mitsunobu
	dynasty=10000017 # Nobesawa
	father=10000870 # Nobesawa Mitsushige
	religion="shinto"
	culture="dewa"
	martial=7
	trait="education_martial_2"
	trait="strong"
	prowess=13
	1544.1.1={
		birth=yes
	}
	1554.1.1={
		employer = 10000901 # Tendo Yorinaga
	}
	1564.1.1={
		employer = 10000902 # Tendo Yorisada
	}
	1584.1.1={
		employer = 10000716 # Mogami Yoshiaki
	}
	1591.5.7={
		death=yes
	}
}
10000880={
	name=character_name_10000880 # Hidetsuna
	dynasty=10000018 # Sakenobe
	religion="shinto"
	culture="dewa"
	martial=7
	trait="education_martial_3"
	trait="brave"
	prowess=14
	1563.1.1={
		birth=yes
	}
	1581.1.1={
		employer = 10000716 # Mogami Yoshiaki
	}
	1646.8.2={
		death=yes
	}
}
10000901 = {
	name=character_name_10000901 # Yorinaga
	dynasty=10000009 # Tendo
	martial=7
	diplomacy=7
	intrigue=5
	stewardship=6
	religion="shinto"
	culture="dewa"
	father=12015209 # Tendo Yorimichi
	prowess=12
	1499.1.1={
		birth=yes
	}
	1564.1.1={
		death=yes
	}
}
10000902 = {
	name=character_name_10000902 # Yorisada
	dynasty=10000009 # Tendo
	martial=7
	diplomacy=6
	intrigue=7
	stewardship=6
	religion="shinto"
	culture="dewa"
	father=12015209 # Tendo Yorimichi
	prowess=10
	1533.1.1={
		birth=yes
	}
	1583.1.1={
		death=yes
	}
}
10000907 = {
	name=character_name_10000907 # Yorizumi
	dynasty=10000009 # Tendo
	religion="shinto"
	culture="dewa"
	father=10000902
	prowess=9
	1558.1.1={
		birth=yes
	}
	1600.1.1={
		death=yes
	}
}
10000908 = {
	name=character_name_10000908 # Yorikage #a.k.a. Higashine Yorikage
	dynasty=10000009 # Tendo
	religion="shinto"
	culture="dewa"
	father=10000902
	prowess=10
	1560.1.1={
		birth=yes
	}
	1581.6.11={
		death=yes
	}
}