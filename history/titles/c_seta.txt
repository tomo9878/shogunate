c_nmih_seta = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1336.3.10 = { change_development_level = 40 }
	1563.1.1 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_kozuke"
	}
	1110.1.1={
		liege="d_nmih_kozuke"
	    holder=12180104 # Ogo Shigetoshi
	}
	1124.1.1={
		holder=12180300 # Ogo Narichika
	}
	1160.1.1={
		holder=12180310 # Ogo Takayoshi
	}
	1180.1.1={
		liege="d_nmih_ashikaga"
	}
	1180.10.26={
		liege="k_nmih_western_kanto" # Minamoto Yoritomo
	}
	1192.7.12={
		liege="e_nmih_japan" # Minamoto Yoritomo
	}
	1200.1.1={
		liege="d_nmih_kozuke"
		holder=12180320 # Ogo Sanehide
	}
	1246.1.1={
		liege=0
		holder=0
	}
	1336.3.10={
		liege="d_nmih_kozuke"
		holder=12215325 # Nagao Kagetada
	}
	1370.1.1={
		holder=10017311 # Nagao Kageyuki
	}
	1390.1.1={
		holder=10017310 # Nagao Tadatsuna
	}
	1410.1.1={
		holder=10017314 # Nagao Tadamasa
	}
	1450.11.17={
		holder=10017322 # Nagao Tadakage
	}
	1501.8.5={
		holder=10017328 # Nagao Akitada
	}
	1509.10.29={
		holder=10017331 # Nagao Akikata
	}
	1524.1.1={
		holder=10017301 # Nagao Akikage
	}
	1527.1.1={
		holder=10017300 # Nagao Kagetaka
	}
	1552.3.1={
		liege=0
		holder=10017303 # Nagao Kagefusa
	}
	1560.11.1={
		liege="d_nmih_echigo" # 32011 Uesugi Kenshin
	}
	1561.5.10={
		liege="k_nmih_kanto_kanrei" # 32011 Uesugi Kenshin
	}
	1563.1.1={
		holder=10032523 # Kitajo Takahiro
	}
	1567.1.1={
		liege=0
	}
	1569.1.1={
		liege="d_nmih_echigo" # 32011 Uesugi Kenshin
	}
	1578.4.19={
		liege="d_nmih_kozuke" # Uesugi Kagetora
	}
	1579.4.19={
		liege=0
	}
	1579.8.1={
		liege="d_nmih_kai"
	}
	1582.4.3={
		liege=0
	}
	1582.4.15={
		liege="d_nmih_kozuke"
		holder=10029330 # Takigawa Kazumasu
	}
	1582.7.8={
		liege="d_nmih_kozuke"
		holder=10032523 # Kitajo Takahiro
	}
	1582.11.14={
		liege="d_nmih_echigo"
	}
	1583.9.1={
		liege="d_nmih_kozuke"
		holder=10016016 # Hojo Ujikuni
	}
}
