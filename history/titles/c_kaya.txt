c_nmih_kaya = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1200.1.1 = { change_development_level = 30 }

	0701.1.1={
		liege="d_nmih_bitchu"
	}
	1180.1.1={
		liege="d_nmih_bitchu"
		holder=10260103 # Kaya Sadayori
	}
	1183.8.17={
		liege="k_nmih_western_chugoku" # Taira's escape to the west
	}
	1199.1.1={
		liege=0
		holder=0 # 
	}
	1239.1.1={
		liege="d_nmih_bitchu"
		holder=12630950	# Akiha Shigenobu
	}
	1240.1.1={
		liege="d_nmih_bitchu"
		holder=12630952	# Akiha Shigeto
	}
	1285.1.1={
		liege="d_nmih_bitchu"
		holder=12630953	# Akiha Yoshitsugu
	}
	1290.1.1={
		liege="d_nmih_bitchu"
		holder=12630954	# Akiha Shigetomo
	}
	1355.1.1={
		liege="d_nmih_bitchu"
		holder=12139589	# Kou Morohide
	}
	1362.1.1={
		liege="d_nmih_bitchu"
		holder=12630955	# Akiha Nobumori
	}
	1365.1.1={
		liege="d_nmih_bitchu"
		holder=12630956	# Akiha Shigetsugu
	}
	1367.1.1={
		liege="d_nmih_bitchu"
		holder=12630957	# Akiha Shigeaki
	}
	1370.1.1={
		liege="d_nmih_bitchu"
		holder=12630958	# Akiha Yorishige
	}
	1405.1.1={
		liege="d_nmih_bitchu"
		holder=12630959	# Akiha Yoritsugu
	}
	1440.1.1={
		liege="d_nmih_bitchu"
		holder=12630960	# Akiha Motoaki
	}
	1509.1.1={
		liege=0
		holder=12630961	# Akiha Motoshige
	}
	1509.6.1={
		liege="k_nmih_western_chugoku" # Ouchi Yoshioki
		holder=10055063	# Ueno Yorihisa
	}
	1521.1.1={
		liege=0
		holder = 10055066 # Ueno Yoriuji
	}
	1533.1.1={
		liege=0
		holder = 10054800 # Sho Motosuke
	}
	1540.1.1={
		liege="k_nmih_sanin" # Amago Tsunehisa
		holder = 10054801 # Sho Tamesuke
	}
	1553.2.15={
		liege="k_nmih_sanin" # Amago Tsunehisa
		holder = 10054804 # Sho Takasuke
	}
	1560.7.1={
		liege=0
	}
	1561.1.1={
		liege="k_nmih_sanin" # Amago Haruhisa
		holder=10051508 # Amago Haruhisa
	}
	1561.1.9={
		holder=10051525 # Amago Yoshihisa
	}
	1561.3.1={
		liege="k_nmih_western_chugoku" # Mori Motonari
		holder = 10054900 # Mimura Iechika
	}
	1566.2.24={
		holder = 10054906 # Mimura Motochika
	}
	1568.1.1={
		liege="d_nmih_bizen" # Uragami Munekage
		holder = 10054804 # Sho Takasuke
	}
	1571.2.1={
		liege="k_nmih_western_chugoku" # Mori Motonari
		holder = 10054906 # Mimura Motochika
	}
	1574.10.1={
		liege=0
	}
	1575.7.9={
		liege="k_nmih_western_chugoku" # Mori Terumoto
		holder=10056016 # Kobayakawa Takakage
	}

}
