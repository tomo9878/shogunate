c_nmih_otori = {
	1.1.1 = { change_development_level = 10 }
	0701.1.1 = { change_development_level = 20 }
	1231.1.1 = { change_development_level = 30 }
	1331.1.1 = { change_development_level = 60 }
	1467.1.1 = { change_development_level = 80 }
	1571.1.1 = { change_development_level = 70 }

	0701.1.1={
		liege="d_nmih_izumi"
	}
	1180.1.1={
		liege="d_nmih_izumi" # Taira Nobukane
		holder=10110235 # Taira Nobukane
	}
	1183.8.17={
		liege="d_nmih_izumi" # Minamoto Yukiie
		holder=10120072 # Minamoto Yukiie / Taira's escape to the west
	}
	1185.11.26={
		liege=0
		holder=0 # Failure of Yoshitsune's rebellion
	}
	1440.1.1={
		holder=10042860 # Yukawa Sena
		liege="d_nmih_sakai"
	}
	1483.5.17={
		holder=10042861 # Yukawa Shinbee
	}
	1510.1.1={
		holder=10042890 # Sen (Tanaka) Yohyoe
	}
	1530.1.1={
		holder=10042830 # Takeno Joo
	}
	1555.12.12={
		holder=10042881 # Imai Sokyu
	}
	1570.1.1={
		holder=10042801 # Tsuda Sokyu
	}
	1574.1.1={
		holder=10042891 # Sen Soeki
	}

}
