c_nmih_watarai = {
	1.1.1 = { change_development_level = 30 }
	1000.1.1 = { change_development_level = 30 }
	1331.1.1 = { change_development_level = 50 }
	1569.11.21 = { change_development_level = 50 }

	0701.1.1={
		liege="d_nmih_ise"
	}
	1180.1.1={
		liege="d_nmih_shima"
		holder=12434100 # Onakatomi Chikataka
	}
	1183.8.17={
		liege=0 # Taira's escape to the west
	}
	1187.1.1={
		liege=0
		holder=0
	}
	1429.1.25={
		liege="d_nmih_shima"
		holder=10026006 # Kitabatake Noritomo
	}
	1471.4.13={
		holder=10026005 # Kitabatake Masasato
	}
	1508.12.15={
		holder=10026140 # Tamaru Akiharu
	}
	1526.1.1={
		holder=10026141 # Tamaru Tomotada
	}
	1568.1.1={
		holder=10026142 # Tamaru Naomasa
	}
	1569.11.21={
		liege="d_nmih_shima"
		holder=10029120 # Oda Nobunaga
	}
	1575.1.1={
		holder=10029161 # Oda Nobukatsu
	}

}
