c_nmih_kariwa = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1444.1.1 = { change_development_level = 20 }
	1544.1.1 = { change_development_level = 20 }

	0697.1.1={
		liege="d_nmih_echigo"
	}
	1180.1.1={
		liege=0
		holder=12270210 # Oguni Muneyori
	}
	1181.7.25={
		liege="k_nmih_shinano" # Battle of Yokota-gawara
	}
	1183.6.22={
		liege="k_nmih_hokuriku" # Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan" # Taira's escape to the west
	}
	1184.3.4={
		liege=0 # Battle of Awazu
	}
	1195.1.1={
		liege=0
		holder=0 # 
	}
	1300.1.1={
		liege="d_nmih_echigo"
	}
	1368.10.31={
		holder=10032801 # Uesugi Noriyoshi
	}
	1386.1.1={
		holder=10017015 # Uesugi Fusakata
	}
	1421.12.4={
		holder=10032803 # Uesugi Tomokata
	}
	1422.10.29={
		holder=10032806 # Uesugi Fusatomo
	}
	1449.3.21={
		holder=10032808 # Uesugi Fusasada
	}
	1494.11.14={
		holder=10032811 # Uesugi Fusayoshi
	}
	1506.10.5={
		liege=0
	}
	1507.9.13={
		holder=10032812 # Uesugi Sadazane
	}
	1515.1.1={
		holder=10032800 # Uesugi Sadanori
	}
	1544.1.1={
		liege="d_nmih_echigo" # 32009 Nagao Harukage
		holder=10032523 # Kitajo Takahiro
	}
	1554.1.1={
		liege=0
		holder=10032523 # Kitajo Takahiro
	}
	1555.1.1={
		liege="d_nmih_echigo" # 32011 Uesugi Kenshin
	}
	1567.1.1={
		liege=0 
	}
	1569.1.1={
		liege="d_nmih_echigo" # 32011 Uesugi Kenshin
	}
	1578.4.19={
		liege="d_nmih_kozuke" # Uesugi Kagetora
	}
	1579.2.28={
		liege="d_nmih_echigo" # 32032 Uesugi Kagekatsu
		holder=10032032 # Uesugi Kagekatsu
	}

}
