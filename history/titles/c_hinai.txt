c_nmih_hinai = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1189.10.14 = { change_development_level = 10 }

	0712.9.23={
		liege="k_nmih_dewa"
	}
	1180.1.1={
		liege="k_nmih_northern_oshu" # Fujiwara Hidehira
		holder=12000400 # Kawada Moritsugu
	}
	1189.10.3={
		liege=0
		holder=12000400 # Kawada Moritsugu / Fall of Hiraizumi
	}
	1189.10.14={
		liege="k_nmih_western_kanto"
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		liege="k_nmih_western_kanto"
		holder=10120116 # Asari Yoshito
	}
	1190.1.1={
		liege=0 # Okawa Kaneto's rebellion
		holder=12000011 # Okawa Kaneto
	}
	1190.4.16={
		liege="k_nmih_western_kanto" # End of Okawa Kaneto's rebellion
		holder=10120116 # Asari Yoshito
	}
	1221.1.1={
		holder=12365001 # Asari Tomoyoshi
	}
	1240.1.1={
		holder=12365002 # Asari Yoshikuni
	}
	1265.1.1={
		holder=12010000 # Asari Rokuro
	}
	1290.1.1={
		holder=12010001 # Asari Rokuro
	}
	1315.1.1={
		holder=12010002 # Asari Kiyotsura
	}
	1345.1.1={
		holder=12010003 # Asari Joko
	}
	1370.1.1={
		holder=12010004 # Asari Taro
	}
	1390.1.1={
		holder=12010005 # Asari Shiro
	}
	1420.1.1={
		liege=0
		holder=12010006 # Asari Hyobu
	}
	1445.1.1={
		holder=12010007 # Asari Hyobutaifu
	}
	1470.1.1={
		holder=12010008 # Asari Yoichi
	}
	1490.1.1={
		holder=10000404 # Asari Noriaki
	}
	1525.1.1={
		holder=10000401 # Asari Noriyori
	}
	1550.7.31={
		holder=10000402 # Asari Norisuke
	}
	1562.1.1={
		holder=10000403 # Asari Katsuyori
	}
	1582.6.7={
		holder=10000405 # Asari Yorihira
	}
}
