c_nmih_kasa = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 20 }
	1580.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_tango"
	}
	1179.12.18={
		liege="d_nmih_tango"
		holder=10110065 # Taira Tadafusa
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1392.1.24={
		liege="d_nmih_tango"
		holder=10049509 # Isshiki Mitsunori
	}
	1409.1.25={
		holder=10049512 # Isshiki Yoshitsura
	}
	1440.6.14={
		holder=10049514 # Isshiki Norichika
	}
	1451.12.21={
		holder=10049515 # Isshiki Yoshinao
	}
	1474.4.1={
		holder=10049516 # Isshiki Yoshiharu
	}
	1484.9.3={
		holder=10049517 # Isshiki Yoshihide
	}
	1498.6.18={
		holder=10049519 # Isshiki Yoshiari
	}
	1512.8.30={
		holder=10049521 # Isshiki Yoshikiyo
	}
	1519.1.1={
		holder=10049500 # Isshiki Yoshiyuki
	}
	1558.1.1={
		holder=10049525 # Isshiki Yoshimichi
	}
	1580.1.1={
		liege="e_nmih_japan"
		holder=10040203 # Hosokawa Fujitaka(Yusai)
	}
	1582.6.21={
		liege=0
		holder=10040220 # Hosokawa Tadaoki
	}
	1582.7.2={
		liege="d_nmih_tango"
	}
}
