c_nmih_suzu = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_echizen"
	}
	718.5.2={
		liege="d_nmih_noto"
	}
	1179.12.18={
		liege="d_nmih_noto"
		holder=10110111 # Taira Noritsune
	}
	1183.6.22={
		liege="k_nmih_hokuriku"
		holder=10120084 # Minamoto Yoshinaka / Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan"
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1391.12.1={
		liege="d_nmih_noto"
		holder=10048110 # Hatakeyama Motokuni
	}
	1391.12.2={
		holder=12305001 # Yusa Motomitsu
	}
	1429.1.1={
		holder=12305005 # Yusa Tadamitsu
	}
	1460.1.1={
		holder=12305010 # Yusa Munehide
	}
	1510.1.1={
		holder=12305016 # Yusa Hidemori
	}
	1530.1.1 ={
		holder=12305020 # Yusa Hideyori
	}
	1553.1.1={
		holder=10031108 # Nukui Fusasada
	}
	1555.1.1={
		liege="d_nmih_noto"
		holder=10031150 # Yusa Tsugumitsu
	}
	1577.8.7={
		liege="d_nmih_echigo"
	}
	1579.9.1={
		liege=0
	}
	1580.9.1={
		liege="d_nmih_noto" # 29120 Oda Nobunaga
	}
	1581.3.28={
		holder=10029241 # Sugaya Nagayori
	}
	1581.10.29={
		holder=10029360 # Maeda Toshiie
	}
	1582.6.21={
		liege="d_nmih_echigo"
		holder=10031112 # Nukui Kagetaka
	}
	1582.7.13={
		liege="d_nmih_noto"
		holder=10029360 # Maeda Toshiie
	}

}
