c_nmih_yasu = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_omi"
	}
	1180.1.1={
		liege="e_nmih_japan"
		holder=10120213 # Minamoto Shigesada
	}
	1183.8.17={
		liege="d_nmih_omi" # Yamamoto Yoshitsuna
		holder=12515013 # Yamamoto [Nishigori] Yoshitaka / Taira's escape to the west
	}
	1184.3.4={
		liege="k_nmih_western_kanto"
		holder=12510030 # Sasaki Hideyoshi / Battle of Awazu
	}
	1184.8.26={
		liege="d_nmih_omi"
		holder=12510060 # Sasaki Sadatsuna / the three-day rebellion of the Taira clan
	}
	1205.4.29={
		holder=12510070 # Sasaki Hirotsuna
	}
	1221.7.22={
		holder=12510073 # Sasaki Nobutsuna / the Jokyu War
	}
	1242.4.7={
		holder=10044004 # Rokkaku [Sasaki] Yasutsuna
	}
	1276.6.30={
		holder=10044005 # Rokkaku [Sasaki] Yoritsuna
	}
	1311.1.22={
		holder=10044006 # Rokkaku [Sasaki] Tokinobu
	}
	1334.1.1={
		liege="d_nmih_southern_omi"
		holder=10044007 # Rokkaku Ujiyori
	}
	1370.6.30={
		holder=10044008 # Rokkaku Mitsutaka
	}
	1416.12.6={
		holder=10044010 # Rokkaku Mitsutsuna
	}
	1440.1.1={
		holder=10044660 # Iba Mitsutaka
	}
	1460.1.1={
		holder=10044661 # Iba Sadataka
	}
	1520.8.1={
		holder=10044600 # Shindo Nagahisa
	}
	1530.1.1={
		holder=10044601 # Shindo Sadaharu
	}
	1551.1.1={
		holder=10044602 # Shindo Katamori
	}
	1568.10.12={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1570.7.1={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029510 # Sakuma Nobumori
	}
	1575.12.9={
		liege="e_nmih_japan"
		holder=10029510 # Sakuma Nobumori
	}
	1580.8.1={
		liege="d_nmih_omi"
		holder=10029120 # Oda Nobunaga
	}
	1582.6.21={
		liege="k_nmih_northern_kinai"
		holder=10025639 # Akechi Hidemitsu
	}
	1582.7.2={
		liege="k_nmih_northern_kinai"
		holder=10029400 # Hashiba Hideyoshi
	}
}
