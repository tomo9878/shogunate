c_nmih_akasaka = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }
	1554.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	1180.1.1={
		liege = "d_nmih_bizen"
		holder=12630401 # Naniwa Tsuneto
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1340.1.1={
		liege="d_nmih_bizen"
		holder=10054415 # Uragami Tamekage
	}
	1360.1.1={
		holder=10054414 # Uragami Yukikage
	}
	1380.1.1={
		holder=10054413 # Uragami Sukekage
	}
	1408.1.1={
		holder=10054412 # Uragami Munetaka
	}
	1420.1.1={
		holder=10054400 # Uragami Muneyasu
	}
	1450.1.1={
		holder=10054401 # Uragami Norimune
	}
	1460.1.1={
		holder=10054405 # Uragami Norimune
	}
	1502.7.15={
		holder=10054403 # Uragami Muramune
	}
	1531.7.17={
		holder=10054404 # Uragami Masamune
	}
	1554.1.1={
		holder=10054407 # Uragami Munekage
	}
	1575.10.1={
		holder=10055100 # Ukita Naoie
	}
	1579.10.1={
		liege="e_nmih_japan"
	}
	1582.2.1={
		holder=10055101 # Ukita Hideie
	}

}
