c_nmih_minami = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_izumi"
	}
	1180.1.1={
		liege="d_nmih_izumi" # Taira Nobukane
		holder=10110235 # Taira Nobukane
	}
	1183.8.17={
		liege="d_nmih_izumi" # Minamoto Yukiie
		holder=10120072 # Minamoto Yukiie / Taira's escape to the west
	}
	1185.11.26={
		liege=0
		holder=0 # Failure of Yoshitsune's rebellion
	}
	1410.9.1={
		liege="d_nmih_izumi"
		holder=10040514 # Hosokawa Mochiari
	}
	1438.10.1={
		holder=10040515 # Hosokawa Noriharu
	}
	1450.6.1={
		holder=10040516 # Hosokawa Tsuneari
	}
	1480.12.1={
		holder=10040517 # Hosokawa Motoari
	}
	1500.1.1={
		holder=10040520 # Hosokawa Mototsune
	}
	1511.9.16={
		holder=10040566 # Hosokawa Takakuni
	}
	1512.1.1={
		holder=10040558 # Hosokawa Takamoto
	}
	1524.1.1={
		holder=10040595 # Hosokawa Katsumoto
	}
	1531.7.21={
		holder=10040522 # Hosokawa Harusada
	}
	1549.7.18 ={
		liege="k_nmih_southern_kinai"
		holder=10048753 # Matsura Mamoru
	}
	1557.1.1={
		liege="d_nmih_sanuki" # Sogo(Miyoshi) Kazumasa
		holder=10059025 # Matsura Hikaru
	}
	1561.4.2={
		liege="d_nmih_awa" # Miyoshi Jikkyu
	}
	1562.4.8={
		liege="k_nmih_southern_kinai"
	}
	1572.1.1={
		liege="k_nmih_chubu"
	}
	1573.12.10={
		liege="d_nmih_kawachi"
	}
	1576.1.1={
		holder=10048751 # Matsura Munekiyo
	}
	1580.8.1={
		liege="e_nmih_japan"
		holder=10025940 # Hachiya Yoritaka
	}
	1582.6.19={
		liege="d_nmih_ise"
	}
	1582.7.2={
		liege="k_nmih_northern_kinai"
	}

}
