d_nmih_shima = {
	0697.1.1={
		liege="e_nmih_tenno"
	}
	0701.1.1={
		liege="e_nmih_tenno"
	}
	1180.1.1={
		liege="k_nmih_northern_kinai"
		holder=10131576 # Ito [Taira] Tadakiyo
	}
	1183.8.17={
		liege= "k_nmih_western_chugoku"
	}
	1185.4.25={
		liege=0
	}
	1185.6.15={
		liege=0
		holder=0
	}
	1383.7.1={
		holder=10026050 # Kitabatake Akiyasu
	}
	1414.1.1={
		holder=10026052 # Kitabatake Mitsumasa
	}
	1429.1.25={
		liege="e_nmih_japan"
		holder=10026006 # Kitabatake Noritomo
	}
	1467.6.27={ # Onin War
		liege=0
	}
	1471.4.13={
		liege=0
		holder=10026005 # Kitabatake Masasato
	}
	1508.12.15={
		holder=10026001 # Kitabatake Kichika
	}
	1517.1.1={
		holder=10026000 # Kitabatake Harutomo
	}
	1553.1.1={
		holder=10026003 # Kitabatake Tomonori
	}
	1563.10.4={
		holder=10026007 # Kitabatake Tomofusa
	}
	1569.11.21={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1575.1.1={
		holder=10029161 # Oda Nobukatsu
	}
	1575.12.9={
		liege="e_nmih_japan"
	}
	1582.6.21={ # Honno-ji Incident
		liege=0
	}

}
