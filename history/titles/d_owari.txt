d_nmih_owari = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	0837.2.24={
		holder=11000035 # Fujiwara Tasuku {Owari no kami}
	}
	0841.2.11={
		holder=0 #  
	}
	0881.2.13={
		holder=11003300 # Fujiwara Takafuji {Owari no kami}
	}
	0884.4.12={
		holder=0 #  
	}
	1060.12.17={
		holder=11005677 # Fujiwara Tokifusa {Owari no kami}
	}
	1065.2.14={
		holder=0 #  
	}
	1090.9.12={
		holder=11000136 # Fujiwara Tadanori {Owari no kami}
	}
	1099.2.21={
		holder=0 #  
	}
	1114.5.16={
		holder=11001022 # Fujiwara Suenari {Owari no kami}
	}
	1116.1.9={
		holder=0 #  
	}
	1179.12.18={
		liege="e_nmih_japan"
		holder=10110140 # Taira Kiyosada {Owari no kami}
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1314.9.1={
		liege="e_nmih_japan"
		holder=10132288 # Hojo Takaie
	}
	1317.4.1={
		holder=10131857 # Hojo Munenori
	}
	1333.7.4={
		holder=0 #  
	}
	1351.10.1={
		holder=10025313 # Toki Yoriyasu
	}
	1388.2.3={
		holder=10025318 # Toki Mitsusada
	}
	1391.5.1={
		holder=10049507 # Isshiki Akinori
	}
	1391.6.1={
		holder=0 #  
	}
	1393.10.1={
		holder=12395223 # Imagawa Nakaaki
	}
	1398.11.1={
		holder=10048110 # Hatakeyama Motokuni
	}
	1400.6.1={
		holder=10029011 # Shiba Yoshishige
	}
	1418.9.10={
		holder=10029012 # Shiba Yoshiatsu
	}
	1434.1.11={
		holder=10029013 # Shiba Yoshisato
	}
	1436.11.8={
		holder=10029016 # Shiba Yoshitake
	}
	1452.10.13={
		holder=10029019 # Shiba Yoshitoshi
	}
	1460.1.1={
		holder=10029021 # Shiba Yoshihiro
	}
	1466.9.2={
		holder=10029019 # Shiba Yoshitoshi
	}
	1466.10.23={
		holder=10057621 # Shiba Yoshikado
	}
	1467.6.27={
		liege=0
		holder=10057621 # Shiba Yoshikado
	}
	1478.1.1={
		holder=10029021 # Shiba Yoshihiro
	}
	1513.5.21={
		holder=10029001 # Shiba Yoshitatsu
	}
	1521.11.1={
		holder=10029000 # Shiba Yoshimune
	}
	1554.08.10={
		holder=10029024 # Shiba Yoshikane
	}
	1555.01.01={
		holder=10029120 # Oda Nobunaga
	}
	1567.10.1={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1575.12.30={
		holder=10029160 # Oda Nobutada
	}
	1582.6.21={
		liege=0
		holder=10029184 # Oda Hidenobu, Honno-ji incident
	}
	1582.7.16={
		liege=0
		holder=10029161 # Oda Nobukatsu
	}

}
