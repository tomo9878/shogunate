c_nmih_joto = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1331.1.1 = { change_development_level = 30 }

	100.1.1={
	}
	1180.1.1={
		liege = "d_nmih_bizen"
		holder=12630401 # Naniwa Tsuneto
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1460.1.1={
		holder = 10054604 # Matsuda Motoshige
	}
	1490.1.1={
		holder = 10054603 # Matsuda Motokatsu
	}
	1510.1.1={
		holder = 10054602 # Matsuda Mototaka
	}
	1531.7.17={
		holder = 10054601 # Matsuda Motomori
	}
	1559.1.1={
		holder=10054600 # Matsuda Motokata
	}
	1564.1.1={
		holder=10054900 # Mimura Iechika
	}
	1566.2.24={
		holder=10054906 # Mimura Motochika
	}
	1567.1.1={
		holder=10055100 # Ukita Naoie
		liege="d_nmih_bizen"
	}
	1569.9.1={
		liege=0
	}
	1569.12.1={
		liege="d_nmih_bizen"
	}
	1574.3.1={
		liege=0
	}
	1575.10.1={
		liege="d_nmih_bizen"
	}
	1579.10.1={
		liege="e_nmih_japan"
	}
	1582.2.1={
		holder=10055101 # Ukita Hideie
	}

}
