c_nmih_nanjo = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1331.1.1 = { change_development_level = 30 }

	0701.1.1={
		liege="d_nmih_echizen"
	}
	1179.12.18={
		liege="d_nmih_echizen"
		holder=10110032 # Taira Norimori
	}
	1181.7.26={
		liege="d_nmih_kaga" # Battle of Yokota-gawara
		holder=10130716 # Hayashi Mitsuaki
	}
	1183.5.20={
		liege="d_nmih_echizen" # Siege of Hiuchi Castle
		holder=10110032 # Taira Norimori
	}
	1183.6.22={
		liege=0
		holder=0 # Battle of Shinohara
	}
	1481.8.21={
		holder=10030008 # Asakura Ujikage
	}
	1486.8.3={
		holder=10030010 # Asakura Sadakage
	}
	1512.4.11={
		holder=10030000 # Asakura Takakage
	}
	1548.4.30={
		holder=10030011 # Asakura Yoshikage
	}
	1573.9.16={
		holder=10029120 # Oda Nobunaga
	}
	1573.9.24={
		holder=10030100 # Maeba Yoshitsugu
	}
	1574.2.10={
		liege=0
		holder=10030120 # Toda Nagashige
	}
	1574.3.7={
		holder=10042324 # Shimotsuma Raisho
	}
	1574.9.1={
		liege="d_nmih_echizen" # 42324 Shimotsuma Raisho
		holder=10042324 # Shimotsuma Raisho
	}
	1575.9.25={
		holder=10029120 # Oda Nobunaga
	}
	1575.10.1={
		holder=10029360 # Maeda Toshiie
	}
	1581.1.1={
		holder=10025911 # Fuwa Naomitsu
	}

}
