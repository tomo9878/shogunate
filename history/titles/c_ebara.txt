c_nmih_ebara = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1366.1.1 = { change_development_level = 20 }
	1460.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_musashi"
	}
	1180.1.1={
		liege=0
		holder=12204001 # Oi Saneharu
	}
	1180.10.24={
		liege="k_nmih_western_kanto" # served under Yoritomo
	}
	1192.7.12={
		liege="e_nmih_japan"
	}
	1210.1.1={
		liege=0
		holder=0
	}
	1430.1.1={
		liege="d_nmih_sagami"
		holder=10018310 # Kira Yoriharu
	}
	1457.1.1={
		liege="d_nmih_musashi"
	}
	1460.1.1={
		holder=10018311 # Kira Yoriuji
	}
	1480.1.1={
		holder=10018312 # Kira Yoritaka
	}
	1500.1.1={
		holder=10018313 # Kira Masatada
	}
	1510.1.1={
		holder=10018314 # Kira Shigetaka
		liege="d_nmih_musashi"
	}
	1523.1.1={
		liege="d_nmih_sagami" #became vassal of the Hojo
	}
	1540.1.1={
		holder=10018315 # Kira Yorisada
	}
	1560.1.1={
		holder=10018316 # Kira Ujitomo
	}
}
