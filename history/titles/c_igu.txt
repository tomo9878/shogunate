c_nmih_igu = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	1050.1.1={
	    holder=11004629 # Fujiwara Tsunekiyo
	}
	1128.8.10={
	    holder=12030013 # Fujiwara Kiyotsuna
	}
	1142.1.1={
	    holder=12030040 # Fujiwara Toshihira
	}
	1180.1.1={
		liege="k_nmih_northern_oshu"
		holder=12030020 # Fujiwara Hidehira
	}
	1187.11.30={
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.9.21={
		liege="k_nmih_western_kanto" # Battle of Mountain Atsukashi
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		holder=12150125 # Takeshi Tanemori
	}
	1215.7.11={
		holder=12150900 # Takeshi Taneshige
	}
	1240.1.1={
		holder=12150907 # Takeshi Taneuji
	}
	1270.1.1={
		holder=12150910 # Takeshi Muneuji
	}
	1300.1.1={
		holder=12150915 # Takeshi Tanetsugu
	}
	1331.1.1={
		holder=12150920 # Takeshi Takahiro
	}
	1339.1.1={
		holder=10008301 # Watari Hirotane
	}
	1381.1.1={
		holder=10008302 # Watari Yukitane
	}
	1393.1.1={
		holder=10008303 # Watari Shigetane
	}
	1412.1.1={
		holder=10008304 # Watari Taneshige
	}
	1441.1.1={
		liege=0
		holder=10008305 # Watari Shigetsura
	}
	1446.1.1={
		holder=10008306 # Watari Munekiyo
	}
	1463.1.1={
		holder=10008307 # Watari Shigemoto
	}
	1481.1.1={
		holder=10008308 # Watari Mototane
	}
	1504.1.1={
		holder=10008309 # Watari Munemoto
	}
	1520.1.1={
		liege="d_nmih_oshu_shugo"
	}
	1531.1.1={
		holder=10008310 # Watari Munetaka
	}
	1542.6.1={
		liege="d_nmih_iwaki" # Tenbun War
	}
	1548.9.1={
		liege="d_nmih_oshu_shugo"
		holder=10001010 # Watari Motomune
	}
}
