c_nmih_tosa = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1555.2.24 = { change_development_level = 20 }

	100.1.1={
	}
	1180.1.1={
		liege="d_nmih_tosa"
		holder=10120077 # Minamoto Mareyoshi
	}
	1180.9.8={
		liege="d_nmih_awa"
		holder=12690900 # Hasuike Ietsuna
	}
	1184.11.20={
		liege=0
		holder=0 # 
	}
	1380.1.1={
		liege="d_nmih_tosa"
		holder=10040586 # Hosokawa Yorimasa
	}
	1408.1.1={
		holder=10040587 # Hosokawa Mitsumasu
	}
	1440.1.1={
		holder=10040588 # Hosokawa Mochimasu
	}
	1468.1.2={
		holder=10040589 # Hosokawa Katsumasu
	}
	1502.7.8={
		holder=10040591 # Hosokawa Masamasu
	}
	1507.8.1={
		liege=0
		holder=10058001 # Motoyama Yasuaki
	}
	1531.1.1={
		holder=10058000 # Motoyama Shigemune
	}
	1555.2.24={
		liege=0
		holder=10059102 # Chosokabe Kunichika
	}
	1560.7.8={
		holder=10059104 # Chosokabe Motochika
	}
	1569.9.21={
		liege="d_nmih_tosa"
	}

}
