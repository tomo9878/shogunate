c_nmih_usa = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	0001.1.1={
	}
	0007.1.1={
		holder=10290100 # Usatsuhiko
		liege=d_nmih_buzen
	}
	0070.1.1={
		holder=10290101 # Tokotsuhiko
	}
	0100.1.1={
		holder=10290102 # Wakayahiko
	}
	0150.1.1={
		holder=10290103 # Oshiiri
	}
	0160.1.1={
		holder=10290104 # Tamashiki
	}
	0210.1.1={
		holder=10290105 # Fushikinushi
	}
	0250.1.1={
		holder=10290106 # Ishirihiko
	}
	0258.1.1={
		liege=0
	}
	0270.1.1={
		holder=10290107 # Makui
	}
	0290.1.1={
		holder=10290108 # Isokari
	}
	0320.1.1={
		holder=10290109 # Itoe
	}
	0350.1.1={
		holder=10290110 # Iamana
	}
	1180.1.1={
		liege="d_nmih_buzen"
		holder=10260310 # Usa Kimimichi
	}
	1185.3.4={
		liege=0
		holder=0 # Battle of Ashiya-no-ura
	}
	1300.1.1={
		liege=k_nmih_western_chugoku
	}
	1380.1.1={
		holder=10067016 # Kii Naotsuna
	}
	1420.1.1={
		holder=10067014 # Kii Ienao
	}
	1450.1.1={
		holder=10067013 # Kii Morinao
	}
	1470.1.1={
		holder=10067003 # Kii Hidefusa
	}
	1490.1.1={
		holder=10067001 # Kii Okifusa
	}
	1517.1.1={
		holder=10067000 # Kii Masafusa
	}
	1551.9.30={
		liege=d_nmih_bungo
	}
	1561.1.1={
		holder=10064444 # Nata Chikatada
	}
}
