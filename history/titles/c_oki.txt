c_nmih_oki = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	100.1.1={
		liege="d_nmih_oki"
	}
	1179.12.18={
		liege = "k_nmih_sanin"
		holder=11006084 # Fujiwara Koreyori
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1441.7.12={
		liege="d_nmih_oki"
		holder=10044210 # Kyogoku Mochikiyo
	}
	1470.8.30={
		holder=10044215 #　Kyogoku Magodojimaru
	}
	1471.1.1={
		holder=10044213 # Kyogoku Masatsune
	}
	1486.8.1={
		holder = 10051501 # Amago Tsunehisa
	}
	1541.11.30={
		holder = 10051508 # Amago Haruhisa
	}
	1561.1.9={
		holder = 10051525 # Amago Yoshihisa
	}
	1567.1.8={
		liege = "k_nmih_western_chugoku"
		holder=10056004 # Mori Motonari
	}
	1571.7.6={
		holder=10056025 # Mori Terumoto
	}

}
