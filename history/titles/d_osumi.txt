d_nmih_osumi = {
	0100.1.1={
	}
	130.1.1={
		holder=10290303 # Hishimochi
	}
	170.1.1={
		holder=10290304 # Okuchi
	}
	210.1.1={
		holder=10290305 # Sonobe
	}
	260.1.1={
		holder=10290306 # Sutaki
	}
	0701.1.1={
		liege="e_nmih_tenno"
	}
	764.11.6={
		holder=10041023 # Empress Koken/Shotoku
	}
	1180.1.1={
		liege=0
		holder=0 # 
	}
	1197.1.1={
		holder=10060035 # Shimazu Tadahisa
	}
	1203.1.1={
		holder=0
	}
	1217.1.1={
		holder=10131653 # Hojo Yoshitoki
	}
	1224.7.1={
		holder=10131667 # Hojo Tomotoki
	}
	1245.5.3={
		holder=10131697 # Hojo Tokiaki
	}
	1272.3.11={
		holder=0
	}
	1291.1.1={
		holder=10131933 # Hojo Tokinao
	}
	1317.1.1={
		holder=10132166 # Hojo Moroyori
	}
	1333.5.1={
		holder=10060031 # Shimazu Sadahisa
	}
	1363.8.12={
		holder=0 # Shimazu Morohisa
	}
	1375.1.1={
		holder=12395221 # Imagawa Sadayo
	}
	1394.1.1={
		holder=10060030 # Shimazu Motohisa
	}
	1411.8.25={
		holder=10060004 # Shimazu Hisatoyo
	}
	1425.2.18={
		holder=10060005 # Shimazu Tadakuni
	}
	1470.2.21={
		holder=10060008 # Shimazu Tatsuhisa
	}
	1474.4.26={
		holder=10060011 # Shimazu Tadamasa
	}
	1508.3.16={
		holder=10060012 # Shimazu Tadaharu
	}
	1515.10.2={
		holder=10060013 # Shimazu Tadataka
	}
	1519.5.12={
		holder=10060002 # Shimazu Katsuhisa
	}
	1527.1.1={
		holder=10060000 # Shimazu Takahisa
	}
	1571.7.15={
		holder=10060024 # Shimazu Takahisa
	}
}
