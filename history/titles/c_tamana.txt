c_nmih_tamana = {
	1.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	230.1.1={
		holder=10290330 # Tsurara
		liege=d_nmih_higo
	}
	295.1.1={
		holder=10041357 # Takekunitake
	}
	330.1.1={
		holder=10041358 # Takeoshihiko
	}
	350.1.1={
		holder=10041359 # Takekawana
	}
	390.1.1={
		holder=10041360 # Takekuwae
	}
	1180.1.1={
		liege="d_nmih_higo"
		holder=12780650 # Kikuchi Takanaga
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1488.11.26={
		holder=10066005 # Kikuchi Shigetomo
		liege=0
	}
	1493.12.7={
		holder=10066004 # Kikuchi Yoshiyuki
	}
	1504.3.1={
		holder=10066003 # Kikuchi Masataka
	}
	1507.1.1={
		holder=10066203 # Kikuchi Taketsune
	}
	1513.1.1={
		holder=10066001 # Kikuchi Takekane
	}
	1532.1.1={
		holder=10066000 # Kikuchi Yoshitake
	}
	1550.1.1={
		holder=10064002 # Otomo Yoshishige
	}
	1578.1.1={
		holder=10068320 # Ryuzoji Takanobu
	}
	1581.7.18={
		holder=10068105 # Nabeshima Naoshige
		liege=d_nmih_hizen
	}
}
