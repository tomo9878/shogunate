c_nmih_okitama = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1385.1.1 = { change_development_level = 20 }

	0712.10.1={
		liege="k_nmih_dewa"
	}
	1157.4.29={
		liege="k_nmih_northern_oshu" # Fujiwara Hidehira
		holder=12030020 # Fujiwara Hidehira
	}
	1187.11.30={
		liege="k_nmih_northern_oshu" # Fujiwara Yasuhira
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.9.24={
		liege="k_nmih_western_kanto"
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		liege="k_nmih_western_kanto"
		holder=10220001 # Oe Hiromoto
	}
	1192.7.12={
		liege="e_nmih_japan"
		holder=10220001 # Oe Hiromoto
	}
	1225.7.16={
		holder=12221011 # Oe Tokihiro
	}
	1241.7.8={
		holder=12221015 # Nagai Yasuhide
	}
	1254.1.11={
		holder=12221021 # Nagai Tokihide
	}
	1290.1.1={
		holder=12221025 # Nagai Munehide
	}
	1327.12.20={
		holder=12221041 # Nagai Hirohide
	}
	1338.9.24={
		liege="e_nmih_japan"
	}
	1358.1.1={
		holder=12221059 # Nagai Hirofusa
	}
	1385.1.1={
		liege="d_nmih_oshu_shugo"
		holder=12071030 # Date Masamune
	}
	1405.10.7={
		holder=12071033 # Date Ujimune
	}
	1412.8.24={
		holder=12071035 # Date Mochimune
	}
	1469.2.19={
		holder=12071040 # Date Narimune
	}
	1487.10.11={
		holder=12071045 # Date Hisamune
	}
	1514.5.28={
		holder=10001001 # Date Tanemune
	}
	1542.6.1={
		liege="d_nmih_oshu_shugo" # Tenbun War
		holder=10001002 # Date Harumune
	}
	1564.1.1={
		holder=10001021 # Date Terumune
	}
	1584.10.6={
		holder=10001035 # Date Masamune
	}
}
