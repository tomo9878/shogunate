c_nmih_morokata = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	220.1.1={
		holder=10041352 # Takekukuchi a.k.a Kukuchihiko
	}
	1180.1.1={
		liege=0
		holder=12810104 # Tomo Nobuaki
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1300.1.1={
		liege="d_nmih_hyuga"
	}
	1352.1.1={
		holder=10060258 # Hongo Suketada
	}
	1360.1.1={
		holder=10060257 # Hongo Yoshihisa
	}
	1370.1.1={
		holder=10060256 # Hongo Tomohisa
	}
	1410.1.1={
		holder=10060255 # Hongo Mochihisa
	}
	1450.1.1={
		holder=10060254 # Hongo Toshihisa
	}
	1480.1.1={
		holder=10060253 # Hongo Yoshihisa
	}
	1500.1.1={
		liege=0
	}
	1500.1.1={
		holder=10060251 # Hongo Kazuhisa
	}
	1521.1.1={
		holder=10060250 # Hongo Tadasuke
	}
	1543.1.1={
		liege="d_nmih_hyuga"
	}
	1559.12.14={
		holder=10060252 # Hongo Tokihisa
	}
}
