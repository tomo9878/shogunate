c_nmih_imizu = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1380.1.1 = { change_development_level = 40 }
	1520.1.1 = { change_development_level = 40 }

	0702.10.14={
		liege="d_nmih_etchu"
	}
	1179.12.18={
		liege="d_nmih_etchu"
		holder=10110142 # Taira Nariie
	}
	1183.6.22={
		liege="k_nmih_hokuriku"
		holder=10120084 # Minamoto Yoshinaka / Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan"
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1380.7.1={
		liege="d_nmih_etchu"
		holder=10031205 # Jinbo Kunihisa
	}
	1420.1.1={
		holder=10031206 # Jinbo Norihisa
	}
	1440.1.1={
		holder=10031207 # Jinbo Kunimune
	}
	1454.4.1={
		holder=10031201 # Jinbo Naganobu
	}
	1493.6.9={
		liege="k_nmih_southern_kinai"
		holder=10040011 # Ashikaga Yoshiki
	}
	1499.12.1={
		liege="d_nmih_etchu"
		holder=10031201 # Jinbo Naganobu
	}
	1501.12.18={
		holder=10031202 # Jinbo Yoshimune
	}
	1519.1.1={
		liege=0
	}
	1521.1.30={
		holder=10031200 # Jinbo Nagamoto
	}
	1572.1.1={
		holder=10031204 # Jinbo Naganari
	}
	1576.1.1={
		liege="d_nmih_echigo"
		holder=10031241 # Jinbo Ujiharu
	}
	1578.4.19={
		liege="e_nmih_japan"
	}
	1581.3.5={
		liege="d_nmih_etchu"
	}

}
