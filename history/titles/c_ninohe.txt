c_nmih_ninohe = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1470.1.1 = { change_development_level = 10 }

	100.1.1={
	}
	1157.4.29={
		liege="k_nmih_northern_oshu"
		holder=12030020 # Fujiwara Hidehira
	}
	1187.11.30={
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.10.3={
		liege=0 # 
		holder=0 # Fall of Hiraizumi
	}
	1392.1.1={
		liege="d_nmih_mutsu"
		holder=12040190 # Nanbu Moriyuki
	}
	1437.5.13={
		holder=12040200 # Nanbu Yoshimasa
	}
	1440.8.9={
		holder=12040201 # Nanbu Masamori
	}
	1445.1.1={
		holder=10003130 # Nanbu Mitsumasa
	}
	1449.1.1={
		holder=10003131 # Nanbu Tokimasa
	}
	1470.1.1={
		holder=10003210 # Kunohe Mitsumasa
	}
	1480.1.1={
		holder=10003211 # Kunohe Tsurayasu
	}
	1520.1.1={
		holder=10003200 # Kunohe Nobunaka
	}
	1560.1.1={
		holder=10003201 # Kunohe Masazane
	}
	1591.5.7={
		liege=0 # Kunohe Masazane's rebe
		holder=10003201 # Kunohe Masazane
	}
	1591.11.6={
		holder=10003154 # Nanbu Nobunao
	}

}
