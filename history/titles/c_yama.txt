c_nmih_yama = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1181.7.26 = { change_development_level = 30 }

	100.1.1={
	}
	1180.1.1={
		liege="d_nmih_echigo"
		holder=12270150 # Jo Jotan
	}
	1181.7.25={
		liege="k_nmih_northern_oshu"
		holder=12030020 # Fujiwara Hidehira, Battle of Yokota-gawara
	}
	1187.11.30={
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.9.21={
		liege="k_nmih_western_kanto" # Battle of Mountain Atsukashi
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		holder=12211035 # Sahara Yoshitsura
	}
	1200.1.1={
		holder=12211203 # Sahara Moritsura
	}
	1233.5.1={
		holder=12211223 # Sahara Mitsumori
	}
	1266.1.1={
		holder=12070001 # Ashina Yasumori
	}
	1295.1.1={
		holder=12070005 # Ashina Morimune
	}
	1320.1.1={
		holder=12070010 # Ashina Morikazu
	}
	1335.1.1={
		liege="d_nmih_aizu"
		holder=12070016 # Ashina Naomori
	}
	1390.1.1={
		holder=12070020 # Ashina Akimori
	}
	1407.1.1={
		holder=12070025 # Ashina Morimasa
	}
	1434.6.1={
		holder=12070030 # Ashina Morihisa
	}
	1444.1.1={
		holder=12070031 # Ashina Morinobu
	}
	1451.1.1={
		holder=10007008 # Ashina Moriakira
	}
	1466.1.1={
		holder=10007004 # Ashina Moritaka
	}
	1480.1.1={
		holder=10007801 # Inawashiro Morikiyo
	}
	1539.1.1={
		holder=10007802 # Inawashiro Morikuni
	}
}
