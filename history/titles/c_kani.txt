c_nmih_kani = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1339.1.1 = { change_development_level = 20 }
	1556.11.8 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_mino"
	}
	1058.1.1={
		liege="d_nmih_mino"
		holder=10120165 # Minamoto Kunifusa
	}
	1119.1.1={
		holder=10120180 # Minamoto Mitsukuni
	}
	1148.1.5={
		holder=12420100 # Toki Mitsumoto
	}
	1180.1.1={
		liege="d_nmih_mino"
		holder=12420102 # Toki Mitsunaga
	}
	1184.1.3={
		liege=0
		holder=10025300 # Toki Mitsuhira / Siege of Hojujidono
	}
	1206.5.4={
		liege="d_nmih_mino"
		holder=10025306 # Toki Mitsuyuki
	}
	1250.1.1={
		liege="d_nmih_mino"
		holder=10025308 # Toki Mitsusada
	}
	1281.9.25={
		liege="d_nmih_mino"
		holder=10025309 # Toki Yorisada
	}
	1339.4.1={
		liege="d_nmih_mino"
		holder=10025312 # Toki Yorimoto
	}
	1360.1.1={
		liege="d_nmih_mino"
		holder=12420500 # Akechi Yorishige
	}
	1380.1.1={
		holder=12420501 # Akechi Yorihide
	}
	1405.1.1={
		holder=12420503 # Akechi Kuniatsu
	}
	1430.1.1={
		holder=12420504 # Akechi Yoriaki
	}
	1455.1.1={
		holder=12420505 # Akechi Yorihide
	}
	1480.1.1={
		holder=10025635 # Akechi Yorihiro
	}
	1500.1.1={
		holder=10025630 # Akechi Mitsutsugu
	}
	1538.4.14={
		holder=10025633 # Akechi Mitsuyasu
	}
	1555.12.24={
		liege=0
	}
	1556.11.8={
		liege="d_nmih_mino" 
		holder=10025506 # Nagai Michitoshi
	}
	1567.10.1={
		holder=10029120 # Oda Nobunaga
	}
	1568.1.1={
		liege="d_nmih_mino" # 29120 Oda Nobunaga
		holder=10025420 # Mori Yoshinari
	}
	1570.10.19={
		holder=10025421 # Mori Nagayoshi
	}
	1582.6.21={ # Honno-ji Incident
		liege=0
	}
	1582.7.16={
		liege="k_nmih_northern_kinai"
	}
}
