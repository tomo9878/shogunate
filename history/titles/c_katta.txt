c_nmih_katta = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1187.11.30 = { change_development_level = 10 }

	100.1.1={
	}
	1180.1.1={
		liege="k_nmih_northern_oshu"
		holder=12030051 # Fujiwara Yasuhira
	}
	1187.11.30={
		holder=12082010 # Katta Hidenobu
	}
	1189.9.21={
		liege="k_nmih_western_kanto" # Battle of Mountain Atsukashi
		holder=12082010 # Katta Hidenobu
	}
	1189.10.1={
		holder=12082011 # Katta Hidenaga
	}
	1244.1.1={
		holder=12082015 # Katta Nagamasa
	}
	1269.1.1={
		holder=12082020 # Katta Nagatoshi
	}
	1276.1.1={
		holder=10006601 # Shiroishi Munehiro
	}
	1339.1.1={
		holder=10006602 # Shiroishi Munetsugu
	}
	1361.1.1={
		holder=10006603 # Shiroishi Muneyoshi
	}
	1377.1.1={
		holder=10006604 # Shiroishi Munechika
	}
	1414.1.1={
		holder=10006605 # Shiroishi Munenobu
	}
	1425.1.1={
		holder=10006606 # Shiroishi Muneyasu
	}
	1437.1.1={
		liege=0
		holder=10006607 # Shiroishi Munenori
	}
	1479.1.1={
		holder=10006608 # Shiroishi Munenaga
	}
	1495.1.1={
		holder=10006609 # Shiroishi Munekiyo
	}
	1517.1.1={
		liege="d_nmih_oshu_shugo"
		holder=10006611 # Shiroishi Munetsuna
	}
	1535.1.1={
		holder=10006612 # Shiroishi Munetoshi
	}
	1553.1.1={
		holder=10006613 # Shiroishi Munezane
	}

}
