d_nmih_nagato = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	1179.12.18={
		liege = "e_nmih_japan"
		holder=12661100 # Kii Michisuke
	}
	1183.8.17={
		liege = "k_nmih_western_chugoku"
		holder=10110043 # Taira Tomomori
	}
	1185.3.22={
		liege=0
		holder=0 # Battle of Yashima
	}
	1186.7.30={
		liege="k_nmih_western_kanto"
		holder=12510063 # Sasaki Takatsuna
	}
	1192.7.12={
		liege="e_nmih_japan" # Minamoto no Yoritomo
	}
	1194.1.14={
		holder=12510060 # Sasaki Sadatsuna
	}
	1205.4.29={
		holder=12510070 # Sasaki Hirotsuna
	}
	1221.7.1={
		liege="e_nmih_tenno" # Gotoba Emperor Emeritus
		holder=12510070 # Sasaki Hirotsuna
	}
	1221.7.22={
		liege=0
		holder=0
	}
	1221.8.1={
		liege="e_nmih_japan"
		holder=12690610 # Tachibana no Kiminari
	}
	1221.10.1={
		holder=0
	}
	1222.1.1={
		holder=12230105 # Amano Masakage
	}
	1276.1.28={
		holder=10132000 # Hojo Muneyori
	}
	1279.7.15={
		holder=0 # Hojo Muneyori died
	}
	1280.7.3={
		holder=10132162 # Hojo Kanetoki
	}
	1281.9.14={
		holder=10132156 # Hojo Morotoki
	}
	1282.9.27={
		holder=0
	}
	1292.2.1={
		holder=10131747 # Hojo Tokimura
	}
	1323.1.1={
		holder=10131933 # Hojo Tokinao
	}
	1333.7.8={
		liege=0
		holder=0
	}
	1358.1.1={
		holder=10057014 # Ouchi Hiroyo
	}
	1380.12.12={
		holder=10057013 # Ouchi Yshiriro
	}
	1397.1.1={
		liege="k_nmih_western_chugoku"
		holder=10057011 # Ouchi Hiroshige
	}
	1402.2.1={
		liege=0 # independence
		holder=10057010 # Ouchi Moriharu
	}
	1402.3.1={
		liege="k_nmih_western_chugoku" # Ouchi Moriharu
	}
	1431.8.6={
		holder=10057015 # Ouchi Mochiyo
	}
	1441.8.14={
		holder=10057008 # Ouchi Norihiro
	}
	1465.9.23={
		holder=10057007 # Ouchi Masahiro
	}
	1470.3.6={
		liege="e_nmih_japan" # Ashikaga Yoshimasa
		holder=10057009 # Ouchi Doton [Noriyuki]
	}
	1472.1.1={
		liege="k_nmih_western_chugoku" # Ouchi Masahiro
		holder=10057007 # Ouchi Masahiro
	}
	1495.10.6={
		holder= 10057001 # Ouchi Yoshioki
	}
	1529.1.29={
		holder = 10057002 # Ouchi Yoshitaka
	}
	1551.9.30={
		holder = 10057102 # Sue Harukata
	}
	1555.10.16={
		holder = 10057116 # Sue Nagafusa
	}
	1557.4.1={
		holder=10056004 # Mori Motonari
	}
	1562.01.01={
		liege="k_nmih_western_chugoku"
		holder=10056013 # Mori Takamoto
	}
	1563.9.28={
		holder=10056025 # Mori Terumoto
	}

}
