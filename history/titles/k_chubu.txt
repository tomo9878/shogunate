k_nmih_chubu = {
	0001.1.1={
	}
	1179.12.18={
		liege="e_nmih_japan"
		holder = 10110025 # Taira Kiyomori
	}
	1181.3.20={
		holder=10110042 # Taira Munemori
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1567.10.1={ # Nobunaga ruled Owari and Mino
		liege=0
		holder=10029120 # Oda Nobunaga
	}
	1568.11.7={
		liege="e_nmih_japan" # Ashikaga Yoshiaki
	}
	1572.10.1={
		liege=0
	}
	1575.12.9={
		liege="e_nmih_japan" # Oda Nobunaga
	}
	1582.04.15={
		holder=10029160 # Oda Nobutada
	}
	1582.6.21={ # Honno-ji incident
		liege=0
		holder=10029184 # Oda Hidenobu
	}

}
