c_nmih_ichishi = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }

	0701.1.1={
		liege="d_nmih_ise"
	}
	1180.1.1={
		liege="d_nmih_ise" # Taira Moritoshi
		holder=10110250 # Taira Moritoshi
	}
	1183.8.17={
		liege="d_nmih_shima" # Ito Tadakiyo / Taira's escape to the west
		holder=10131576 # Ito Tadakiyo
	}
	1184.9.23={
		liege=0
		holder=0 # Failure of Taira's Three-day rebellion
	}
	1383.7.1={
		holder=10026050 # Kitabatake Akiyasu
	}
	1414.1.1={
		holder=10026052 # Kitabatake Mitsumasa
	}
	1429.1.25={
		holder=10026006 # Kitabatake Noritomo
	}
	1471.4.13={
		holder=10026005 # Kitabatake Masasato
	}
	1508.12.15={
		holder=10026001 # Kitabatake Kichika
	}
	1517.1.1={
		holder=10026000 # Kitabatake Harutomo
	}
	1553.1.1={
		holder=10026003 # Kitabatake Tomonori
	}
	1563.10.4={
		holder=10026007 # Kitabatake Tomofusa
	}
	1569.11.21={
		liege="d_nmih_shima" # 29120 Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1575.1.1={
		liege="d_nmih_shima" # 29161 Oda Nobukatsu
		holder=10029161 # Oda Nobukatsu
	}

}
