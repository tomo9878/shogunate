c_nmih_kojima = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	1180.1.1={
		liege = "d_nmih_bizen"
		holder=12630401 # Naniwa Tsuneto
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1300.1.1={
		liege="k_nmih_kanrei"
	}
	1392.3.25={
		holder = 10040524 # Hosokawa Yoshiyuki
	}
	1402.1.1={
		holder = 10040525 # Hosokawa Mitsuhisa
	}
	1430.10.15={
		holder = 10040526 # Hosokawa Mochitsune
	}
	1449.1.1={
		holder = 10040528 # Hosokawa Shigeyuki
	}
	1479.1.1={
		holder = 10040530 # Hosokawa Masayuki
	}
	1488.9.4={
		holder = 10040529 # Hosokawa Yoshiharu
	}
	1495.1.17={
		holder = 10040532 # Hosokawa Yukimochi
	}
	1509.1.1={
		liege="e_nmih_japan"
		holder = 10055062 # Ueno Nobutaka
	}
	1520.1.1={
		holder = 10055064 # Ueno Takanao
	}
	1560.1.1={
		liege=0
		holder = 10055067 # Ueno Takanori
	}
	1575.7.9={
		liege="k_nmih_western_chugoku"
		holder=10056016 # Kobayakawa Takakage
	}

}
