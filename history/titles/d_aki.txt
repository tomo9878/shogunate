d_nmih_aki = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	0771.4.20={
		holder=10252024 # Sakanoue no Karitamaro
	}
	0774.4.20={
		holder=0
	}
	0982.1.30={
		holder=10127106 # Minamoto no Sukenori
	}
	0982.2.26={
		holder=0
	}
	1031.3.8={
		holder=10120036 #　Minamoto no Yorikiyo
	}
	1032.1.1={
		holder=0
	}
	1098.3.2={
		holder=11006330 #　Fujiwara no Tsunetada
	}
	1106.4.16={
		holder=0
	}
	1111.5.27={
		holder=11006881 # Fujiwara no Tadamichi
	}
	1118.2.10={
		holder=0
	}
	1146.3.15={
		holder=10110025 # Taira no Kiyomori
	}
	1147.2.3={
		holder=0
	}
	1153.4.8={
		holder=10110025 # Taira no Kiyomori
	}
	1156.7.29={
		holder=0
	}
	1156.10.3={
		holder=10110031 #　Taira no Tsunemori
	}
	1156.10.8={
		holder=10110033 # Taira no Yorimori
	}
	1158.8.10={
		holder=0
	}
	1179.12.18={
		liege = "e_nmih_japan"
		holder=12660401 # Saeki Kagehiro
	}
	1183.8.17={
		liege= "k_nmih_western_chugoku"
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1186.1.1={
		holder=12360103 # Takeda Nobumitsu
	}
	1196.1.1={
		liege="e_nmih_japan"
		holder=12660700 #　Sou Takachika
	}
	1221.7.1={
		liege="e_nmih_tenno"
	}
	1221.7.22={
		liege=0
		holder=0
	}
	1222.1.1={
		liege="e_nmih_japan"
		holder=12360103 # Takeda Nobumitsu
	}
	1235.6.1={
		holder=0 #　Fujiwara no Chikazane
	}
	1243.1.1={
		holder=12360120 # Takeda Nobutoki
	}
	1284.1.1={
		holder=10132022 #　Hojo Munenaga
	}
	1309.8.28={
		holder=12360155 #　Takeda Nobumune
	}
	1332.1.1={
		holder=10020001 #　Takeda Nobutake
	}
	1333.7.4={
		liege=0
	}
	1333.8.1={
		liege="e_nmih_tenno" # Emperor Godaigo
	}
	1335.12.1={
		liege="e_nmih_japan" # Ashikaga Takauji
	}
	1336.4.1={
		holder=12187221 # Momonoi Yoshimori
	}
	1336.7.5={
		holder=10020001 #　Takeda Nobutake
	}
	1349.10.26={
		liege="e_nmih_japan" # Ashikaga Takauji
	}
	1351.1.1={
		holder=10020003 #　Takeda Ujinobu
	}
	1370.12.1={
		liege=0
		holder=0
	}
	1371.3.1={
		liege="e_nmih_japan"
		holder=12395221 #　Imagawa Sadayo
	}
	1454.11.1={
		liege="e_nmih_japan"
		holder=10050044 # Yamana Noritoyo
	}
	1462.1.1={
		holder=10050041 #　Yamana Koretoyo
	}
	1475.11.1={
		holder=10050044 #　Yamana Masatoyo
	}
	1493.5.8={ # Meiou Coup
		liege=0
	}
	1499.3.13={
		liege=0
		holder=0
	}
	1550.1.1={
		liege="k_nmih_western_chugoku"
		holder=10056004 # Mori Motonari
	}
	1551.9.30={
		liege= 0
	}
	1557.5.1={
		liege="k_nmih_western_chugoku"
	}
	1571.7.6={
		holder=10056025 # Mori Terumoto
	}

}
