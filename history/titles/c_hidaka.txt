c_nmih_hidaka = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_kii"
	}
	1180.1.1={
		liege="e_nmih_japan"
		holder=12550030 # Kumano Tanzo
	}
	1180.9.23={
		liege=0 # Rebellion of Kumano
	}
	1198.6.14={
		liege=0
		holder=0 # 
	}
	1400.1.1={
		liege="d_nmih_kii"
	}
	1420.1.1={
		holder=10048605 # Yukawa Mochiharu
	}
	1450.1.1={
		holder=10048604 # Yukawa Kazuharu
	}
	1460.1.1={
		holder=10048603 # Yukawa Noriharu
	}
	1470.1.1={
		holder=10048602 # Yukawa Katsuharu
	}
	1493.5.8={
		liege="d_nmih_kawachi"
	}
	1500.1.1={
		holder=10048600 # Yukawa Masaharu
	}
	1521.1.1={
		liege="d_nmih_kii"
	}
	1530.1.1={
		holder=10048601 # Yukawa Naomitsu
	}
	1562.6.20={
		holder=10048610 # Yukawa Naoharu
	}

}
