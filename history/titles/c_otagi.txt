c_nmih_otagi = {
	1.1.1 = { change_development_level = 40 }
	1121.1.1 = { change_development_level = 50 }
	1179.12.18 = { change_development_level = 50 }
	1378.1.1 = { change_development_level = 70 }

	0701.1.1={
		liege="d_nmih_yamashiro"
	}
	1179.12.18={
		liege="e_nmih_japan" # Taira Kiyomori
		holder=10110025 # Taira Kiyomori
	}
	1181.3.20={
		holder=10110042 # Taira Munemori
	}
	1183.8.17={
		holder=10120084 # Minamoto Yoshinaka. Taira's escape to the west
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1394.1.1={
		liege="e_nmih_japan"
		holder = 10040004 # Ashikaga Yoshimochi
	}
	1423.3.18={
		holder = 10040005 # Ashikaga Yoshikazu
	}
	1425.3.17={
		holder = 10040004 # Ashikaga Yoshimochi (again)
	}
	1429.3.15={
		holder = 10040006 # Ashikaga Yoshinori
	}
	1441.7.12={ # Ashikaga Yoshinori was assassinated.
		holder = 10040007 # Ashikaga Yoshikatsu
	}
	1443.8.16={ # Ashikaga Yoshikatsu died.
		holder = 10040008 # Ashikaga Yoshimasa
	}
	1473.12.19={
		holder = 10040009 # Ashikaga Yoshihisa
	}
	1489.4.26={ # Ashikaga Yoshihisa died.
		holder = 10040011 # Ashikaga Yoshiki (Yoshitane)
	}
	1493.5.8={ # Meiou Coup
		holder = 10040012 # Ashikaga Yoshizumi
	}
	1508.7.28={
		holder = 10040011 # Ashikaga Yoshiki (Yoshitane)
	}
	1522.1.22={
		holder = 10040000 # Ashikaga Yoshiharu
	}
	1527.3.14={
		liege="k_nmih_kanrei"
		holder=10040500 # Hosokawa Harumoto
	}
	1534.1.1={
		liege="e_nmih_japan"
		holder = 10040000 # Ashikaga Yoshiharu
	}
	1547.1.11={
		holder=10040040 # Ashikaga Yoshiteru
	}
	1549.7.18={
		liege="k_nmih_southern_kinai"
		holder=10059007 # Miyoshi Nagayoshi
	}
	1552.1.1={
		liege="e_nmih_japan"
		holder=10040040 # Ashikaga Yoshiteru
	}
	1553.9.8={
		liege="k_nmih_southern_kinai"
		holder=10059007 # Miyoshi Nagayoshi
	}
	1558.11.1={
		liege="e_nmih_japan"
		holder=10040040 # Ashikaga Yoshiteru
	}
	1565.6.17={
		liege="k_nmih_southern_kinai"
		holder=10059014 # Miyoshi Yoshitsugu
	}
	1567.5.24={
		liege="d_nmih_yamashiro"
		holder=10059006 # Miyoshi Nagayasu
	}
	1568.10.19={
		liege="d_nmih_yamashiro"
		holder=10029120 # Oda Nobunaga
	}
	1568.11.7={
		liege="d_nmih_yamashiro"
		holder=10040042 # Ashikaga Yoshiaki
	}
	1573.8.25={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1575.12.9={
		liege="e_nmih_japan" # 29120 Oda Nobunaga
	}
	1582.6.21={
		liege="k_nmih_northern_kinai"
		holder=10025632 # Akechi Mitsuhide
	}
	1582.7.2={
		liege="k_nmih_northern_kinai"
		holder=10029400 # Hashiba Hideyoshi
	}
}
