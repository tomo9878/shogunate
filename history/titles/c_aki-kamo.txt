c_nmih_aki-kamo = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }
	1525.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_aki"
	}
	1180.1.1={
		liege = "d_nmih_aki"
		holder=12660503 # Nuta Goro
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1402.9.16={
		liege="k_nmih_western_chugoku" # Ouchi Moriharu
		holder=10056406 #　Hiraga Yorimune
	}
	1467.7.9={
		liege="k_nmih_western_chugoku" # Ouchi Masahiro
		holder=10056405 #　Hiraga Hiromune
	}
	1470.1.1={
		liege="k_nmih_western_chugoku"
		holder=10056401 # Hiraga Hiroyori
	}
	1492.6.5={
		holder= 10056400 # Hiraga Hiroyasu
	}
	1525.1.1={
		holder = 10056900 # Hironaka Okikatsu
	}
	1542.1.1={
		holder = 10056901 # Hironaka Takakane
	}
	1555.10.28={
		holder = 10056404 # Hiraga Hirosuke
	}
	1567.4.26={
		holder = 10056421 # Hiraga Motosuke
	}

}
