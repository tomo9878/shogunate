d_nmih_musashi = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	0806.2.24={
		holder=11000024 # Fujiwara Uchimaro {Musashi no Kami}
	}
	0807.8.1={
		holder=11000025 # Fujiwara Manatsu {Musashi no Kami}
	}
	0807.9.29={
		holder=0 #  
	}
	0885.2.8={
		holder=11003527 # Fujiwara Sadamoto {Musashi no Kami}
	}
	0906.9.24={
		holder=0 #  
	}
	0919.1.1={
		holder=11004699 # Fujiwara Toshihito {Musashi no Kami}
	}
	0920.1.1={
		holder=0 #  
	}
	0940.4.24={
		holder=11004609 # Fujiwara Hidesato {Musashi no Kami}
	}
	0943.1.1={
		holder=0 #  
	}
	0949.4.6={
		holder=10120002 # Minamoto Mitsunaka {Musashi no Kami}
	}
	0953.1.1={
		holder=0 #  
	}
	1128.1.17={
		holder=11001035 # Fujiwara Kinnobu {Musashi no Kami}
	}
	1135.6.11={
		holder=0 #  
	}
	1166.12.14={
		holder=10110025 # Taira Kiyomori {Chigyo-kokushi (sovereign of the fief)}
	}
	1180.1.1={
		liege = 0 # De facto
		holder=12200130 # Kawagoe Shigeyori
	}
	1180.10.24={
		liege=0
		holder=0 # served to MINAMOTO no Yoritomo
	}
	1186.4.11={
		holder=10120075 # Minamoto Yoritomo
	}
	1199.2.9={
		holder=0 #  
	}
	1204.5.9={
		holder=10120087 # Minamoto Sanetomo
	}
	1207.2.6={
		holder=10131655 # Hojo Tokifusa
	}
	1240.1.1={
		holder=10131666 # Hojo Yasutoki
	}
	1242.7.14={
		holder=10131823 # Hojo Tsunetoki
	}
	1246.5.17={
		holder=10131824 # Hojo Tokiyori
	}
	1256.11.1={
		holder=10131711 # Hojo Nagatoki
	}
	1264.9.12={
		holder=10131998 # Hojo Tokimune
	}
	1272.11.1={
		holder=0 #  
	}
	1279.12.1={
		holder=10132173 # Hojo Tokimori
	}
	1292.10.1={
		holder=10132155 # Hojo Sadatoki
	}
	1311.12.6={
		holder=10132268 # Hojo Takatoki
	}
	1333.7.4={
		holder=0 #  
	}
	1368.7.2={
		liege="k_nmih_kanto_kanrei"
		holder=10017006 # Uesugi Noriaki
	}
	1368.10.31={
		holder=10017008 # Uesugi Yoshinori
	}
	1378.5.14={
		holder=10017010 # Uesugi Noriharu
	}
	1379.3.25={
		holder=10017011 # Uesugi Norikata
	}
	1394.11.17={
		holder=10017047 # Uesugi Tomomune
	}
	1409.9.1={
		holder=10017048 # Uesugi Ujinori (Zenshu)
	}
	1415.6.8={
		liege=0 # Uesugi Zenshu's rebellion
	}
	1417.1.27={
		liege=0
		holder=0 # 
	}
	1418.2.9={
		liege="k_nmih_kanto_kanrei"
		holder=10017020 # Uesugi [Yamauchi] Norizane
	}
	1439.3.24={
		holder=10032805 # Uesugi Kiyokata
	}
	1444.1.1={
		holder=10017021 # Uesugi Noritada
	}
	1455.1.15={
		holder=10017022 # Uesugi Fusaaki
	}
	1457.1.1={
		liege=0
		holder=10017037 # Uesugi [Ougigaya] Mochitomo
	}
	1467.10.4={
		holder=10017042 # Uesugi Masazane
	}
	1473.12.22={
		holder=10017040 # Uesugi Sadamasa
	}
	1494.11.20={
		holder=10017043 # Uesugi Tomoyoshi
	}
	1518.5.30={
		holder=10018000 # Uesugi Tomooki
	}
	1537.6.4={
		holder=10018001 # Uesugi Tomosada
	}
	1546.5.19={
		holder=10016004 # Hojo Ujiyasu
	}
	1571.10.21={
		holder=10016014 # Hojo Ujimasa
	}
}
