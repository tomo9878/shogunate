c_nmih_kaga = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1184.3.4 = { change_development_level = 30 }
	1442.1.1 = { change_development_level = 30 }
	1460.1.1 = { change_development_level = 30 }
	1546.1.1 = { change_development_level = 40 }
	1580.5.2 = { change_development_level = 50 }

	0701.1.1={
		liege="d_nmih_echizen"
	}
	0823.1.1={
		liege="d_nmih_kaga"
	}
	1180.1.1={
		liege="d_nmih_kaga"
		holder=12300100 # Inoie Norikata
	}
	1183.6.1={
		holder=12300110 # Inoie Morokata
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1334.1.1={
		liege="d_nmih_kaga"
		holder=12310130 # Togashi Takaie
	}
	1351.1.1={
		holder=12310140 # Togashi Ujiharu
	}
	1358.1.1={
		holder=12310150 # Togashi Masaie
	}
	1387.4.1={
		holder=12310160 # Togashi Akichika
	}
	1392.1.24={
		holder=12310170 # Togashi Mitsuharu
	}
	1427.7.3={
		holder=12310180 # Togashi Mochiharu
	}
	1433.8.24={
		holder=12310181 # Togashi Noriie
	}
	1441.7.6={
		liege="d_nmih_kawachi"
		holder=12310181 # Togashi Noriie
	}
	1442.1.1={
		liege="k_nmih_honganji"
		holder=10042216 # Honganji Nyojo
	}
	1460.2.18={
		holder=10042214 # Honganji Renjo
	}
	1483.1.1={
		holder=10042206 # Honsenji Rengo
	}
	1488.7.17={
		liege="d_nmih_kaga"
	}
	1531.11.26={
		holder=10042200 # Honganji Shonyo
	}
	1554.9.9={
		holder=10042210 # Honganji Kennyo
	}
	1580.5.2={
		liege="e_nmih_japan" # 29120 Oda Nobunaga
		holder=10029280 # Shibata Katsuie
	}
	1580.12.7={
		liege="d_nmih_kaga" # 29280 Shibata Katsuie
		holder=10029514 # Sakuma Morimasa
	}

}
