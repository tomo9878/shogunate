c_nmih_satsuma = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1331.1.1 = { change_development_level = 30 }

	10.1.1={
	}
	20.1.1={
		holder=10290300 # Sori
	}
	70.1.1={
		holder=10290301 # Kibe
	}
	100.1.1={
		holder=10290302 # Toki
	}
	150.1.1={
		holder=10290307 # Ishikori
	}
	170.1.1={
		holder=10290308 # Tomami
	}
	230.1.1={
		holder=10290309 # Atasumi
	}
	1161.1.1={
		liege="d_nmih_satsuma"
		holder=12810512 # Taira Nobuzumi
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1300.1.1={
		liege="d_nmih_satsuma"
	}
	1422.1.1={
		holder=10060004 # Shimazu Hisatoyo
	}
	1425.2.18={
		holder=10060005 # Shimazu Tadakuni
	}
	1470.2.21={
		holder=10060008 # Shimazu Tatsuhisa
	}
	1474.4.26={
		holder=10060011 # Shimazu Tadamasa
	}
	1508.3.16={
		holder=10060012 # Shimazu Tadaharu
	}
	1515.10.2={
		holder=10060013 # Shimazu Tadataka
	}
	1519.5.12={
		holder=10060002 # Shimazu Katsuhisa
	}
	1527.1.1={
		holder=10060000 # Shimazu Takahisa
	}
	1571.7.15={
		holder=10060024 # Shimazu Yoshihisa
	}
}
