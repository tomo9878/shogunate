c_nmih_nishi_matsura = {
	1.1.1 = { change_development_level = 10 }
	0500.1.1 = { change_development_level = 20 }
	1300.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	240.1.1={
		holder=10290031 # Tsushikori
		liege=d_nmih_chikuzen
	}
	280.1.1={
		holder=10290032 # Omi
		liege=0
	}
	1120.1.1={
		holder=12750600 # Matsura Hisashi
	}
	1148.10.29={
		holder=12750610 # Matsura Naoshi
	}
	1155.1.1={
		holder=12750621 # Matsura Kiyoshi
	}
	1180.1.1={
		liege="d_nmih_iki"
	}
	1185.4.25={
		liege=0 # Battle of Dan-no-ura
	}
	1200.1.1={
		holder=12750630 # Matsura Meguru
	}
	1220.1.1={
		holder=12750635 # Matsura Naoshi
	}
	1230.1.1={
		holder=12750640 # Matsura Sadamu
	}
	1300.1.1={
		holder=10068234 # Matsura Masashi
	}
	1340.1.1={
		holder=10068235 # Matsura Kiyoshi
	}
	1360.1.1={
		holder=10068236 # Matsura Sadamu
	}
	1370.1.1={
		holder=10068237 # Matsura Masaru
	}
	1400.1.1={
		holder=10068238 # Matsura Nobiru
	}
	1420.1.1={
		holder=10068239 # Matsura Susumu
	}
	1440.1.1={
		holder=10068240 # Matsura Sakou
	}
	1467.1.1={
		holder=10068241 # Matsura Sadamu
	}
	1492.1.1={
		holder=10068242 # Matsura Masashi
	}
	1498.12.14={
		holder=10068201 # Matsura Hirosada
	}
	1515.1.1={
		holder=10068200 # Matsura Okinobu
	}
	1541.1.1={
		holder=10068202 # Matsura Takanobu
	}
	1573.1.1={
		liege=d_nmih_hizen
	}
}
