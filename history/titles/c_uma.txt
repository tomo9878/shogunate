c_nmih_uma = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 30 }
	1561.1.1 = { change_development_level = 30 }

	100.1.1={
		liege="d_nmih_iyo"
	}
	1180.1.1={
		liege=0
		holder=12690400 # Nii Morinobu
	}
	1185.3.22={
		liege=0
		holder=0 # Battle of Yashima
	}
	1397.6.2={
		liege="d_nmih_sanuki"
		holder=10040545 # Hosokawa Mitsukuni
	}
	1420.1.1={
		holder=10040546 # Hosokawa Mochiharu
	}
	1466.2.24={
		holder=10040547 # Hosokawa Noriharu
	}
	1475.1.1={
		holder=10040555 # Hosokawa Masaharu
	}
	1508.1.1={
		liege="k_nmih_northern_kinai"
	}
	1508.7.28={
		liege="d_nmih_sanuki"
	}
	1518.2.18={
		holder=10040564 # Hosokawa Harukuni
	}
	1527.3.14={
		liege="k_nmih_northern_kinai"
	}
	1536.9.14={
		liege=0
		holder=10040569 # Hosokawa Michitada
	}
	1561.1.1={
		liege="k_nmih_southern_kinai"
		holder=10058680 # Ishikawa Michikiyo
	}
	1564.8.10={
		liege="d_nmih_iyo"
	}
	1582.8.4={
		liege="d_nmih_tosa"
	}

}
