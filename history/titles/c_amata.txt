c_nmih_amata = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 20 }
	1579.9.10 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_tanba"
	}
	1179.12.18={
		liege="d_nmih_tanba"
		holder=10110141 # Taira Kiyokuni
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1397.6.2={
		liege="d_nmih_tanba"
		holder=10040536 # Hosokawa Mitsumoto
	}
	1422.1.1={
		holder=10040730 # Kozai Motosuke
	}
	1431.1.1={
		holder=10042055 # Naito Shinsho
	}
	1440.1.1={
		holder=10042056 # Naito Yukisada
	}
	1452.1.1={
		holder=10042050 # Naito Motosada
	}
	1482.1.1={
		holder=10040750 # Uehara Kataie
	}
	1487.1.1={
		holder=10040752 # Uehara Motohide
	}
	1493.12.26={
		holder=10040750 # Uehara Kataie
	}
	1496.1.14={
		holder=10042050 # Naito Motosada
	}
	1501.1.1={
		holder=10042040 # Shiomi Yorikatsu
	}
	1530.1.1={
		holder=10042041 # Yokoyama Yoriuji
	}
	1560.1.1={
		holder=10042042 # Yokoyama Nobufusa
	}
	1565.8.27={
		liege=0
		holder=10042066 # Akai Naomasa
	}
	1576.2.10={
		liege="d_nmih_tanba"
	}
	1578.4.8={
		holder=10042071 # Akai Naoyoshi
	}
	1579.6.24={
		liege=0
	}
	1579.9.10={
		liege="d_nmih_tanba"
		holder=10025632 # Akechi Mitsuhide
	}
	1580.1.1={
		holder=10025639 # Akechi Hidemitsu
	}
	1582.7.2={
		liege="d_nmih_tanba"
		holder=10029452 # Sugihara Ietsugu
	}

}
