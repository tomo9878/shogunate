c_nmih_shimane = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_izumo"
	}
	1180.1.1={
		liege="d_nmih_izumo"
		holder=12600900 # Ono Sueaki
	}
	1183.8.17={
		liege="k_nmih_western_chugoku" # Taira's escape to the west
	}
	1190.1.1={
		liege=0
		holder=0 # 
	}
	1440.1.1={
		liege="d_nmih_izumo" # Kyogoku Mochikiyo
		holder=12601312 # Matsuda Kimiyori
	}
	1468.6.20={
		liege=0 # rebellion
		holder=12601312 # Matsuda Kimiyori
	}
	1468.10.7={
		holder=12601313 # Matsuda Mikawa no kami
	}
	1471.9.30={
		liege="d_nmih_izumo"
		holder=10051503 # Amago Kiyosada
	}
	1479.1.1={
		holder=10051501 # Amago Tsunehisa, Kiyosada in retirement
	}
	1500.1.1={
		liege="k_nmih_sanin"
	}
	1541.9.30={
		holder = 10051508 # Amago Haruhisa
		liege="d_nmih_izumo"
	}
	1561.1.9={
		holder = 10051525 # Amago Yoshihisa
	}
	1563.1.1={
		liege = "k_nmih_western_chugoku"
		holder=10056004 # Mori Motonari
	}
	1569.1.1={
		holder = 10056022 # Mori(Suetsugu) Motoyasu
	}

}
