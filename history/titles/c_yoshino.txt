c_nmih_yoshino = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 30 }
	1331.1.1 = { change_development_level = 30 }

	0701.1.1={
		liege="d_nmih_yamato"
	}
	1180.1.1={
		liege="k_nmih_yamashiro"
		holder=12540105 # Uno Chikaharu
	}
	1180.6.20={
		liege=0
	}
	1186.9.21={
		holder=0
	}
	1443.10.19={
		holder=10041943 # Yamato Takahide
	}
	1457.12.18={
		liege="d_nmih_kawachi"
		holder=10048115 # Hatakeyama Yoshihiro
	}
	1460.10.4={
		liege=0
	}
	1466.10.1={
		liege="d_nmih_kawachi"
		holder=10048200 # Ochi Iehide
	}
	1500.4.4={
		holder=10048201 # Ochi Ienori
	}
	1507.1.1={
		holder=10048202 # Ochi Ienori
	}
	1517.5.7={
		holder=10048203 # Ochi Iehide
	}
	1521.1.1={
		liege="d_nmih_kii"
	}
	1530.1.1={
		holder=10048204 # Ochi Iehiro
	}
	1534.1.1={
		liege=0
	}
	1550.1.1={
		liege="d_nmih_shima"
		holder=10026000 # Kitabatake Harutomo
	}
	1553.1.1={
		holder=10026003 # Kitabatake Tomonori
	}
	1560.1.1={
		liege=0
		holder=10048204 # Ochi Iehiro
	}
	1569.1.1={
		holder=10048209 # Ochi Ietaka
	}
	1571.1.1={
		holder=10048208 # Ochi Iemasu
	}
	1576.8.24={
		holder=10048210 # Ochi Iehide
	}
	1577.1.1={
		liege="d_nmih_yamato" # 48413 Tsutsui Junkei
	}

}
