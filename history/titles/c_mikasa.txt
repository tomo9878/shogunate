c_nmih_mikasa = {
	1.1.1 = { change_development_level = 40 }
	1331.1.1 = { change_development_level = 40 }

	100.1.1={
	}
	220.1.1={
		holder=10290035 # Shimako
		liege=d_nmih_chikuzen
	}
	1180.1.1={
		liege="d_nmih_chikuzen"
		holder=12750100 # Harada Taneto
	}
	1185.3.4={
		liege=0
		holder=0 # Battle of Ashiya-no-ura
	}
	1400.1.1={
		liege=d_nmih_hakata
	}
	1410.1.1={
		holder=10067863 # Fujiwara Sokin
	}
	1455.1.1={
		holder=10067862 # So Seishun
	}
	1500.1.1={
		holder=10067801 # Shimai Kazue no Suke Shigeyoshi
	}
	1530.1.1={
		holder=10067830 # Kamiya Jutei
	}
	1546.10.22={
		holder=10067890 # Suetsugu Kozen
	}
	1580.1.1={
		holder=10067920 # Kawakami Mokuzaemon
	}
}
