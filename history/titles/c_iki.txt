c_nmih_iki = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	100.1.1={
	}
	230.1.1={
		holder=10290045 # Ijiki
		liege=d_nmih_chikuzen
	}
	1180.1.1={
		liege="d_nmih_iki"
		holder=12750701	# Matsura Takatoshi
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1472.1.1={
		holder=10069003 # Hata Yasushi
	}
	1485.1.1={
		holder=10069001 # Hata Osamu
	}
	1528.1.1={
		holder=10069000 # Hata Okoshi
	}
	1543.1.1={
		holder=10069002 # Hata Satoshi
	}
	1547.1.1={
		holder=10068611 # Hata Chikashi
	}
	1564.1.1={
		holder=10069012 # Hata Omoshi
	}
	1571.1.1={
		holder=10068202 # Matsura Takanobu
	}
	1573.1.1={
		liege=d_nmih_hizen
	}
}
