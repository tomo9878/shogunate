c_nmih_mike = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1185.4.25 = { change_development_level = 30 }

	100.1.1={
	}
	180.1.1={
		holder=10290001 # Himiko
	}
	248.1.1={
		holder=10290005 # Toyo
	}
	270.1.1={
		holder=10290006 # Nukate
	}
	310.1.1={
		holder=10290007 # Semoko
	}
	340.1.1={
		holder=10290009 # Natsuha
	}
	1180.1.1={
		liege="d_nmih_chikuzen"
		holder=12750200 # Mike Atsutane
	}
	1185.3.4={
		liege=0
		holder=0 # Battle of Ashiya-no-ura
	}
	1400.1.1={
		liege=d_nmih_bungo
	}
	1470.1.1={
		holder=10064502 # Kamachi Takehisa
	}
	1526.1.1={
		holder=10064501 # Kamachi Shigehisa
	}
	1545.1.1={
		holder=10064504 # Kamachi Akimori
	}
	1578.12.10={
		holder=10064514 # Kamachi Shigenami
	}
	1580.1.1={
		liege=d_nmih_hizen # Ryuzoji Takanobu
	}
	1581.7.18={
		holder=10068105 # Nabeshima Naoshige
	}
}
