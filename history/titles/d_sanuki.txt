d_nmih_sanuki = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	1179.12.18={
		liege = "k_nmih_sanyo"
		holder=10110255 # Taira Moritsuna
	}
	1181.3.20={
		liege = "e_nmih_japan"
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1300.1.1={
		liege="k_nmih_kanrei"
	}
	1337.1.1={
		holder=10040579 # Hosokawa Akiuji
	}
	1352.1.1={
		holder=10040580 # Hosokawa Shigeuji
	}
	1359.1.1={
		holder=10040535 # Hosokawa Yoriyuki
	}
	1392.3.25={
		holder=10040538 # Hosokawa Mitsumoto
	}
	1426.11.15={
		holder=10040541 # Hosokawa Mochimoto
	}
	1429.8.14={
		holder=10040539 # Hosokawa Mochiyuki
	}
	1442.9.8={
		holder=10040543 # Hosokawa Katsumoto
	}
	1473.6.6={
		liege="k_nmih_kanrei"
		holder=10040544 # Hosokawa Masamoto
	}
	1507.8.1={
		holder=10040567 # Hosokawa Sumiyuki
	}
	1507.9.7={
		holder=10040501 # Hosokawa Sumimoto
	}
	1508.7.28={
		holder=10040566 # Hosokata Takakuni
	}
	1527.3.14={
		holder=10040500 # Hosokawa Harumoto
	}
	1534.1.1={
		holder = 10059007 # Miyoshi Nagayoshi
	}
	1548.10.28={
		liege="k_nmih_southern_kinai"
		holder=10059011 # Sogo(Miyoshi) Kazumasa
	}
	1561.4.2={
		holder=10059008 # Miyoshi Koretora
	}
	1562.4.8={
		holder=10059018 # Miyoshi Nagaharu
	}
	1567.5.24={
		liege=0
	}
	1577.4.16={
		holder=10059019 # Miyoshi Masayasu
	}
}
