c_nmih_kawanuma = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }

	100.1.1={
	}
	1180.1.1={
		liege="d_nmih_echigo"
		holder=12062220 # Tachikawa Jiro
	}
	1181.7.25={
		liege="k_nmih_northern_oshu"
		holder=12030020 # Fujiwara Hidehira, Battle of Yokota-gawara
	}
	1187.11.30={
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.9.21={
		liege="k_nmih_western_kanto" # Battle of Mountain Atsukashi
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		holder=10130571 # Yamauchi-Sudo Michimoto
	}
	1223.1.1={
		holder=12064500 # Yamanouchi Hidemoto
	}
	1270.1.1={
		holder=12064505 # Yamanouchi Toshiharu
	}
	1310.1.1={
		holder=12064510 # Yamanouchi Yoritoshi
	}
	1350.1.1={
		holder=12064515 # Yamanouchi Toshimoro
	}
	1390.1.1={
		holder=12064520 # Yamanouchi Toshisuke
	}
	1425.1.1={
		liege=0
		holder=12064525 # Yamanouchi Toshiakira
	}
	1475.1.1={
		holder=12064530 # Yamanouchi Toshimitsu
	}
	1490.1.1={
		liege="d_nmih_aizu"
		holder=10007950 # Yamanouchi Toshiyasu
	}
	1535.1.1={
		holder=10007951 # Yamanouchi Toshikiyo
	}
	1544.1.1={
		holder=10007952 # Yamanouchi Kiyomichi
	}
	1570.1.1={
		holder=10007956 # Yamanouchi Ujikatsu
	}
}
