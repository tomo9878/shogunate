d_nmih_awa = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	1180.1.1={
		liege = "k_nmih_sanyo"
		holder=12690001 # Awata Shigeyoshi
	}
	1181.3.20={
		liege = "e_nmih_japan"
	}
	1183.8.17={
		liege= "k_nmih_western_chugoku"
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1336.1.1={
		liege="e_nmih_japan"
	}
	1392.3.25={
		holder = 10040524 # Hosokawa Yoshiyuki
	}
	1402.1.1={
		holder = 10040525 # Hosokawa Mitsuhisa
	}
	1430.10.15={
		holder = 10040526 # Hosokawa Mochitsune
	}
	1450.1.28={
		liege="k_nmih_kanrei"
		holder = 10040528 # Hosokawa Shigeyuki
	}
	1479.1.1={
		holder = 10040530 # Hosokawa Masayuki
	}
	1488.9.4={
		holder = 10040529 # Hosokawa Yoshiharu
	}
	1495.1.17={
		holder = 10040532 # Hosokawa Yukimochi
	}
	1508.7.28={
		liege="k_nmih_southern_kinai"
		holder=10040501 # Hosokawa Sumimoto
	}
	1520.6.24={
		holder=10040500 # Hosokawa Harumoto
	}
	1527.3.14={
		liege="k_nmih_kanrei"
	}
	1534.1.1={
		holder=10040534 # Hosokawa Mochitaka
	}
	1548.10.28={
		liege="k_nmih_southern_kinai"
		holder=10059008 # Miyoshi Koretora, De facto
	}
	1562.4.8={
		holder=10059018 # Miyoshi Nagaharu
	}
	1567.5.24={
		liege=0
	}
	1577.4.16={
		holder=10059019 # Miyoshi Masayasu
	}
	1582.10.7={
		holder=10059104 # Chosokabe Motochika
	}
}
