c_nmih_tasai = {
	1.1.1 = { change_development_level = 10 }
	1521.1.1 = { change_development_level = 10 }
	1571.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_musashi"
	}
	1180.1.1={
		liege=0
		holder=12205040 # Yokoyama Tokihiro
	}
	1180.10.26={
		liege="k_nmih_western_kanto" # Yoritomo's conquest of Kamakura
		holder=12205040 # Yokoyama Tokihiro
	}
	1189.1.1={
		holder=12205050 # Yokoyama Tokikane
	}
	1192.7.12={
		liege="e_nmih_japan"
	}
	1213.5.25={
		holder=10131653 # Hojo Yoshitoki
	}
	1224.7.1={
		liege=0
		holder=0
	}
	1356.1.1={
		liege="d_nmih_musashi"
		holder=10018460 # Oishi Nobushige
	}
	1415.6.8={
		liege="k_nmih_kanto_kanrei"
	}
	1418.2.9={
		liege="d_nmih_musashi"
	}
	1424.1.1={
		holder=10018461 # Oishi Norishige
	}
	1429.1.1={
		holder=10018462 # Oishi Noriyoshi
	}
	1441.1.1={
		holder=10018463 # Oishi Fusashige
	}
	1455.2.7={
		holder=10018464 # Oishi Akishige
	}
	1457.1.1={
		liege="k_nmih_kanto_kanrei"
	}
	1490.1.1={
		holder=10018465 # Oishi Sadashige
	}
	1527.11.3={
		holder=10018466 # Oishi Sadashige
	}
	1546.5.19={
		liege="d_nmih_musashi"
		holder=10018467 # Oishi Sadanaka
	}
	1559.1.1={
		holder=10016015 # Hojo Ujiteru
	}
}
