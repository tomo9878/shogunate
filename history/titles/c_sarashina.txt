c_nmih_sarashina = {
	1.1.1 = { change_development_level = 10 }
	1260.1.1 = { change_development_level = 20 }
	1556.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_shinano"
	}
	1160.1.1={
		holder=12385000 # Murakami Tamekuni
	}
	1180.6.15={
		liege="k_nmih_shinano" # Minamoto Yoshinaka
	}
	1183.6.22={
		liege="k_nmih_hokuriku" # Minamoto Yoshinaka
	}
	1183.8.17={
		liege="e_nmih_japan" # Minamoto Yoshinaka
		holder=12385000 # Murakami Tamekuni / Taira's escape to the west
	}
	1184.1.3={
		liege="k_nmih_western_kanto" # Minamoto Yoritomo
		holder=12385000 # Murakami Tamekuni / Siege of Hojujidono
	}
	1185.1.1={
		liege="k_nmih_western_kanto"
		holder=12385012 # Murakami Nobukuni
	}
	1185.4.25={
		liege="d_nmih_shinano"
	}
	1205.1.1={
		holder=12385020 # Murakami Yasunobu
	}
	1235.1.1={
		holder=12385025 # Murakami Nobumura
	}
	1260.1.1={
		holder=12385030 # Murakami Tanenobu
	}
	1285.1.1={
		holder=12385035 # Murakami Nobuyasu
	}
	1310.1.1={
		holder=12385040 # Murakami Yoshiteru
	}
	1333.2.3={
		holder=12385041 # Murakami Nobusada
	}
	1350.1.1={
		holder=12385045 # Murakami Morokuni
	}
	1390.1.1={
		holder=12385050 # Murakami Mitsunobu
	}
	1410.1.1={
		holder=12385055 # Murakami Mochikiyo
	}
	1435.1.1={
		liege=0
		holder=12385060 # Murakami Yorikiyo
	}
	1460.1.1={
		holder=12385085 # Murakami Mitsukiyo
	}
	1470.1.1={
		holder=10021502 # Murakami Masakuni
	}
	1494.1.1={
		holder=10021501 # Murakami Akikuni
	}
	1528.1.1={
		holder=10021500 # Murakami Yoshikiyo
	}
	1553.9.1={
		liege="d_nmih_kai" # Takeda Shingen
		holder=10020010 # Takeda Shingen
	}
	1556.1.1={
		liege="d_nmih_kai"
		holder=10020075 # Baba Nobuharu
	}
	1559.1.1={
		liege="d_nmih_shinano" # Takeda Shingen
	}
	1575.6.29={
		liege="d_nmih_shinano"
		holder=10020076 # Baba Masafusa
	}
	1582.4.3={
		liege="e_nmih_japan"
		holder=10029120 # Oda Nobunaga
	}
	1582.04.15={
		liege="k_nmih_chubu" # 29160 Oda Nobutada
		holder=10025421 # Mori Nagayoshi
	}
	1582.6.21={ # Honno-ji Incident
		liege=0
		holder=10021182 # Yashiro Hidemasa
	}
	1582.7.13={
		liege="d_nmih_echigo"
	}
}
