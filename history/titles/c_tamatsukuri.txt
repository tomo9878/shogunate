c_nmih_tamatsukuri = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1189.9.21 = { change_development_level = 30 }

	100.1.1={
	}
	1180.1.1={
		liege="k_nmih_northern_oshu"
		holder=12062200 # Terui Takaharu
	}
	1189.9.21={
		liege=0
		holder=0 # Battle of Mountain Atsukashi
	}
	1354.1.1={
		liege="d_nmih_rikuzen"
		holder=12081005 # Ujiie Akitsugu
	}
	1385.1.1={
		holder=12081010 # Ujiie Naotsugu
	}
	1410.1.1={
		holder=12081015 # Ujiie Naotaka
	}
	1435.1.1={
		holder=12081020 # Ujiie Takatsugu
	}
	1460.1.1={
		holder=12081026 # Ujiie Takatoki
	}
	1485.1.1={
		holder=12081030 # Ujiie Naotoshi
	}
	1510.1.1={
		holder=12081035 # Ujiie Naomasu
	}
	1540.1.1={
		holder=12081040 # Ujiie Takatsugu
	}
	1580.1.1={
		holder=12081050 # Ujiie Yoshitsugu
	}

}
