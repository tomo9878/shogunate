c_nmih_kadono = {
	1.1.1 = { change_development_level = 30 }
	0794.1.1 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_yamashiro"
	}
	1179.12.18={
		liege="e_nmih_japan" # Taira Kiyomori
		holder=10110025 # Taira Kiyomori
	}
	1181.3.20={
		liege="e_nmih_japan" # Taira Munemori
		holder=10110042 # Taira Munemori
	}
	1183.8.17={
		liege="e_nmih_japan" # Minamoto Yoshinaka
		holder=10120084 # Minamoto Yoshinaka. Taira's escape to the west
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1467.6.27={
		liege="k_nmih_sanin"
		holder=10050039 # Yamana Mochitiyo
	}
	1473.4.15={
		holder=10050044 # Yamana Masatoyo
	}
	1474.4.19={
		liege="k_nmih_western_chugoku"
		holder=10057007 # Ouchi Masahiro
	}
	1477.12.16={
		liege="d_nmih_yamashiro"
		holder=10048121 # Hatakeyama Masanaga
	}
	1486.5.1={
		liege="e_nmih_japan"
		holder=10040100 # Ise Sadamichi
	}
	1521.9.17={
		holder=10040104 # Ise Sadatada
	}
	1535.12.8={
		holder=10040105 # Ise Sadataka
	}
	1549.7.18={
		liege="d_nmih_yamashiro"
	}
	1562.10.8={
		liege="e_nmih_japan"
		holder=10040040 # Ashikaga Yoshiteru
	}
	1565.6.17={
		liege="k_nmih_southern_kinai"
		holder=10059014 # Miyoshi Yoshitsugu
	}
	1567.5.24={
		liege="d_nmih_yamashiro"
		holder=10059006 # Miyoshi Nagayasu
	}
	1568.10.19={
		liege="d_nmih_yamashiro"
		holder=10029120 # Oda Nobunaga
	}
	1568.11.7={
		liege="e_nmih_japan" # 40042 Ashikaga Yoshiaki
		holder=10040042 # Ashikaga Yoshiaki
	}
	1570.11.28={
		holder=10040113 # Ise Sadaoki
	}
	1573.8.25={
		liege="d_nmih_yamashiro"
		holder=10029200 # Murai Sadakatsu
	}
	1582.6.21={
		liege="d_nmih_yamashiro"
		holder=10040113 # Ise Sadaoki
	}
	1582.7.2={
		liege="d_nmih_yamashiro"
		holder=10029400 # Hashiba Hideyoshi
	}
	1582.8.1={
		holder=10029439 # Asano Nagayoshi
	}

}
