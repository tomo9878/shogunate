d_nmih_tajima = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	0757.7.6={
		holder=0 #  
	}
	770.9.22={
		holder=11000018 # Fujiwara Uona
	}
	771.4.20={
		holder=11005008 # Fujiwara Tsugutada
	}
	772.5.27={
		holder=0 #  
	}
	774.2.16={
		holder=11005111 # Fujiwara Yoshio
	}
	778.3.8={
		holder=0 #  
	}
	796.3.7={
		holder=11000024 # Fujiwara Uchimaro
	}
	800.1.1={
		holder=0 #  
	}
	834.5.6={
		holder=11000030 # Fujiwara Nagaoka
	}
	836.3.1={
		holder=0 #  
	}
	841.5.3={
		holder=11005174 # Fujiwara Morouji
	}
	845.10.3={
		holder=0 #  
	}
	1108.3.15={
		holder=10110023 # Taira Masamori
	}
	1110.12.2={
		holder=0 #  
	}
	1179.12.18={
		liege = "k_nmih_sanin"
		holder=10110031 # Taira Tsunemori
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1372.12.1={
		holder=10050007 # Yamana Moroyoshi
	}
	1376.1.1={
		holder=10050011 # Yamana Tokiyoshi
	}
	1389.5.29={
		holder=10050036 # Yamana Tokihiro
	}
	1390.3.1={
		holder=10050010 # Yamana Ujikiyo
	}
	1392.1.24={
		holder=10050036 # Yamana Tokihiro
	}
	1433.1.1={
		holder=10050039 # Yamana Mochitoyo
	}
	1454.1.1={
		holder=10050040 # Yamana Noritoyo
	}
	1458.1.1={
		holder=10050039 # Yamana Mochitoyo
	}
	1472.8.1={
		holder=10050044 # Yamana Masatoyo
	}
	1499.3.13={
		holder=10050045 # Yamana Munetoyo
	}
	1512.1.1={
		holder=10050046 # Yamana Nobutoyo
	}
	1528.3.4={
		holder = 10050005 # Yamana Suketoyo
	}
	1569.8.1={
		holder=0
	}

}
