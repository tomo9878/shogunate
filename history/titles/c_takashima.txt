c_nmih_takashima = {
	1.1.1 = { change_development_level = 40 }
	1100.1.1 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_omi"
	}
	1180.1.1={
		liege="e_nmih_japan"
		holder=10120213 # Minamoto Shigesada
	}
	1183.8.17={
		liege="d_nmih_omi" # Yamamoto Yoshitsuna
		holder=12515013 # Yamamoto [Nishigori] Yoshitaka / Taira's escape to the west
	}
	1184.3.4={
		liege="k_nmih_western_kanto"
		holder=12510030 # Sasaki Hideyoshi / Battle of Awazu
	}
	1184.8.26={
		liege="d_nmih_omi"
		holder=12510060 # Sasaki Sadatsuna / the three-day rebellion of the Taira clan
	}
	1205.4.29={
		holder=12510070 # Sasaki Hirotsuna
	}
	1221.7.22={
		holder=12510073 # Sasaki Nobutsuna / the Jokyu War
	}
	1242.4.7={
		holder=12510082 # Sasaki [Takashima] Takanobu
	}
	1270.1.1={
		holder=12510300 # Takashima Yasunobu
	}
	1290.1.1={
		holder=12510310 # Kutsuki Yoshitsuna
	}
	1310.1.1={
		holder=12510315 # Kutsuki Tokitsune
	}
	1330.1.1={
		holder=12510320 # Kutsuki Yoshiuji
	}
	1340.1.1={
		liege="e_nmih_japan"
		holder=12510325 # Kutsuki Tsuneuji
	}
	1363.1.1={
		holder=12510330 # Kutsuki Ujihide
	}
	1408.1.1={
		holder=12510335 # Kutsuki Yoshitsuna
	}
	1420.1.1={
		holder=12510340 # Kutsuki Tokitsuna
	}
	1445.1.1={
		holder=12510345 # Kutsuki Sadataka
	}
	1475.1.1={
		holder=10045503 # Kutsuki Sadatsuna
	}
	1485.8.27={
		holder=10045501 # Kutsuki Sadakiyo
	}
	1508.7.28={
		liege="k_nmih_southern_kinai"
		holder=10040012 # Ashikaga Yoshizumi, exiled from Otagi
	}
	1511.9.6={
		liege="e_nmih_japan"
		holder=10045501 # Kutsuki Sadakiyo
	}
	1523.1.1={
		holder=10045500 # Kutsuki Tanetsuna
	}
	1527.3.14={
		holder=10040000 # Ashikaga Yoshiharu, exiled from Otagi
	}
	1534.1.1={
		holder=10045500 # Kutsuki Tanetsuna
	}
	1549.1.1={
		holder=10045502 # Kutsuki Harutsuna
	}
	1549.7.18={
		holder=10040040 # Ashikaga Yoshiteru, exiled from Otagi
	}
	1552.1.1={
		holder=10045504 # Kutsuki Mototsuna
	}
	1553.9.8={
		holder=10040040 # Ashikaga Yoshiteru, exiled from Otagi, again.
	}
	1558.11.1={
		holder=10045504 # Kutsuki Mototsuna
	}
	1565.6.17={
		liege=0
	}
	1571.2.24={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10043131 # Isono Kazumasa
	}
	1575.1.1={
		liege="d_nmih_omi" # 29120 Oda Nobunaga
	}
	1578.2.3={
		holder=10029186 # Oda Nobuzumi
	}
	1582.6.21={
		liege=0
		holder=10045504 # Kutsuki Mototsuna
	}
	1582.7.2={
		liege="k_nmih_northern_kinai"
	}
}
