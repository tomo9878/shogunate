c_nmih_kuraki = {
	1.1.1 = { change_development_level = 10 }
	1275.1.1 = { change_development_level = 20 }
	1300.1.1 = { change_development_level = 30 }
	1438.1.1 = { change_development_level = 30 }
	1591.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_musashi"
	}
	1160.1.1={
		liege="d_nmih_musashi"
		holder=12200052 # Oyamada Arishige
	}
	1180.1.1={
		liege="d_nmih_musashi" # Kawagoe Shigeyori
		holder=12200090 # Inage Shigenari
	}
	1180.10.24={
		liege="k_nmih_western_kanto" # served under Yoritomo
	}
	1192.7.12={
		liege="e_nmih_japan"
	}
	1205.7.11={
		liege=0
		holder=0
	}
	1457.1.1={
		liege="d_nmih_musashi"
		holder=10017037 # Uesugi Mochitomo
	}
	1467.10.4={
		holder=10017042 # Uesugi Masazane
	}
	1473.12.22={
		holder=10017040 # Uesugi Sadamasa
	}
	1477.2.10={
		liege=0 # Nagao Kageharu's rebellion
		holder=10017324 # Nagao Kageharu
	}
	1478.5.21={
		liege="d_nmih_musashi" # Fall of Kozukue castle
		holder=10017040 # Uesugi Sadamasa
	}
	1494.11.20={
		holder=10017043 # Uesugi Tomoyoshi
	}
	1518.5.30={
		holder=10018000 # Uesugi Tomosada
	}
	1523.9.8={
		liege="d_nmih_sagami"
		holder=10016200 # Kasahara Nobutame
	}
	1557.8.12={
		holder=10016202 # Kasahara Yasukatsu
	}
	1575.1.1={
		holder=10016204 # Kasahara Masaharu
	}
	1581.10.1={
		liege="d_nmih_sagami"
		holder=10016019 # Hojo Ujimitsu
	}
}
