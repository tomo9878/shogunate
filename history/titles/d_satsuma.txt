d_nmih_satsuma = {
	0010.1.1={
	}
	20.1.1={
		holder=10290300 # Sori
	}
	70.1.1={
		holder=10290301 # Kibe
	}
	100.1.1={
		holder=10290302 # Toki
	}
	150.1.1={
		holder=10290307 # Ishikori
	}
	0701.1.1={
		liege="e_nmih_tenno"
	}
	764.11.6={
		holder=10041023 # Empress Koken/Shotoku
	}
	1180.1.1={
		liege = 0 # De facto
		holder=12810512 # Taira Nobuzumi
	}
	1183.8.17={
		liege= "k_nmih_western_chugoku"
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1197.1.1={
		holder=10060035 # Shimazu Tadahisa
	}
	1227.1.1={
		holder=10060034 # Shimazu Tadatoki
	}
	1265.1.1={
		holder=10060033 # Shimazu Hisatsune
	}
	1281.1.1={
		holder=10060032 # Shimazu Tadamune
	}
	1318.1.1={
		holder=10060031 # Shimazu Sadahisa
	}
	1363.8.12={
		holder=10060029 # Shimazu Ujihisa
	}
	1375.1.1={
		holder=12395221 # Imagawa Sadayo
	}
	1411.8.25={
		holder=10060004 # Shimazu Hisatoyo
	}
	1425.2.18={
		holder=10060005 # Shimazu Tadakuni
	}
	1470.2.21={
		holder=10060008 # Shimazu Tatsuhisa
	}
	1474.4.26={
		holder=10060011 # Shimazu Tadamasa
	}
	1508.3.16={
		holder=10060012 # Shimazu Tadaharu
	}
	1515.10.2={
		holder=10060013 # Shimazu Tadataka
	}
	1519.5.12={
		holder=10060002 # Shimazu Katsuhisa
	}
	1527.1.1={
		holder=10060000 # Shimazu Takahisa
	}
	1571.7.15={
		holder=10060024 # Shimazu Yoshihisa
	}
}
