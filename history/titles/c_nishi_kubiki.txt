c_nmih_nishi_kubiki = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }
	1480.1.1 = { change_development_level = 20 }

	0697.1.1={
		liege="d_nmih_echigo"
	}
	1180.1.1={
		liege="d_nmih_echigo"
		holder=12271200 # Hanagasaki Morishige
	}
	1181.7.25={
		liege="k_nmih_shinano" # Battle of Yokota-gawara
	}
	1183.6.22={
		liege="k_nmih_hokuriku" # Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan" # Taira's escape to the west
	}
	1184.3.4={
		liege=0 # Battle of Awazu
	}
	1200.1.1={
		liege=0
		holder=0
	}
	1300.1.1={
		liege="d_nmih_echigo"
	}
	1360.1.1={
		holder=12215326 # Nagao Kagetsune
	}
	1367.1.1={
		holder=10032003 # Nagao Takakage
	}
	1389.3.26={
		holder=10032004 # Nagao Kunikage
	}
	1450.12.16={
		holder=10032006 # Nagao Yorikage
	}
	1469.10.6={
		holder=10032007 # Nagao Shigekage
	}
	1482.3.14={
		holder=10032008 # Nagao Yoshikage
	}
	1506.10.5={
		holder=10032000 # Nagao Tamekage
	}
	1543.1.29={
		holder=10032009 # Nagao Harukage
	}
	1548.12.30={
		holder=10032011 # Uesugi Kenshin
	}
	1565.1.1={
		holder=10021500 # Murakami Yoshikiyo
	}
	1573.2.3={
		holder=10032011 # Uesugi Kenshin
	}
	1578.4.19={
		liege="d_nmih_kozuke" # 16020 Uesugi Kagetora
		holder=10016020 # Uesugi Kagetora
	}
	1579.4.19={
		liege="d_nmih_echigo" # 32032 Uesugi Kagekatsu
		holder=10032032 # Uesugi Kagekatsu
	}

}
