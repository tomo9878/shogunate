c_nmih_soo = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1200.1.1 = { change_development_level = 10 }

	100.1.1={
	}
	220.1.1={
		holder=10290320 # Takekaya
		liege=d_nmih_higo
	}
	270.1.1={
		holder=10290321 # Atsukaya
		liege=0
	}
	295.1.1={
		holder=10290325 # Torishikaya
	}
	1180.1.1={
		liege = 0 # De facto
		holder=12810905 # Kuwahata Kiyomichi
	}
	1185.4.25={
		liege=0 # Battle of Dan-no-ura
	}
	1210.1.1={
		liege=0
		holder=0
	}
	1300.1.1={
		liege="d_nmih_osumi"
	}
	1380.1.1={
		holder=10060110 # Kimotsuki Kaneuji
	}
	1400.1.1={
		holder=10060109 # Kimotsuki Kanemoto
	}
	1450.1.1={
		holder=10060108 # Kimotsuki Kanetada
	}
	1484.4.6={
		holder=10060107 # Kimotsuki Kanetsura
	}
	1490.1.1={
		holder=10060101 # Kimotsuki Kanehisa
	}
	1506.1.1={
		liege=0
	}
	1523.1.1={
		holder=10060102 # Kimotsuki Kaneoki
	}
	1533.1.1={
		holder=10060100 # Kimotsuki Kanetsugu
	}
	1566.12.26={
		holder=10060104 # Kimotsuki Yoshikane
	}
	1571.8.20={
		holder=10060105 # Kimotsuki Kaneaki
	}
	1574.2.1={
		liege="d_nmih_osumi"
	}
	1575.1.1={
		holder=10060106 # Kimotsuki Kanemori
	}
}
