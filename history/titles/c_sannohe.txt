c_nmih_sannohe = {
	1.1.1 = { change_development_level = 0 }
	1189.10.3 = { change_development_level = 10 }
	1500.1.1 = { change_development_level = 20 }

	100.1.1={
	}
	1160.1.1={
		liege="d_nmih_mutsu"
		holder=10150108 # Akarogu
	}
	1189.10.3={
		liege=0 # 
		holder=0 # Fall of Hiraizumi
	}
	1334.1.5={
		liege="d_nmih_mutsu"
		holder=12040162 # Nanbu Nobunaga
	}
	1350.1.1={
		holder=12040180 # Nanbu Masayuki
	}
	1388.11.17={
		holder=12040190 # Nanbu Moriyuki
	}
	1437.5.13={
		holder=12040200 # Nanbu Yoshimasa
	}
	1440.8.9={
		holder=12040201 # Nanbu Masamori
	}
	1445.1.1={
		holder=10003130 # Nanbu Mitsumasa
	}
	1449.1.1={
		holder=10003131 # Nanbu Tokimasa
	}
	1473.4.2={
		holder=10003132 # Nanbu Michitsugu
	}
	1490.1.1={
		holder=10003133 # Nanbu Nobutoki
	}
	1502.1.11={
		holder=10003134 # Nanbu Nobuyoshi
	}
	1503.6.18={
		holder=10003135 # Nanbu Masayasu
	}
	1507.3.23={
		holder=10003101 # Nanbu Yasunobu
	}
	1541.1.1={
		holder=10003102 # Nanbu Harumasa
	}
	1582.1.27={
		holder=10003154 # Nanbu Nobunao
	}
}
