c_nmih_nihonmatsu = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }

	100.1.1={
	}
	1180.1.1={
		liege="k_nmih_northern_oshu"
		holder=12060060 # Sato Motoharu
	}
	1189.9.21={
		liege=0
		holder=0 # Battle of Mountain Atsukashi
	}
	1351.3.18={
		holder=10007308 # Nihonmatsu Kuniaki
	}
	1435.1.1={
		liege=0
		holder=10007306 # Nihonmatsu Mitsuyasu
	}
	1455.1.1={
		holder=10007305
	}
	1470.3.18={
		holder=10007304
	}
	1494.2.13={
		holder=10007301 # Nihonmatsu Murakuni
	}
	1542.3.23={
		holder=10007302 # Nihonmatsu Ieyasu
	}
	1546.5.22={
		holder=10007303 # Nihonmatsu Yoshiuji
	}
	1547.3.26={
		holder=10007314 # Nihonmatsu Yoshikuni
	}
	1580.9.9={
		holder=10007315 # Nihonmatsu Yoshitsugu
	}
	1585.11.29={
		holder=10007316 # Nihonmatsu Yoshitsuna
	}
	1586.7.16={
		holder=10001035 # Date Masamune
	}
}
