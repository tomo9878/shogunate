c_nmih_echi = {
	1.1.1 = { change_development_level = 40 }
	1100.1.1 = { change_development_level = 40 }
	1331.1.1 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_omi"
	}
	1180.1.1={
		holder=12515000 # Yamamoto Yoshitsune
	}
	1180.12.24={
		liege="e_nmih_japan"
		holder=10110025 # Taira Kiyomori / Battle of Omi
	}
	1181.3.20={
		holder=10110042 # Taira Munemori
	}
	1183.8.17={
		liege="d_nmih_omi" # Yamamoto Yoshitsune
		holder=12515000 # Yamamoto Yoshitsune / Taira's escape to the west
	}
	1184.3.4={
		liege="k_nmih_western_kanto"
		holder=12510030 # Sasaki Hideyoshi / Battle of Awazu
	}
	1184.8.26={
		liege="d_nmih_omi"
		holder=12510060 # Sasaki Sadatsuna / the three-day rebellion of the Taira clan
	}
	1205.4.29={
		holder=12510070 # Sasaki Hirotsuna
	}
	1221.7.22={
		holder=12510073 # Sasaki Nobutsuna / the Jokyu War
	}
	1242.4.7={
		holder=12510083 # Sasaki [Kyogoku] Ujinobu
	}
	1295.1.1={
		holder=12510090 # Sasaki [Kyogoku] Munetsuna
	}
	1297.9.27={
		holder=12510100 # Sasaki [Kyogoku] Sadamune
	}
	1305.5.31={
		holder=10044201 # Sasaki [Kyogoku] Muneuji
	}
	1329.8.11={
		holder=10044203 # Sasaki [Kyogoku] Takauji
	}
	1440.1.1={
		liege="d_nmih_southern_omi"
		holder=10044660 # Iba Mitsutaka
	}
	1460.1.1={
		holder=10044661 # Iba Sadataka
	}
	1467.1.1={
		liege="e_nmih_japan"
		holder=10044500 # Gamo Sadahide
	}
	1493.4.23={
		liege="d_nmih_southern_omi" # vassal of Rokkaku
	}
	1514.3.30={
		holder=10044503 # Gamo Hidenori
	}
	1523.4.23={
		holder=10044504 # Gamo Sadahide
	}
	1568.10.12={
		liege="k_nmih_chubu" # vassal of Oda
		holder=10044505 # Gamo Katahide
	}
	1575.1.1={
		liege="d_nmih_omi" # 29120 Oda Nobunaga
	}
	1582.6.21={
		liege="d_nmih_harima"
		holder=10044508 # Gamo Ujisato
	}
	1582.7.2={
		liege="k_nmih_northern_kinai"
	}

}
