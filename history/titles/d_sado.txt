d_nmih_sado = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	0984.1.1={
		holder=0
	}
	1147.8.29={
		holder=10110230 # Taira Morikane {Sado no Kami}
	}
	1179.12.18={
		liege="k_nmih_southern_kinai"
		holder=10110124 # Taira Nakamori
	}
	1181.7.26={
		liege=0
		holder=0 # Battle of Yokota-gawara
	}
	1458.1.1={
		holder=10034003 # Honma Shigenao
	}
	1474.1.1={
		liege=0
		holder=10034002 # Honma Yasushige
	}
	1498.1.1={
		holder=10034001 # Honma Yasunao
	}
	1527.1.1={
		holder=10034000 # Honma Yasutoki
	}
	1537.1.1={
		holder=10034004 # Honma Ariyasu
	}
	1552.1.1={
		holder=10034005 # Honma Yasutaka
	}
}
