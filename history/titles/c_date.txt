c_nmih_date = {
	1.1.1 = { change_development_level = 30 }
	1100.1.1 = { change_development_level = 30 }
	1189.9.21 = { change_development_level = 30 }
	1532.1.1 = { change_development_level = 30 }
	1548.9.1 = { change_development_level = 30 }

	100.1.1={
	}
	1180.1.1={
		liege="k_nmih_northern_oshu"
		holder=12030051 # Fujiwara Yasuhira
	}
	1189.9.21={
		liege="k_nmih_western_kanto" # Battle of Mountain Atsukashi
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		liege="k_nmih_western_kanto"
		holder=12121000 # Date Nensai
	}
	1199.10.23={
		holder=12121002 # Date Tameshige
	}
	1251.11.17={
		holder=12071005 # Date Masayori
	}
	1301.8.13={
		holder=12071010 # Date Munetsuna
	}
	1317.3.20={
		holder=12071015 # Date Motomune
	}
	1335.8.28={
		liege="d_nmih_oshu_shugo"
		holder=12071020 # Date Yukitomo
	}
	1337.2.9={
		holder=0 # under Oshu Shogun-fu
	}
	1347.1.1={
		liege="d_nmih_oshu_shugo"
		holder=12071020 # Date Yukitomo
	}
	1348.6.6={
		holder=12071025 # Date Munetoo
	}
	1385.6.28={
		holder=12071030 # Date Masamune
	}
	1405.10.7={
		holder=12071033 # Date Ujimune
	}
	1412.8.24={
		holder=12071035 # Date Mochimune
	}
	1469.2.19={
		holder=12071040 # Date Narimune
	}
	1487.10.11={
		holder=12071045 # Date Hisamune
	}
	1514.5.28={
		holder=10001001 # Date Tanemune
	}
	1542.6.1={
		liege="d_nmih_iwaki" # Tenbun War
		holder=10001001 # Date Tanemune
	}
	1548.9.1={
		liege="d_nmih_oshu_shugo"
		holder=10001002 # Date Harumune
	}
	1564.1.1={
		holder=10001021 # Date Terumune
	}
	1584.10.6={
		holder=10001035 # Date Masamune
	}

}
