c_nmih_mie = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }
	1369.1.1 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_ise"
	}
	1180.1.1={
		liege="d_nmih_ise"
		holder=12433000 # Tomida Iesuke
	}
	1183.8.17={
		liege="d_nmih_shima" # Taira's escape to the west
	}
	1184.9.2={
		liege=0
		holder=0 # Failure of Taira's Three-day rebellion
	}
	1369.1.1={
		liege="d_nmih_shima"
		holder=10125170 # Chigusa Akitsune
	}
	1377.10.6={
		holder=10026400 # Chigusa Masamitsu
	}
	1420.1.1={
		holder=10026401 # Chigusa Mitsukiyo
	}
	1450.1.1={
		holder=10026402 # Chigusa Haruyasu
	}
	1478.1.1={
		liege=0
	}
	1480.1.1={
		liege=0
		holder=10026403 # Chigusa Michiharu
	}
	1505.1.1={
		holder=10026404 # Chigusa Takamichi
	}
	1530.1.1={
		holder=10026405 # Chigusa Harukiyo
	}
	1545.1.1={
		holder=10026406 # Chigusa Tadaharu
	}
	1555.1.1={
		liege="d_nmih_southern_omi"
	}
	1568.1.1={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1574.11.1={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029330 # Takigawa Kazumasu
	}
	1575.12.9={
		liege="e_nmih_japan" # 29120 Oda Nobunaga
	}
	1582.04.15={
		liege="d_nmih_kozuke" # 29330 Takigawa Kazumasu
	}
	1582.7.8={
		liege="d_nmih_ise"
	}
}
