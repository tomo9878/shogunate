k_nmih_oshu_tandai = {
	1000.1.1={
	}
	1335.12.22={
		holder=10029006 # Shiba Ienaga
	}
	1345.1.1={
		holder=12395015 # Kira Sadaie
	}
	1354.1.1={
		liege=0
		holder=12139050 # Osaki Iekane
	}
	1356.7.11={
		holder=10006217 # Osaki Tadamochi
	}
	1383.1.1={
		holder=10006213 # Osaki Akimochi
	}
	1400.1.1={
		holder=10006212 # Osaki Mitsuakira
	}
	1404.1.1={
		holder=10006210 # Osaki Mitsumochi
	}
	1424.1.1={
		holder=10006208 # Osaki Mochiakira
	}
	1441.1.1={
		holder=10006207 # Osaki Norikane
	}
	1484.1.1={
		holder=10006206 # Osaki Masakane
	}
	1487.9.22={
		holder=10006203 # Osaki Yoshikane
	}
	1529.1.1={
		holder=10006204 # Osaki Takakane
	}
	1530.1.1={
		holder=10006201 # Osaki Yoshinao
	}
	1555.1.1={ # Date became Oshu Tandai.
		liege=0
		holder=10001002 # Date Harumune 
	}
	1564.1.1={
		holder=10001021 # Date Terumune
	}
	1584.10.6={
		holder=10001035 # Date Masamune
	}

}
