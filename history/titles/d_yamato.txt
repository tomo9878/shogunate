d_nmih_yamato = {
	0697.1.1={
		liege="e_nmih_tenno"
	}
	1006.8.5={
		holder=10120012 # Minamoto Yorichika {Yamato no Kami}
	}
	1009.4.7={
		holder=0
	}
	1031.2.26={
		holder=10120012 # Minamoto Yorichika {Yamato no Kami}
	}
	1036.11.10={
		holder=0
	}
	1047.6.22={
		holder=10120012 # Minamoto Yorichika {Yamato no Kami}
	}
	1050.2.25={
		holder=0
	}
	1159.1.27={
		holder=10110032 # Taira Norimori {Yamato no Kami}
	}
	1160.2.13={
		holder=0
	}
	1179.5.20={
		liege="k_nmih_yamashiro"
		holder=12570101 # Genen, Kofuku-ji betto (the head priest of Kofuku-ji Temple)
	}
	1180.6.20={
		liege=0
	}
	1181.1.12={
		liege="k_nmih_southern_kinai"
		holder=11000162 # Shinen, Siege_of_Nara
	}
	1183.8.17={
		liege=0
	}
	1189.7.13={
		holder=0
	}
	1265.6.29={
		liege=0
		holder=11004139 # Raien, Kofuku-ji betto (the head priest of Kofuku-ji Temple)
	}
	1426.1.1={
		holder=11000774 # [165] Kujo Kyogaku
	}
	1431.1.1={
		holder=11000774 # [168] Kujo Kyogaku
	}
	1456.1.1={
		holder=11000788 # [180] Ichijo Jinson
	}
	1461.1.1={
		holder=11000774 # [182] Kujo Kyogaku
	}
	1469.1.1={
		holder=11000774 # [186] Kujo Kyogaku
	}
	1473.9.19={
		holder=11004301 # [187] Kojun
	}
	1486.1.1={
		holder=11003189 # [189] Sonyo
	}
	1490.1.1={
		holder=11000803 # [190] Seikaku
	}
	1494.4.21={
		holder=10048003 # [191] Ryuken 
	}
	1497.1.30={
		holder=10048002 # [192] Kukaku
	}
	1510.12.25={
		holder=11004429 # [193] Kokei 
	}
	1515.1.1={
		holder=11000293 # [194] Ryoyo
	}
	1520.1.1={
		holder=10048001 # [195] Kenkei
	}
	1525.1.1={
		holder=11000826 # [196] Kyojin
	}
	1526.9.4={
		holder=11004316 # [197] Enshin
	}
	1534.1.1={
		liege="k_nmih_kanrei"
		holder=10040831 # Kizawa Nagamasa, De facto
	}
	1542.4.2={
		holder=10048403 # Tsutsui Junsho, De facto
	}
	1550.8.2={
		holder=10048413 # Tsutsui Junkei, De facto
	}
	1560.1.1={ # Matsunaga Hisahide's invation of Yamato
		liege="k_nmih_southern_kinai"
		holder=10045400 # Matsunaga Hisahide
	}
	1565.12.8={
		liege=0
	}
	1567.5.24={
		liege="k_nmih_southern_kinai"
	}
	1573.12.10={ # Matsunaga surrendered to Oda Nobunaga
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029120 # Oda Nobunaga
	}
	1575.5.15={
		holder=10029390 # Ban Naomasa
	}
	1575.12.9={
		liege="e_nmih_japan" # 29120 Oda Nobunaga
	}
	1576.5.30={
		holder=10029120 # Oda Nobunaga
	}
	1576.6.6={
		holder=10048413 # Tsutsui Junkei
	}
	1582.6.21={
		liege=0
	}
	1582.7.2={
		liege="k_nmih_northern_kinai"
	}
}
