c_nmih_shiga = {
	1.1.1 = { change_development_level = 30 }
	1571.9.30 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_omi"
	}
	0788.1.1={
		liege="d_nmih_enryakuji"
	}
	1015.1.1={
		holder=11003600 # Keimyo
	}
	1038.1.1={
		holder=0
	}
	1105.4.7={
		holder=11006265 # [39] Zoyo
	}
	1110.6.1={
		holder=11006565 # [42] Ningo
	}
	1121.11.24={
		holder=11006456 # [43] Kankei
	}
	1138.12.10={
		holder=11000149 # [48] Gyogen
	}
	1162.3.25={
		holder=11000152 # [50] Kakuchu
	}
	1179.12.16={
		holder=12570100 # Myoun
	}
	1184.1.3={
		liege=0
		holder=0 # Siege of Hojujidono
	}
	1193.1.11={
		holder=11000165 # Jien
	}
	1206.1.30={
		holder=11000462 # Ryoken
	}
	1212.2.27={
		holder=11000165 # Jien
	}
	1214.1.8={
		holder=11000165 # Jien
	}
	1221.9.21={
		holder=11000209 # Enki
	}
	1277.1.7={
		holder=11000636 # Dogen
	}
	1278.5.1={
		holder=11001358 # Kingo
	}
	1288.5.6={
		holder=11000637 # Kujo
	}
	1293.6.21={
		holder=11000403 # Jiki
	}
	1297.1.22={
		holder=11001246 # Sonkyo
	}
	1302.6.21={
		holder=11000666 # Dojun
	}
	1303.5.17={
		holder=11000636 # Dogen
	}
	1377.5.29={
		holder=11000734 # Jizai
	}
	1393.2.19={
		holder=11000280 # Jiben
	}
	1402.4.8={
		holder=11000754 # Dogo
	}
	1404.12.5={
		holder=11000756 # Kankyo
	}
	1409.4.11={
		holder=11000757 # Ryojun
	}
	1412.6.5={
		holder=11000756 # Kankyo
	}
	1413.12.18={
		holder=11001461 # Jitsuen
	}
	1421.5.12={
		holder=10040018 # [156] Ashikaga Jiben
	}
	1428.6.4={
		holder=10040023 # [157] Ashikaga Gisho
	}
	1431.7.1={
		holder=11000770 # [158] Ichijo Ryoju
	}
	1435.5.21={
		holder=10040023 # [159] Ashikaga Gisho
	}
	1447.2.21={
		holder=11001467 # [160] Kinsho
	}
	1471.5.9={
		holder=11000790 # [162] Sonou
	}
	1493.5.15={
		holder=10041756 # Gyoin, 163th
	}
	1515.4.13={
		holder=10041759 # Kakuin, 164th
	}
	1530.4.23={
		holder=10041701 # Sonchin, 165th
	}
	1550.1.1={
		holder=10041748 # Gyoson, 166th
	}
	1559.9.5={
		holder=10041743 # Ouin, 167th
	}
	1571.9.30={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10025632 # Akechi Mitsuhide
	}
	1575.12.9={
		liege="e_nmih_japan"
	}
	1582.6.21={
		liege="k_nmih_northern_kinai"
	}
	1582.7.2={
		liege=0
		holder=10025639 # Akechi Hidemitsu
	}
	1582.7.4={
		liege="k_nmih_northern_kinai"
		holder=10029400 # Hashiba Hideyoshi
	}
	1582.7.16={
		liege="d_nmih_wakasa"
		holder=10029310 # Niwa Nagahide
	}
}
