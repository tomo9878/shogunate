c_nmih_takakusa = {
	1.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	100.1.1={
	}
	1176.1.1={
		liege="k_nmih_sanin"
		holder=12600201 # Nagata Sanetsune
	}
	1183.8.17={
		liege="k_nmih_western_chugoku" # Taira's escape to the west
	}
	1200.1.1={
		liege=0
		holder=0 # 
	}
	1400.1.1={
		liege="d_nmih_inaba"
	}
	1475.1.1={
		holder = 10050032 # Yamana Toyotoki
	}
	1488.1.1={
		holder = 10050056 # Yamana Masazane
	}
	1489.12.18={
		holder = 10050032 # Yamana Toyotoki
	}
	1504.1.1={
		holder = 10050057 # Yamana Toyoshige
	}	
	1512.12.1={
		holder = 10050033 # Yamana Toyoyori
	}
	1521.1.1={
		holder = 10050034 # Yamana Toyoharu
	}
	1527.1.1={
		holder = 10050035 # Yamana Nobumichi
	}
	1546.1.1={
		holder = 10050048 # Yamana Toyosada
	}
	1560.3.29={
		holder = 10050070 # Yamana Munetoyo
	}
	1561.5.1={
		holder = 10050051 # Yamana Toyokazu
	}
	1564.1.1={
		holder = 10050060 # Yamana Toyokuni
	}
	1573.11.1={
		liege="k_nmih_western_chugoku" # Mori Terumoto
	}
	1580.9.1={
		liege="e_nmih_japan"
		holder = 10029400 # Hashiba Hideyoshi
	}

}
