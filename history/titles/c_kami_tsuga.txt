c_nmih_kami_tsuga = {
	1.1.1 = { change_development_level = 10 }
	1000.1.1 = { change_development_level = 10 }
	1462.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_shimotsuke"
	}
	1180.1.1={
		liege=0
		holder=12136004 # Seki Ryusen, Rinnoji
	}
	1180.10.26={
		liege="k_nmih_western_kanto"
	}
	1192.7.12={
		liege="e_nmih_japan"
	}
	1205.1.1={
		holder=12136006 # Seki Bengaku, Rinnoji
	}
	1251.1.1={
		holder=0 # 
	}
	1462.1.1={
		liege="d_nmih_oyama"
		holder=10015001 # Mibu Tanenari
	}
	1471.1.1={
		liege="d_nmih_shimotsuke"
	}
	1494.11.1={
		holder=10015002 # Mibu Tsunashige
	}
	1516.10.12={
		holder=10015003 # Mibu Tsunafusa
	}
	1549.10.7={
		liege=0
	}
	1555.4.8={
		holder=10015006 # Mibu Tsunatake
	}
	1576.3.25={
		holder=10015007 # Mibu Yoshitake
	}
}
