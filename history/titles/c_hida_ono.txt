c_nmih_hida_ono = {
	1.1.1 = { change_development_level = 10 }
	1462.1.1 = { change_development_level = 20 }
	1575.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_hida"
	}
	1180.1.1={
		liege="d_nmih_hida"
		holder=10131579 # Ito Kagekiyo
	}
	1183.6.22={
		liege="k_nmih_hokuriku"
		holder=10120084 # Minamoto Yoshinaka / Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan"
		holder=10120084 # Minamoto Yoshinaka / Taira's escape to the west
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1334.1.1={
		holder=11003176 # Anekoji Takamoto
	}
	1358.4.10={
		holder=11003178 # Anekoji Ietsuna
	}
	1389.1.1={
		holder=11003179 # Anekoji Yoritoki
	}
	1411.9.20={
		holder=11003181 # Anekoji Morotoki
	}
	1440.1.1={
		holder=11003185 # Anekoji Mochitoki
	}
	1460.1.1={
		liege=0
		holder=11003187 # Anekoji Katsutoki
	}
	1481.1.1={
		holder=11003202 # Anekoji Mototsuna
	}
	1504.6.5={
		holder=11003205 # Anekoji Naritsugu
	}
	1518.7.7={
		holder=11003208 # Anekoji Naritoshi
	}
	1527.10.26={
		liege=0
		holder=10025101 # Miki Naoyori
	}
	1554.7.13={
		holder=10025103 # Miki Yoshiyori
	}
	1570.2.1={
		liege="k_nmih_chubu"
		holder=10025109 # Anegakoji(Miki) Yoritsuna
	}
	1582.6.21={ # Honno-ji Incident
		liege=0
	}
	1582.11.22={
		liege="d_nmih_hida"
	}

}
