d_nmih_bizen = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	1180.1.1={
		liege = "k_nmih_sanyo"
		holder=12630401 # Naniwa Tsuneto
	}
	1181.3.20={
		liege = "e_nmih_japan"
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1467.7.1={
		holder=10053518 # Akamatsu Masanori
	}
	1496.6.6={
		holder=10053519 # Akamatsu Yoshimura
	}
	1520.1.1={
		holder=10054403 # Uragami Muramune
	}
	1531.7.17={
		holder=10054404 # Uragami Masamune
	}
	1554.1.1={
		holder=10054407 # Uragami Munekage
	}
	1575.10.1={
		holder=10055100 # Ukita Naoie
	}
	1579.10.1={
		liege="e_nmih_japan"
	}
	1582.2.1={
		holder=10055101 # Ukita Hideie
	}
	1582.6.21={ # Honno-ji Incident
		liege="k_nmih_sanyo"
	}
}
