c_nmih_yuki = {
	1.1.1 = { change_development_level = 10 }
	1250.1.1 = { change_development_level = 20 }
	1460.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_shimousa"
	}
	1180.1.1={
		liege="d_nmih_oyama"
		holder=10010227 # Yuki Tomomitsu
	}
	1180.10.22={
		liege="k_nmih_western_kanto" # served under Yoritomo
	}
	1192.7.12={
		liege="e_nmih_japan"
	}
	1254.2.24={
		holder=10010225 # Yuki Tomohiro
	}
	1274.1.1={
		holder=10010224 # Yuki Hirotsuna
	}
	1278.7.14={
		holder=10010223 # Yuki Tokihiro
	}
	1290.8.7={
		holder=10010222 # Yuki Sadahiro
	}
	1309.11.13={
		holder=10010220 # Yuki Tomosuke
	}
	1336.5.30={
		holder=10010221 # Yuki Naotomo
	}
	1343.4.26={
		liege="e_nmih_japan"
		holder=10010216 # Yuki Naomitsu
	}
	1395.2.7={
		liege="k_nmih_kamakura_kubo"
		holder=10010213 # Yuki Motomitsu
	}
	1410.1.1={
		holder=10010212 # Yuki Mitsuhiro
	}
	1416.1.1={
		holder=10010208 # Yuki Ujitomo
	}
	1440.3.1={
		liege=0 # Battle of Yuki
	}
	1441.5.6={
		liege="k_nmih_kamakura_kubo"
		holder=10010211 # Yuki Shigetomo
	}
	1463.1.18={
		holder=10010205 # Yuki Ujihiro
	}
	1481.4.27={
		holder=10010203 # Yuki Masatomo
	}
	1527.1.1={
		holder=10010201 # Yuki Masakatsu
	}
	1534.1.1={
		liege="d_nmih_oyama"
	}
	1559.9.2={
		holder=10010228 # Yuki Akitomo
	}
	1559.9.3={
		holder=10011204 # Yuki Harutomo
	}
}
