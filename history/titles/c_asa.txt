c_nmih_asa = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_nagato"
	}
	1180.1.1={
		liege="d_nmih_nagato"
		holder=12661000 # Koto Takemitsu
	}
	1190.1.1={
		liege=0
		holder=0 # 
	}
	1397.1.1={
		liege="d_nmih_nagato" #　Ouchi Hiroshige
		holder=10057011 #　Ouchi Hiroshige
	}
	1402.2.1={
		holder=10057010 #　Ouchi Moriharu
	}
	1465.9.23={
		holder=10057007 #　Ouchi Masahiro
	}
	1470.3.6={
		holder=10057009 #　Ouchi Doton [Noriyuki]
	}
	1472.1.1={
		holder=10057007 #　Ouchi Masahiro
	}
	1495.10.6={
		holder=10057001 #　Ouchi Yoshioki
	}
	1529.1.29={
		holder=10057002 #　Ouchi Yoshitaka
	}
	1551.9.30={
		holder=10057200 # Naito Okimori
	}
	1555.1.7={
		holder=10057251 # Naito Takayo
	}
	1557.4.30={
		liege="d_nmih_aki"
		holder = 10057402 # Yoshimi Masayori
	}

}
