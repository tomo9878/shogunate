c_nmih_saba = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_suo"
	}
	1179.12.18={
		liege = "d_nmih_suo"
		holder=10110143 # Taira Noritsune
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1441.8.14={
		liege="d_nmih_suo"
		holder=10057008 # Ouchi Norihiro
	}
	1465.9.23={
		holder=10057007 # Ouchi Masahiro
	}
	1495.10.6={
		holder= 10057001 # Ouchi Yoshioki
	}
	1529.1.29={
		holder = 10057002 # Ouchi Yoshitaka
	}
	1551.9.30={
		holder = 10057102 # Sue Harukata
	}
	1555.10.16={
		holder = 10057116 # Sue Nagafusa
	}
	1557.4.1={
		holder=10056004 # Mori Motonari
	}
	1557.4.30={
		liege="d_nmih_aki"
		holder = 10057402 # Yoshimi Masayori
	}

}
