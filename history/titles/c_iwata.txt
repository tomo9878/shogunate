c_nmih_iwata = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }
	1493.1.1 = { change_development_level = 40 }

	0701.1.1={
		liege="d_nmih_totomi"
	}
	1180.1.1={
		liege="d_nmih_suruga" # Tachibana Tomochi
		holder=12390300 # Asaba Munenobu
	}
	1180.11.9={
		liege="d_nmih_totomi" # Battle of Fujikawa
	}
	1181.4.28={
		holder=10120113 # Yasuda Yoshisada
	}
	1185.4.25={
		liege=0
		holder=0
	}
	1336.1.1={
		holder=12395214 # Imagawa Norikuni
	}
	1339.10.1={
		holder=12396021 # Nikki Yoshinaga
	}
	1352.2.1={
		holder=12395214 # Imagawa Norikuni
	}
	1384.6.8={
		holder=12395221 # Imagawa Sadayo
	}
	1400.1.17={
		liege="d_nmih_suruga"
		holder=12395260 # Imagawa Sadaomi
	}
	1410.1.1={
		holder=12395270 # Imagawa Sadasuke
	}
	1430.1.1={
		holder=12395273 # Imagawa Norimasa
	}
	1459.10.1={
		holder=10029019 # Shiba Yoshitoshi
	}
	1460.1.1={
		holder=10029021 # Shiba Yoshihiro
	}
	1461.9.6={
		holder=10057621 # Shiba Yoshikado
	}
	1466.9.2={
		holder=10029019 # Shiba Yoshitoshi
	}
	1466.10.23={
		holder=10057621 # Shiba Yoshikado
	}
	1467.6.27={
		holder=10029019 # Shiba Yoshitoshi
	}
	1474.1.1={
		holder=12395276 # Imagawa Sadanobu
	}
	1475.1.1={
		holder=12395500 # Imagawa Sadamoto
	}
	1508.8.1={
		liege="d_nmih_totomi"
	}
	1536.4.7={
		liege=0
	}
	1537.1.1={
		liege="d_nmih_totomi"
		holder=10023010 # Imagawa Yoshimoto
	}
	1560.6.12={
		holder=10023017 # Imagawa Ujizane
	}
	1569.1.1={
		liege="d_nmih_mikawa"
		holder=10024006 # Tokugawa Ieyasu
	}
	1569.5.17={
		liege="d_nmih_totomi"
	}

}
