c_nmih_kiku = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }

	100.1.1={
	}
	180.1.1={
		holder=10290001 # Himiko
	}
	1180.1.1={
		liege="d_nmih_buzen"
		holder=12750500 # Itai Taneto
	}
	1185.3.4={
		liege="k_nmih_western_chugoku" # Battle of Ashiya-no-ura
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1400.1.1={
		liege=k_nmih_western_chugoku
	}
	1480.1.1={
		holder=10067103 # Nagano Yoshifusa
	}
	1510.1.1={
		holder=10067101 # Nagano Yoshiaki
	}
	1532.1.1={
		holder=10067100 # Nagano Fusamori
	}
	1559.1.1={
		holder=10067506 # Akisuki Tanefuyu
		liege=d_nmih_aki
	}
	1565.1.1={
		liege=d_nmih_bungo
	}
	1567.1.1={
		liege=d_nmih_aki # Mori Motonari
	}
	1568.8.1={
		liege=d_nmih_bungo # Otomo Yoshishige
	}
	1579.1.1={
		liege=d_nmih_hizen # Ryuzoji Takanobu
	}
}
