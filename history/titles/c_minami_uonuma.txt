c_nmih_minami_uonuma = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }

	0697.1.1={
		liege="d_nmih_echigo"
	}
	1180.1.1={
		liege=0
		holder=12270170 # Echigo Shigeie
	}
	1181.2.27={
		holder=12270171 # Echigo Iemitsu
	}
	1181.7.25={
		liege="k_nmih_shinano" # Battle of Yokota-gawara
	}
	1183.6.22={
		liege="k_nmih_hokuriku" # Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan" # Taira's escape to the west
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1300.1.1={
		liege="d_nmih_echigo"
	}
	1360.1.1={
		holder=10032020 # Nagao Nagakage
	}
	1378.1.1={
		holder=10032021 # Nagao Fusakage
	}
	1415.1.1={
		holder=10032022 # Nagao Norikage
	}
	1438.1.1={
		holder=10032023 # Nagao Norinaga
	}
	1487.1.1={
		holder=10032024 # Nagao Kagetaka
	}
	1506.10.5={
		liege=0
		holder=10032001 # Nagao Fusanaga
	}
	1510.7.25={
		liege="d_nmih_echigo"
	}
	1552.9.3={
		holder=10032025 # Nagao Masakage
	}
	1561.8.21={
		holder=10032011 # Uesugi Kenshin
	}
	1575.1.1={
		holder=10032032 # Uesugi Kagekatsu
	}

}
