c_nmih_hioki = {
	1.1.1 = { change_development_level = 20 }
	1000.1.1 = { change_development_level = 20 }
	1450.1.1 = { change_development_level = 20 }
	1527.1.1 = { change_development_level = 20 }

	0001.1.1={
	}
	0020.1.1={
		holder=10290300 # Sori
	}
	0070.1.1={
		holder=10290301 # Kibe
	}
	0100.1.1={
		holder=10290302 # Toki
	}
	0150.1.1={
		holder=10290307 # Ishikori
	}
	0170.1.1={
		holder=10290308 # Tomami
	}
	0230.1.1={
		holder=10290309 # Atasumi
	}
	1161.1.1={
		liege="d_nmih_satsuma"
		holder=12810512 # Taira Nobuzumi
	}
	1185.4.25={
		liege=0
		holder=0 # Battle of Dan-no-ura
	}
	1380.1.1={
		liege="d_nmih_satsuma"
		holder=10060809 # Ijuin Yorihisa
	}
	1400.1.1={
		holder=10060809 # Ijuin Yorihisa
	}
	1418.1.1={
		holder=10060810 # Ijuin Hirohisa
	}
	1450.1.1={
		holder=10060005 # Shimazu Tadakuni
	}
	1459.1.8={
		holder=10060010 # Shimazu Hisayasu
	}
	1470.1.1={
		holder=10060022 # Shimazu Yoshihisa
	}
	1497.9.11={
		holder=10060013 # Shimazu Hisayasu
	}
	1519.5.12={
		holder=10060800 # Ijuin Tadaaki
	}
	1570.1.1={
		holder=10060801 # Ijuin Tadaao
	}
}
