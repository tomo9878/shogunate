c_nmih_yamada = {
	1.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 50 }

	0701.1.1={
		liege="d_nmih_owari"
	}
	1180.1.1={
		liege=0
		holder=12430010 # Yamada Shigemitsu
	}
	1180.11.9={
		liege="d_nmih_mikawa" # Battle of Fujikawa
	}
	1181.4.25={
		liege=0 # Battle of Sunomata 	
		holder=12430021 # Yamada Shigetada
	}
	1183.8.17={
		liege="e_nmih_japan" # Taira's escape to the west
	}
	1184.1.3={
		liege="k_nmih_western_kanto" # Siege of Hojujidono
	}
	1221.7.6={
		liege=0
		holder=0 # Jokyu War
	}
	1400.6.1={
		liege="d_nmih_owari"
		holder=12320001 # Kai Yukinori
	}
	1402.1.1={
		holder=10029105 # Oda Jochiku
	}
	1436.1.1={
		holder=10029113 # Oda Hisanaga
	}
	1467.6.27={ # Onin War
		liege="d_nmih_totomi"
		holder=10029114 # Oda Toshisada
	}
	1478.1.1={
		liege="d_nmih_owari"
	}
	1495.7.1={
		holder=10029116 # Oda Tosada
	}
	1495.9.1={
		holder=10029119 # Oda Michisada
	}
	1513.1.1={
		holder=10029101 # Oda Michikatsu
	}
	1551.01.01={
		holder=10029122 # Oda Nobutomo
	}
	1555.05.10={
		holder=10029120 # Oda Nobunaga
	}
	1558.1.1={
		liege="d_nmih_owari"
		holder=10029280 # Shibata Katsuie
	}
	1570.7.1={
		holder=10029270 # Yanada Hiromasa
	}
	1575.10.6={
		holder=12430210 # Okada Shigeyoshi
	}
}
