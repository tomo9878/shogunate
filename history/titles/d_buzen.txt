d_nmih_buzen = {
	0100.1.1={
	}
	180.1.1={
		holder=10290001 # Himiko
	}
	0701.1.1={
		liege="e_nmih_tenno"
	}
	764.11.6={
		holder=10041023 # Empress Koken/Shotoku
	}
	1180.1.1={
		liege = 0 # De facto
		holder=10260310 # Usa Kimimichi
	}
	1185.3.4={
		liege=0
		holder=0 # Battle of Ashiya-no-ura
	}
	1200.1.1={
		holder=10130546 # Muto Sukeyori
	}
	1227.1.1={
		holder=10130550 # Muto Sukeyoshi
	}
	1279.1.1={
		holder=10131932 # Hojo Sukemasa
	}
	1310.1.1={
		holder=10132097 # Hojo Masaaki
	}
	1317.1.1={
		holder=10132241 # Hojo Akiyoshi
	}
	1323.1.1={
		holder=10132245 # Hojo Sadayoshi
	}
	1333.7.1={
		holder=10068020 # Shoni Sadatsune
	}
	1334.1.1={
		holder=10068018 # Shoni Yorihisa
	}
	1353.1.1={
		holder=0
	}
	1375.1.1={
		holder=12395221 # Imagawa Sadayo
	}
	1387.1.1={
		holder=10057430 # Yoshimi Naoyori
	}
	1408.1.1={
		holder=10068005 # Shoni Mitsusada
	}
	1431.8.6={
		holder=10057015 # Ouchi Mochiyo
	}
	1441.8.14={
		holder=10057008 # Ouchi Norihiro
	}
	1465.9.23={
		holder=10057007 # Ouchi Masahiro
	}
	1495.10.6={
		holder= 10057001 # Ouchi Yoshioki
	}
	1529.1.29={
		holder = 10057002 # Ouchi Yoshitaka
	}
	1551.9.30={
		holder=0
	}
	1559.6.1={
		holder=10064002 # Otomo Yoshishige(Sorin)
	}
}
