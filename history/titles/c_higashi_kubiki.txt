c_nmih_higashi_kubiki = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 20 }
	1410.1.1 = { change_development_level = 30 }

	0697.1.1={
		liege="d_nmih_echigo"
	}
	1180.1.1={
		liege="d_nmih_echigo"
		holder=12270130 # Jo Sukenaga
	}
	1181.3.11={
		holder=12270131 # Jo Nagamochi
	}
	1181.7.25={
		liege="k_nmih_shinano"
		holder=10120084 # Minamoto [Kiso] Yoshinaka / Battle of Yokota-gawara
	}
	1183.6.22={
		liege="k_nmih_hokuriku"
	}
	1183.8.17={
		liege="e_nmih_japan"
	}
	1184.3.4={
		liege=0
		holder=0 # Battle of Awazu
	}
	1300.1.1={
		liege="d_nmih_echigo"
	}
	1360.1.1={
		holder=12215326 # Nagao Kagetsune
	}
	1367.1.1={
		holder=10032003 # Nagao Takakage
	}
	1389.3.26={
		holder=10032004 # Nagao Kunikage
	}
	1450.12.16={
		holder=10032006 # Nagao Yorikage
	}
	1469.10.6={
		holder=10032007 # Nagao Shigekage
	}
	1482.3.14={
		holder=10032008 # Nagao Yoshikage
	}
	1506.10.5={
		holder=10032000 # Nagao Tamekage
	}
	1543.1.29={
		holder=10032009 # Nagao Harukage
	}
	1548.12.30={
		holder=10032011 # Uesugi Kenshin
	}
	1578.4.19={
		holder=10032032 # Uesugi Kagekatsu
	}

}
