c_nmih_kaisei = {
	1.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_owari"
	}
	1179.12.18={
		liege="d_nmih_owari" # Taira Kiyosada
		holder=10110140 # Taira Kiyosada
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1400.1.1={
		liege=0
		holder=12535030 # Hattori Sakyoshin
	}
	1430.1.1={
		holder=12535031 # Hattori Yasumune
	}
	1450.1.1={
		holder=12535032 # Hattori Munesumi
	}
	1470.1.1={
		holder=12535033 # Hattori Muneie
	}
	1490.1.1={
		holder=12535034 # Hattori Munenobu
	}
	1492.1.1={
		liege="k_nmih_honganji"
		holder=12535034 # Hattori Munenobu
	}
	1510.1.1={
		holder=12535035 # Hattori Munemasa
	}
	1530.1.1={
		holder=10028950 # Hattori Masaie
	}
	1550.1.1={
		holder=10028951 # Hattori Tomosada
	}
	1568.1.1={
		liege="d_nmih_owari"
		holder=10029138 # Oda Nobuoki
	}
	1570.12.28={
		liege="k_nmih_honganji"
		holder=10042251 # Gansho-ji Shoi
	}
	1571.6.28={
		holder=10042252 # Gansho-ji Kennin
	}
	1574.10.23={
		liege="d_nmih_owari"
		holder=10029120 # Oda Nobunaga
	}
	1575.12.30={
		liege="d_nmih_owari" # 29160 Oda Nobutada
		holder=10029160 # Oda Nobutada
	}
	1582.6.21={ # Honno-ji Incident
		liege="d_nmih_owari"
		holder=10029184 # Oda Hidenobu
	}
	1582.7.16={
		liege="d_nmih_owari"
		holder=10029161 # Oda Nobukatsu
	}

}
