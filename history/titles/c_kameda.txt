c_nmih_kameda = {
	1.1.1 = { change_development_level = 0 }
	100.1.1 = { change_development_level = 0 }
	900.1.1 = { change_development_level = 0 }
	1200.1.1 = { change_development_level = 20 }
	1435.1.1 = { change_development_level = 20 }
	1500.1.1 = { change_development_level = 30 }
	1512.5.1 = { change_development_level = 30 }
	1600.1.1 = { change_development_level = 40 }
	1700.1.1 = { change_development_level = 50 }
	1800.1.1 = { change_development_level = 60 }
	1864.6.15 = { change_development_level = 70 }
	1869.6.27 = { change_development_level = 70 }

	0100.1.1={
	}
	1170.1.1={
		liege="d_nmih_iburi"
		holder=10150103 # Kakinsu
	}
	1210.1.1={
		liege=0
		holder=0 # 
	}
	1457.1.1={
		liege="d_nmih_shimokuni"
		holder=10000119 # Ando Iemasa
	}
	1495.7.28={
		holder=10000121 # Ando Morosue
	}
	1512.5.1={
		liege="d_nmih_iburi"
		holder=10150502 #Shoya
	}
	1515.6.22={
		holder=10150504 #Cikomotayn
	}
	1560.1.1={
		holder=10150505 #Kuturayn
	}
	1575.3.15={
		holder=10150506 #Kaera
	}
}
