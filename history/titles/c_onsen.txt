c_nmih_onsen = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1480.1.1 = { change_development_level = 30 }

	100.1.1={
		liege="d_nmih_iyo"
	}
	1180.1.1={
		liege="d_nmih_iyo"
		holder=12691005 # Kono Michikiyo
	}
	1181.2.7={
		holder=12691010 # Kono Michinobu
	}
	1221.7.6={
		liege=0
		holder=0 # Jokyu War
	}
	1379.12.23={
		liege="d_nmih_iyo"
		holder=10058616 # Kono Michiyoshi
	}
	1394.12.9={
		holder=10058617 # Kono Michiyuki
	}
	1414.9.1={
		holder=10058615 # Kono Michihisa
	}
	1435.7.24={
		holder=10058604 # Kono Norimichi
	}
	1500.2.19={
		holder=10058601 # Kono Michinobu
	}
	1519.1.1={
		holder=10058600 # Kono Michinao
	}
	1553.1.1={
		holder=10058603 # Kono Michinobu
	}
	1581.1.1={
		holder=10058605 # Kono Michinao
	}
}
