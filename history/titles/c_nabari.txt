c_nmih_nabari = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 20 }
	1331.1.1 = { change_development_level = 30 }

	0701.1.1={
		liege="d_nmih_iga"
	}
	1167.5.29={
		liege="d_nmih_iga" # Taira Ietsugu
		holder=10110220 # Taira Ietsugu
	}
	1184.8.14={
		liege=0
		holder=0 # Failure of Taira's Three-day rebellion
	}
	1429.1.25={
		liege="d_nmih_shima"
		holder=10026006 # Kitabatake Noritomo
	}
	1471.4.13={
		holder=10026005 # Kitabatake Masasato
	}
	1480.1.1={
		liege=0
		holder=10028801 # Momochi Masanaga
	}
	1540.1.1={
		liege=0
		holder=10028800 # Momochi Tanba
	}
	1581.11.06={
		liege="d_nmih_iga" # Oda Nobukatsu
		holder=10029161 # Oda Nobukatsu
	}

}
