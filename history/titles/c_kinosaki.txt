c_nmih_kinosaki = {
	1.1.1 = { change_development_level = 30 }
	1331.1.1 = { change_development_level = 30 }

	100.1.1={
	}
	1179.12.18={
		liege="d_nmih_tajima"
		holder=10110031 # Taira Tsunemori
	}
	1183.8.17={
		liege=0
		holder=0 # Taira's escape to the west
	}
	1433.1.1={
		holder=10050039 # Yamana Mochitoyo
	}
	1454.1.1={
		holder=10050040 # Yamana Noritoyo
	}
	1458.1.1={
		holder=10050039 # Yamana Mochitoyo
	}
	1472.8.1={
		holder=10050044 # Yamana Masatoyo
	}
	1499.3.13={
		holder=10050045 # Yamana Munetoyo
	}
	1512.1.1={
		holder=10050046 # Yamana Nobutoyo
	}
	1528.3.4={
		holder = 10050005 # Yamana Suketoyo
	}
	1569.8.1={
		liege="k_nmih_chubu"
		holder=10029400 # Hashiba Hideyoshi
	}
	1570.1.1={
		holder = 10050005 # Yamana Suketoyo
	}
	1575.5.1={
		liege=0
	}
	1580.7.2={
		holder=10029401 # Hashiba Hidenaga
		liege="d_nmih_harima"
	}
}
