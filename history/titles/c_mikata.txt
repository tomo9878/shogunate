c_nmih_mikata = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1331.1.1 = { change_development_level = 10 }

	0701.1.1={
		liege="d_nmih_wakasa"
	}
	1180.1.1={
		liege=0
		holder=12480100 # Inaba Tokisada
	}
	1183.6.22={
		 liege="k_nmih_hokuriku" # Battle of Shinohara
	}
	1183.8.17={
		 liege="e_nmih_japan" # Taira's escape to the west
	}
	1196.8.1={
		liege=0
		holder=0 # 
	}
	1440.1.1={
		holder=10042001 # Takeda Nobuhide
	}
	1440.8.20={
		holder=10056104 # Takeda Nobukata
	}
	1471.6.20={
		holder=10042002 # Takeda Kuninobu
	}
	1475.1.1={
		holder=10042003 # Takeda Nobuchika
	}
	1514.8.22={
		holder=10042004 # Takeda Motonobu
	}
	1521.12.3={
		holder=10042000 # Takeda Motomitsu
	}
	1539.1.1={
		holder=10042005 #Takeda Nobutoyo, the 7th header of the Wakasa-Takeda family.
	}
	1556.1.1={
		holder=10042006 # Takeda Yoshizumi
	}
	1567.5.16={
		liege="d_nmih_wakasa" # 42012 Takeda Motoaki
		holder=10042030 # Awaya Katsuhisa
	}
	1568.6.1={
		liege=0 # 
	}
	1570.1.1={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
	}
	1573.9.16={
		liege="d_nmih_wakasa" # 29310 Niwa Nagahide
	}

}
