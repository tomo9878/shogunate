d_nmih_wakasa = {
	0701.1.1={
		liege="e_nmih_tenno"
	}
	1084.8.2={
		 holder=11003650 # Fujiwara Masaie {Wakasa no Kami}
	}
	1101.10.24={
		 holder=10110023 # Taira Masamori {Wakasa no Kami}
	}
	1135.3.31={
		 holder=11001035 # Fujiwara Kinnobu {Wakasa no Kami}
	}
	1174.3.3={
		holder=10110102 # Taira Atsumori {Wakasa no Kami}
	}
	1178.2.24={
		holder=10110064 # Taira Moromori {Wakasa no Kami}
	}
	1179.2.21={
		holder=10110031 # Taira Tsunemori {Chigyo-kokushi (sovereign of the fief)}
	}
	1179.12.18={
		liege="k_nmih_sanin"
		holder=10110101 #  Taira Tsunetoshi {Wakasa no Kami}
	}
	1183.6.22={
		 holder=0 # Battle of Shinohara
	}
	1203.9.1={
		liege="e_nmih_japan"
		 holder=10131653 # Hojo Yoshitoki
	}
	1220.1.1={
		 holder=0 #  
	}
	1228.1.1={
		 holder=10131688 # Hojo Tokiuji
	}
	1229.1.1={
		 holder=10131823 # Hojo Tsunetoki
	}
	1231.1.1={
		 holder=10131668 # Hojo Shigetoki
	}
	1259.1.1={
		 holder=0 #  
	}
	1260.1.1={
		 holder=10131712 # Hojo Tokishige
	}
	1271.1.1={
		 holder=10131998 # Hojo Tokimune
	}
	1284.4.20={
		 holder=10132155 # Hojo Sadatoki
	}
	1299.1.1={
		 holder=10132163 # Hojo Munekata
	}
	1305.5.27={
		 holder=10131777 # Hojo Nobutoki
	}
	1309.1.1={
		 holder=10132155 # Hojo Sadatoki
	}
	1311.12.6={
		 holder=10132268 # Hojo Takatoki
	}
	1333.7.4={
		holder=0
	}
	1440.1.1={
		holder=10042001 # Takeda Nobuhide, the 1st header of the Wakasa-Takeda family.
	}
	1440.8.20={
		holder=10056104 # Takeda Nobukata, the 2nd header of the Wakasa-Takeda family.
	}
	1471.6.20={
		holder=10042002 # Takeda Kuninobu, the 3rd header of the Wakasa-Takeda family.
	}
	1475.1.1={
		liege=0
		holder=10042003 # Takeda Nobuchika, the 4th header of the Wakasa-Takeda family.
	}
	1514.8.22={
		holder=10042004 # Takeda Motonobu, the 5th header of the Wakasa-Takeda family.
	}
	1521.12.3={
		holder=10042000 #Takeda Motomitsu, the 6th header of the Wakasa-Takeda family.
	}
	1539.1.1={
		holder=10042005 #Takeda Nobutoyo, the 7th header of the Wakasa-Takeda family.
	}
	1556.1.1={
		holder=10042006 #Takeda Yoshizumi, the 8th header of the Wakasa-Takeda family.
	}
	1567.5.16={
		holder=10042012 #Takeda Motoaki, the 9th header of the Wakasa-Takeda family.
	}
	1568.6.1={
		holder=0
	}
	1573.9.16={
		liege="k_nmih_chubu" # 29120 Oda Nobunaga
		holder=10029310 # Niwa Nagahide
	}
	1582.6.21={ # Honno-ji Incident
		liege=0
	}
	1582.7.2={
		liege="k_nmih_northern_kinai"
	}
}
