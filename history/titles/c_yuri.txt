c_nmih_yuri = {
	1.1.1 = { change_development_level = 10 }
	1100.1.1 = { change_development_level = 10 }
	1450.1.1 = { change_development_level = 10 }

	0712.9.23={
		liege="k_nmih_dewa"
	}
	1180.1.1={
		liege="k_nmih_northern_oshu" # Fujiwara Hidehira
		holder=12000300 # Yuri Hachiro
	}
	1189.9.24={
		liege="k_nmih_western_kanto"
		holder=10120075 # Minamoto Yoritomo
	}
	1189.11.15={
		holder=12391106 # Yuri Korehira
	}
	1190.1.1={
		liege=0 # Okawa Kaneto's rebellion
		holder=12000011 # Okawa Kaneto
	}
	1190.4.16={
		liege="k_nmih_western_kanto" # End of Okawa Kaneto's rebellion
		holder=12391110 # Yuri Korehisa
	}
	1213.5.24={
		holder=12360506 # Kagami Daini
	}
	1224.1.1={
		holder=12360605 # Oi Tomomitsu
	}
	1225.1.1={
		holder=12361001 # Oi Tomouji
	}
	1280.1.1={
		holder=12361050 # Oi Mitsuie
	}
	1330.1.1={
		holder=12361055 # Oi Mitsunaga
	}
	1370.1.1={
		holder=12361060 # Oi Yukimitsu
	}
	1410.1.1={
		holder=12361065 # Oi Tomomitsu
	}
	1450.1.1={
		liege=0
		holder=12361070 # Nikaho Tomokiyo
	}
	1503.1.1={
		holder=10000555 # Nikaho Kiyomasa
	}
	1540.1.1={
		holder=10000556 # Nikaho Kiyohisa
	}
	1576.1.1={
		holder=10000553 # Nikaho Kiyonaga
	}
	1577.1.1={
		holder=10000550 # Nikaho Shigekiyo
	}
	1583.1.1={
		holder=10000551 # Nikaho Kiyoharu
	}
	1586.1.1={
		holder=10000552 # Nikaho Mitsushige
	}

}
