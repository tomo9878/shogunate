c_nmih_kami_ina = {
	1.1.1 = { change_development_level = 20 }
	1100.1.1 = { change_development_level = 20 }
	1374.1.1 = { change_development_level = 20 }
	1400.1.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_shinano"
	}
	1180.1.1={
		liege="d_nmih_echigo"
		holder=12388510 # Kasahara Tomonori
	}
	1180.9.30={
		liege="k_nmih_shinano" # Minamoto Yoshinaka
		holder=10120084 # Minamoto Yoshinaka / Siege of Odagiri Castle
	}
	1183.6.22={
		liege="k_nmih_hokuriku" # Minamoto Yoshinaka
		holder=10120084 # Minamoto Yoshinaka / Battle of Shinohara
	}
	1183.8.17={
		liege="e_nmih_japan"
		holder=10120084 # Minamoto Yoshinaka / Taira's escape to the west
	}
	1184.3.4={
		liege="k_nmih_western_kanto"
		holder=10120075 # Minamoto Yoritomo
	}
	1184.4.1={
		holder=12386535 # Katagiri Tameyasu
	}
	1185.4.25={
		liege="d_nmih_shinano"
	}
	1200.1.1={
		holder=12204341 # Hiki Yoshikazu
	}
	1203.10.8={
		holder=10131653 # Hojo Yoshitoki
	}
	1224.7.1={
		holder=10131666 # Hojo Yasutoki
	}
	1242.7.14={
		holder=10131823 # Hojo Tsunetoki
	}
	1246.5.17={
		holder=10131824 # Hojo Tokiyori
	}
	1263.12.24={
		holder=10131998 # Hojo Tokimune
	}
	1284.4.20={
		holder=10132155 # Hojo Sadatoki
	}
	1311.12.6={
		holder=10132268 # Hojo Takatoki
	}
	1333.7.4={
		holder=12375967 # Suwa Tokitsugu
	}
	1335.7.1={
		holder=10132313 # Hojo Tokiyuki
	}
	1340.1.1={
		holder=12380613 # Kosaka Takamune
	}
	1344.1.1={
		holder=10041903 # Yamato Muneyoshi
	}
	1374.1.1={
		holder=12375976 # Takato Yorichika
	}
	1390.1.1={
		holder=12375978 # Takato Yoshimitsu
	}
	1420.1.1={
		holder=12375979 # Takato Taigen
	}
	1450.1.1={
		liege=0
		holder=12375980 # Takato Etsuzan
	}
	1470.1.1={
		holder=12375981 # Takato Tsugumune
	}
	1510.1.1={
		holder=12375983 # Takato Mitsutsugu
	}
	1530.1.1={
		holder=12375984 # Takato Yoritsugu
	}
	1545.5.27={
		liege="d_nmih_kai" # Takeda Shingen
		holder=10020010 # Takeda Shingen
	}
	1559.1.1={
		liege="d_nmih_shinano"
	}
	1562.6.1={
		liege="d_nmih_shinano" # 20010 Takeda Shingen
		holder=10020033 # Takeda Katsuyori
	}
	1565.11.7={
		liege="d_nmih_shinano"
		holder=10020012 # Takeda Nobukado
	}
	1581.1.1={
		holder=10020035 # Nishina (Takeda) Morinobu
	}
	1582.3.25={
		liege="e_nmih_japan"
		holder=10029120 # Oda Nobunaga
	}
	1582.04.15={
		liege="k_nmih_chubu" # 29160 Oda Nobutada
		holder=10029025 # Mori Hideyori
	}
	1582.6.21={ # Honno-ji Incident
		liege="k_nmih_tokai"
		holder=10024006 # Tokugawa Ieyasu
	}
}
