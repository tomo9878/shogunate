c_nmih_izu_naka = {
	1.1.1 = { change_development_level = 10 }
	1400.1.1 = { change_development_level = 20 }
	1493.8.1 = { change_development_level = 20 }

	0701.1.1={
		liege="d_nmih_izu"
	}
	1116.1.1={
	    holder=10130044 # Kudo Ietsugu
	}
	1146.1.1={
	    holder=10130048 # Kudo Suketsugu
	}
	1161.1.1={
	    holder=10130051 # Ito Sukechika
	}
	1180.1.1={
		liege="d_nmih_sagami"
	}
	1180.11.9={
		liege="k_nmih_western_kanto" # Minamoto Yoritomo
	    holder=10130078 # Kudo Suketsune
	}
	1193.6.28={
		liege="d_nmih_izu"
		holder=10130080 # Ito Suketoki / Killed by Soga
	}
	1252.7.24={
		holder=0
	}
	1458.6.1={
		liege="d_nmih_izu"
		holder=10057620 # Shibukawa Yoshikane
	}
	1463.1.1={
		holder=10017071 # Uesugi Masanori
	}
	1487.1.1={
		holder=10040037 # Ashikaga Masatomo
	}
	1491.5.11={
		holder=10040038 # Ashikaga Chachamaru
	}
	1493.8.1={
		holder=10016001 # Hojo Soun
	}
	1519.9.8={
		holder=10016000 # Hojo Ujitsuna
	}
	1526.1.1={
		liege="d_nmih_sagami"
		holder=10016100 # Shimizu Tsunayoshi
	}
	1557.1.1={
		holder=10016101 # Shimizu Tsunayoshi
	}
}
