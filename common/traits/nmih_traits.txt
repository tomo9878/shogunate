## Override Vanilla Traits

ambitious = {
	index = 77
	personality = yes
	opposites = {
		content
	}
	intrigue = 1
	stewardship = 1
	diplomacy = 1
	martial = 1
	learning = 1
	prowess = 1
	
	opinion_of_liege = -15
	same_opinion = -15
	
	stress_gain_mult = 0.25
	
	ai_war_cooldown = -0.25
	ai_war_chance = 1
	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_ambitious_desc
			}
			desc = trait_ambitious_character_desc
		}
	}

	ai_honor = high_negative_ai_value
	ai_greed = very_high_positive_ai_value
	ai_energy = very_high_positive_ai_value
	ai_boldness = high_positive_ai_value
	ai_zeal = very_low_positive_ai_value
	ai_sociability = low_positive_ai_value

	compatibility = {
		ambitious = @pos_compat_low
		diligent = @pos_compat_low
		greedy = @pos_compat_low
		brave = @pos_compat_low
		rowdy = @pos_compat_low
		content = @neg_compat_medium
		lazy = @neg_compat_medium
		craven = @neg_compat_medium
		generous = @neg_compat_low
		trusting = @neg_compat_low
	}
}

cynical = {
	index = 81
	personality = yes
	opposites = {
		zealous
	}
	intrigue = 2
	learning = 2
	
	monthly_piety_gain_mult = -0.2
	faith_conversion_piety_cost_mult = -0.2

	opposite_opinion = -10
	same_opinion = 10

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_cynical_desc
			}
			desc = trait_cynical_character_desc
		}
	}

	ai_honor = high_negative_ai_value
	ai_rationality = medium_positive_ai_value
	ai_energy = low_negative_ai_value
	ai_zeal = dominant_negative_ai_value
	ai_compassion = very_low_negative_ai_value

	compatibility = {
		cynical = @pos_compat_high
		whole_of_body = @pos_compat_medium
		scholar = @pos_compat_medium
		theologian = @pos_compat_medium
		honest = @pos_compat_low
		intellect_good_1 = @pos_compat_low
		intellect_good_2 = @pos_compat_low
		intellect_good_3 = @pos_compat_low
		shrewd = @pos_compat_low
		zealous = @neg_compat_high
		intellect_bad_1 = @neg_compat_medium
		intellect_bad_2 = @neg_compat_medium
		intellect_bad_3 = @neg_compat_medium
		trusting = @neg_compat_low
	}
}

## Special Traits

immortality = {
	index = 351

	immortal = yes
	health = 10

	icon = {
		desc = saoshyant.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_immortality_desc
			}
			desc = trait_immortality_character_desc
		}
	}
}

child_of_adoption = {
	index = 352

	diplomacy = -1

	icon = {
		desc = born_in_the_purple.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_child_of_adoption_desc
			}
			desc = trait_child_of_adoption_character_desc
		}
	}
}

ashikaga_family = {
	index = 353

	monthly_prestige = 0.1

	# Always inherited from fathers
	inherit_chance = 100
	parent_inheritance_sex = male
	inherit_from_real_father = no

	icon = {
		desc = blood_of_prophet.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_ashikaga_family_desc
			}
			desc = trait_ashikaga_family_character_desc
		}
	}
}

tea_master = {
	index = 354
	
	diplomacy = 2
	learning = 1
	
	same_opinion = 20

	icon = {
		desc = content.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_tea_master_desc
			}
			desc = trait_tea_master_character_desc
		}
	}
}

painter = {
	index = 355

	stewardship = 1
	learning = 2
	
	same_opinion = 20

	icon = {
		desc = calm.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_painter_desc
			}
			desc = trait_painter_character_desc
		}
	}
}



## Ranks of the Imperial Court

jugoi_ge = {
	index = 356

	monthly_prestige = 0.1
	general_opinion = 5

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_jugoi_ge_desc
			}
			desc = trait_jugoi_ge_character_desc
		}
	}
}

jugoi_jo = {
	index = 357

	monthly_prestige = 0.2
	general_opinion = 10

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_jugoi_jo_desc
			}
			desc = trait_jugoi_jo_character_desc
		}
	}
}

shogoi_ge = {
	index = 358

	monthly_prestige = 0.3
	general_opinion = 15

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_shogoi_ge_desc
			}
			desc = trait_shogoi_ge_character_desc
		}
	}
}

shogoi_jo = {
	index = 359

	monthly_prestige = 0.4
	general_opinion = 20

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_shogoi_jo_desc
			}
			desc = trait_shogoi_jo_character_desc
		}
	}
}

jushii_ge = {
	index = 360

	monthly_prestige = 0.5
	general_opinion = 25

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_jushii_ge_desc
			}
			desc = trait_jushii_ge_character_desc
		}
	}
}

jushii_jo = {
	index = 361

	monthly_prestige = 0.6
	general_opinion = 30
	diplomacy = 1
	stewardship = 1

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_jushii_jo_desc
			}
			desc = trait_jushii_jo_character_desc
		}
	}
}

shoshii_ge = {
	index = 362

	monthly_prestige = 0.8
	general_opinion = 35
	diplomacy = 2
	stewardship = 1

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_shoshii_ge_desc
			}
			desc = trait_shoshii_ge_character_desc
		}
	}
}

shoshii_jo = {
	index = 363

	monthly_prestige = 1.0
	general_opinion = 40
	diplomacy = 2
	stewardship = 2

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_shoshii_jo_desc
			}
			desc = trait_shoshii_jo_character_desc
		}
	}
}

jusanmi = {
	index = 364

	monthly_prestige = 1.5
	general_opinion = 45
	diplomacy = 3
	stewardship = 2

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_jusanmi_desc
			}
			desc = trait_jusanmi_character_desc
		}
	}
}

shosanmi = {
	index = 365

	monthly_prestige = 2.0
	general_opinion = 50
	diplomacy = 3
	stewardship = 3

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_shosanmi_desc
			}
			desc = trait_shosanmi_character_desc
		}
	}
}

junii = {
	index = 366

	monthly_prestige = 2.5
	general_opinion = 75
	diplomacy = 4
	stewardship = 3

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_junii_desc
			}
			desc = trait_junii_character_desc
		}
	}
}

shonii = {
	index = 367

	monthly_prestige = 3.0
	general_opinion = 100
	diplomacy = 4
	stewardship = 4

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_shonii_desc
			}
			desc = trait_shonii_character_desc
		}
	}
}

juichii = {
	index = 368

	monthly_prestige = 4
	general_opinion = 100
	diplomacy = 5
	stewardship = 4

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_juichii_desc
			}
			desc = trait_juichii_character_desc
		}
	}
}

shoichii = {
	index = 369

	monthly_prestige = 5.0
	general_opinion = 100
	diplomacy = 5
	stewardship = 5

	icon = {
		desc = augustus.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_shoichii_desc
			}
			desc = trait_shoichii_character_desc
		}
	}
}

## CK2 Compatibility

stressed = {
	index = 370
	health_trait = yes

	stewardship = -1
	intrigue = -1
	health = -0.5

	fertility = -0.1

	icon = {
		desc = depressed_1.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_stressed_desc
			}
			desc = trait_stressed_character_desc
		}
	}
}

is_fat = {
	index = 371
	physical = yes

	prowess = -2

	ai_zeal = -20

	icon = {
		desc = giant.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_is_fat_desc
			}
			desc = trait_is_fat_character_desc
		}
	}
}

falconer = {
	index = 372

	diplomacy = 1

	same_opinion = 10

	icon = {
		desc = brave.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_falconer_desc
			}
			desc = trait_falconer_character_desc
		}
	}
}

poet = {
	index = 373

	diplomacy = 1

	same_opinion = 10

	icon = {
		desc = compassionate.dds
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_poet_desc
			}
			desc = trait_poet_character_desc
		}
	}
}


