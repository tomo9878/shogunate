# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:10029100
bm_sengoku1534_oda_nobuhide={
	type=male
	id=62147
	age=0.240000
	genes={ 		hair_color={ 112 241 126 241 }
 		skin_color={ 212 106 222 101 }
 		eye_color={ 63 243 67 244 }
 		gene_chin_forward={ "chin_forward_neg" 105 "chin_forward_pos" 134 }
 		gene_chin_height={ "chin_height_pos" 130 "chin_height_pos" 128 }
 		gene_chin_width={ "chin_width_pos" 138 "chin_width_neg" 96 }
 		gene_eye_angle={ "eye_angle_pos" 159 "eye_angle_pos" 142 }
 		gene_eye_depth={ "eye_depth_neg" 112 "eye_depth_neg" 91 }
 		gene_eye_height={ "eye_height_neg" 66 "eye_height_neg" 105 }
 		gene_eye_distance={ "eye_distance_pos" 135 "eye_distance_neg" 116 }
 		gene_eye_shut={ "eye_shut_pos" 134 "eye_shut_pos" 144 }
 		gene_forehead_angle={ "forehead_angle_pos" 165 "forehead_angle_pos" 144 }
 		gene_forehead_brow_height={ "forehead_brow_height_pos" 148 "forehead_brow_height_pos" 127 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 94 "forehead_roundness_neg" 110 }
 		gene_forehead_width={ "forehead_width_pos" 137 "forehead_width_neg" 44 }
 		gene_forehead_height={ "forehead_height_neg" 104 "forehead_height_neg" 103 }
 		gene_head_height={ "head_height_neg" 126 "head_height_neg" 101 }
 		gene_head_width={ "head_width_neg" 108 "head_width_neg" 86 }
 		gene_head_profile={ "head_profile_neg" 26 "head_profile_neg" 102 }
 		gene_head_top_height={ "head_top_height_neg" 115 "head_top_height_neg" 109 }
 		gene_head_top_width={ "head_top_width_pos" 129 "head_top_width_pos" 160 }
 		gene_jaw_angle={ "jaw_angle_pos" 174 "jaw_angle_neg" 115 }
 		gene_jaw_forward={ "jaw_forward_neg" 120 "jaw_forward_pos" 159 }
 		gene_jaw_height={ "jaw_height_neg" 125 "jaw_height_pos" 128 }
 		gene_jaw_width={ "jaw_width_neg" 114 "jaw_width_pos" 155 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_pos" 132 "mouth_corner_depth_neg" 70 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 128 "mouth_corner_height_neg" 125 }
 		gene_mouth_forward={ "mouth_forward_neg" 126 "mouth_forward_neg" 115 }
 		gene_mouth_height={ "mouth_height_neg" 114 "mouth_height_pos" 130 }
 		gene_mouth_width={ "mouth_width_pos" 132 "mouth_width_neg" 101 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_neg" 98 "mouth_upper_lip_size_neg" 119 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_neg" 106 "mouth_lower_lip_size_neg" 113 }
 		gene_mouth_open={ "mouth_open_neg" 113 "mouth_open_pos" 218 }
 		gene_neck_length={ "neck_length_neg" 113 "neck_length_neg" 113 }
 		gene_neck_width={ "neck_width_pos" 153 "neck_width_pos" 137 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 25 "cheek_forward_neg" 37 }
 		gene_bs_cheek_height={ "cheek_height_neg" 15 "cheek_height_pos" 20 }
 		gene_bs_cheek_width={ "cheek_width_pos" 92 "cheek_width_pos" 26 }
 		gene_bs_ear_angle={ "ear_angle_neg" 16 "ear_angle_pos" 1 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 21 "ear_inner_shape_pos" 59 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 43 "ear_upper_bend_pos" 21 }
 		gene_bs_ear_outward={ "ear_outward_neg" 19 "ear_outward_neg" 45 }
 		gene_bs_ear_size={ "ear_size_neg" 18 "ear_size_neg" 4 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_neg" 200 "eye_corner_depth_neg" 91 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_pos" 2 "eye_fold_shape_neg" 21 }
 		gene_bs_eye_size={ "eye_size_neg" 39 "eye_size_neg" 48 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 224 "eye_upper_lid_size_neg" 214 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 31 "forehead_brow_curve_neg" 18 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 7 "forehead_brow_forward_pos" 45 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 40 "forehead_brow_inner_height_pos" 34 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 18 "forehead_brow_outer_height_neg" 57 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_neg" 11 "forehead_brow_width_neg" 2 }
 		gene_bs_jaw_def={ "jaw_def_neg" 234 "jaw_def_neg" 205 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 6 "mouth_lower_lip_def_pos" 172 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 6 "mouth_lower_lip_full_pos" 96 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 50 "mouth_lower_lip_pad_neg" 64 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 10 "mouth_lower_lip_width_neg" 3 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 11 "mouth_philtrum_def_pos" 34 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_pos" 112 "mouth_philtrum_shape_pos" 106 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 9 "mouth_philtrum_width_pos" 42 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 49 "mouth_upper_lip_def_pos" 24 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 121 "mouth_upper_lip_full_neg" 33 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_pos" 189 "mouth_upper_lip_profile_pos" 72 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 68 "mouth_upper_lip_width_neg" 56 }
 		gene_bs_nose_forward={ "nose_forward_neg" 17 "nose_forward_pos" 36 }
 		gene_bs_nose_height={ "nose_height_neg" 91 "nose_height_pos" 74 }
 		gene_bs_nose_length={ "nose_length_neg" 87 "nose_length_neg" 63 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 22 "nose_nostril_height_neg" 9 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 24 "nose_nostril_width_pos" 7 }
 		gene_bs_nose_profile={ "nose_profile_pos" 16 "nose_profile_pos" 7 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 1 "nose_ridge_angle_pos" 43 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 9 "nose_ridge_width_pos" 48 }
 		gene_bs_nose_size={ "nose_size_pos" 69 "nose_size_pos" 30 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 2 "nose_tip_angle_neg" 41 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_pos" 18 "nose_tip_forward_neg" 101 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 32 "nose_tip_width_neg" 36 }
 		face_detail_cheek_def={ "cheek_def_02" 1 "cheek_def_01" 10 }
 		face_detail_cheek_fat={ "cheek_fat_01_neg" 52 "cheek_fat_01_pos" 8 }
 		face_detail_chin_cleft={ "chin_cleft" 60 "chin_dimple" 16 }
 		face_detail_chin_def={ "chin_def" 11 "chin_def" 17 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 30 "eye_lower_lid_def" 17 }
 		face_detail_eye_socket={ "eye_socket_03" 250 "eye_socket_02" 222 }
 		face_detail_nasolabial={ "nasolabial_01" 7 "nasolabial_03" 11 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 240 "nose_ridge_def_neg" 25 }
 		face_detail_nose_tip_def={ "nose_tip_def" 18 "nose_tip_def" 6 }
 		face_detail_temple_def={ "temple_def" 20 "temple_def" 94 }
 		expression_brow_wrinkles={ "brow_wrinkles_03" 218 "brow_wrinkles_04" 47 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 255 "eye_wrinkles_01" 223 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_02" 164 "forehead_wrinkles_01" 62 }
 		expression_other={ "cheek_wrinkles_left_01" 127 "cheek_wrinkles_left_01" 127 }
 		complexion={ "complexion_2" 206 "complexion_3" 136 }
 		gene_height={ "normal_height" 96 "normal_height" 117 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 163 "body_fat_head_fat_medium" 152 }
 		gene_bs_body_shape={ "body_shape_average_clothed" 112 "body_shape_triangle_half" 0 }
 		gene_bs_bust={ "bust_clothes" 199 "bust_shape_4_full" 111 }
 		gene_age={ "old_3" 151 "old_2" 3 }
 		gene_eyebrows_shape={ "far_spacing_high_thickness" 158 "far_spacing_low_thickness" 163 }
 		gene_eyebrows_fullness={ "layer_2_avg_thickness" 241 "layer_2_high_thickness" 201 }
 		gene_body_hair={ "body_hair_sparse_low_stubble" 1 "body_hair_sparse_low_stubble" 205 }
 		hairstyles={ "steppe_hairstyles" 21 "steppe_hairstyles" 21 }
 		beards={ "steppe_beards" 199 "steppe_beards" 159 }
 		eye_accessory={ "normal_eyes" 187 "normal_eyes" 187 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "asian_eyelashes" 203 "asian_eyelashes" 203 }
 		pose={ "" 255 "" 0 }
 		clothes={ "steppe_low_nobility_clothes" 219 "most_clothes" 0 }
 		headgear={ "no_headgear" 184 "no_headgear" 0 }
 		legwear={ "mena_common_legwear" 111 "all_legwear" 0 }
 }
	entity={ 979141817 979141817 }
}

