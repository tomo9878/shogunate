﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

tohoku = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

kanto = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

hokuriku = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

chubu = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

kinai = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

saigoku = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

kyushu = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

imperial_court = {
	template = "asian"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

ryukyu = {
	template = "caucasian_blond"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

ainu = {
	template = "caucasian_blond"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 

okhotsk = {
	template = "caucasian_blond"
	skin_color = {
		10 = { 0.5 0.3 1.0 0.5 }
	}
	eye_color = {
		# # Brown
		# 50 = { 0.05 0.7 0.35 1.0 }
		# Black
		50 = { 0.05 0.95 0.35 1.0 }
	}
	hair_color = {
		# Blonde
		# 10 = { 0.25 0.2 0.75 0.55 }
		# Brown
		# 2 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 10 = { 0.85 0.0 1.0 0.5 }
		# Black
		98 = { 0.01 0.9 0.5 0.99 }
	}
	hairstyles = {
	    10 = { name = steppe_hairstyles          range = { 0.0 1.0 } }
	}
	beards = {
		10 = { name = steppe_beards 		range = { 0.0 1.0 } }
	}	
} 
